﻿[![GPL enforced badge](https://img.shields.io/badge/GPL-enforced-blue.svg "This project enforces the GPL.")](https://gplenforced.org)
# RogueBox Adventures

__Eons ago the gods imagined all kinds of worlds and gave them their shapes.__  
__In order to protect their creations they have made the wanderers: mighty warriors formed in the image of the gods.__  
__As some wanderers became more like the gods themselves they built a sanctuary, and called it 'The Elysium'.__  
__From there younger wanderers travel the worlds, ﻿tasked to protect them and maintain the fragile balance in the fabric of creation.__  
  
__You are one of them – and you come in these troubled times with a task of utmost importance.__  
  
![Image](https://rogueboxadventures.tuxfamily.org/screens/screen_elysium.png)
  
  
_RogueBox Adventures_ is an unique mix between roguelike and sandbox game. You will visit different worlds, searching for adventures and problems to solve.
The game offers a story with a variety of quests in an open world format, and gives you the ability to build ﻿different structures and to craft powerful items.
  
### Fight - Explore - ﻿Reshape worlds 
  
  
## Features:
  
* Unique blend of roguelike and sandbox game
* Various Locations - different worlds with surface and underground areas as well as dungeons
* Many different monsters with unique abilities
* A story that guides you through the game and motivates you to explore the worlds
* Original handcrafted pixel art
* Free and Open Source Software - Created by enthusiasts with a lot of love but without commercial interests
  
* Item crafting and shelter building
* Satisfy your basic needs like thirst, hunger and shelter in order to maintain your strength
* Find and care for pets, that will support you on your journey 
  
![Image](https://rogueboxadventures.tuxfamily.org/screens/screen_grassland.png)
![Image](https://rogueboxadventures.tuxfamily.org/screens/screen_sever.png)
![Image](https://rogueboxadventures.tuxfamily.org/screens/screen_tomb.png)
![Image](https://rogueboxadventures.tuxfamily.org/screens/screen_grotto.png)
  
  
# FAQ
## Where can I get the game? On which platforms does it run?
You can download a build of RBA at: [https://themightyglider.itch.io/roguebox-adventures](https://themightyglider.itch.io/roguebox-adventures)

It is distributed in three ways:

* A Windows standalone to run the game on Microsoft Windows PCs
* A GNU/Linux standalone for 32 and 64 Bit systems 
* A source release that should be runnable on any system with a working installation of Python (3+) and pygame. (So the game also runs on OSX, BSD*, Haiku and so on.) You can download Python at: https://www.python.org/ and pygame at: http://pygame.org. Afterward simply run the file main.py from the main directory of RBA.

## How do I play? What are the key-bindings?

RBA can be played with the keyboard, mouse/touchscreen or a gamepad.

The keybindings are:

    w,a,s,d/Arrow-keys/gamepad hat : move/navigate trough the menus
    e/ENTER/gamepad Button1        : interaction/choose/use
    i/BACKSPACE/gamepad Button4    : open inventory
    b/SPACE/gamepad Button3        : open build menu/switch mode/done/drop
    x/ESCAPE/gamepad Button2       : open main menu/abort/back
    f       /gamepad Button5       : fire mode on/off
    .       /gamepad Button6       : skip turn
    
There are also a few shortcuts if you are playing with keyboard:

    1-7								: open inventory on page...
    q								: show quest log
    c								: show character status
    t								: throw something
    r								: read something
    
The game is playable without using any of this shortcuts. They are only implemented to make gameplay faster.

A low-spoiler tutorial on gameplay can be found in the tutorial.html file.

## I've found a bug. I want to make a suggestion. I want to contribute something.

You can report bugs at: [https://gitlab.com/The_Mighty_Glider/rba/issues](https://gitlab.com/The_Mighty_Glider/rba/issues)

Or you can mail me at: contact@rogueboxadventures.tuxfamily.org (love- or hate-mail is welcome as well)

## Have Fun!

Thank you for being interested in my little game. I hope it will bring you some hours of joy.

	Copyright (C) 2015-2020  Marian Luck
	
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
