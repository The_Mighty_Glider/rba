#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

#This file contains the definition of the buffs class
#Buffs are effects that work on the player for some rounds. eg.:bleeding
#every player object contains a buff object

class buffs():
	
	def __init__(self):
		
		self.buff_list = {}
		
	def set_buff(self,name,duration,add = True,maximum = -1):
		
		if add:
			try:
				if duration > self.buff_list[name]/2:
					self.buff_list[name] = int((self.buff_list[name]/2) + duration)
				else:
					self.buff_list[name] += 10
			except:
				if len(self.buff_list) < 13:
					self.buff_list[name] = duration
		else:
			try:
				if duration > self.buff_list[name]:
					self.buff_list[name] = duration
			except:
				if len(self.buff_list) < 13:
					self.buff_list[name] = duration
				
		if maximum > 0:
			try:
				if self.buff_list[name] > maximum:
					self.buff_list[name] = maximum
			except:
				None			
			
	def buff_tick(self):
		
		key_list = self.buff_list.keys()
		
		del_list = []
		
		if len(key_list) > 0:
			for i in key_list:
				self.buff_list[i] -= 1
				if  self.buff_list[i] <= 0:
					del_list.append(i)
					
		if len(del_list) > 0:
			for j in del_list:
				del self.buff_list[j]
	
	def get_buff(self,name):
		
		try:
			r = self.buff_list[name]
		except:
			r = 0
			
		return r
		
	def remove_buff(self,name):
		
		try:
			del self.buff_list[name]
		except:
			print('error')
			
	def sget(self):
	
		slist = [' ']
		key_list = self.buff_list.keys()
		
		if len(key_list) > 0:
			for i in key_list:
				s = i.upper() + '(' + str(self.buff_list[i]) + ')'
				slist.append(s)
			
		if len(slist) > 1:
			del slist[0]
			
		if len(slist) > 3:
			new_list = [slist[0],slist[1],slist[2],'more...']
			slist = new_list
		
		return slist
