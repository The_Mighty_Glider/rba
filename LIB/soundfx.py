#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

try:
	import pygame_sdl2 as pygame
except:
	import pygame

import os
import sys
from time import sleep as sleep

from util import game_options

basic_path = os.path.dirname(os.path.realpath('main.py')) #just get the execution path for resources

if '-h' in sys.argv:
	home_save = True
else:
	home_save = False

game_options = game_options(basic_path,home_save)

class sfX():
	
	def __init__(self):
		
		sfx_path = basic_path + os.sep + 'AUDIO' + os.sep + 'SFX' + os.sep
		
		s_files = os.listdir(sfx_path)
		
		self.sfx_list = {}
		
		for i in s_files:
			if i.find('.ogg') != -1:
				name = i.replace('.ogg','')
				self.sfx_list[name] =  pygame.mixer.Sound(sfx_path + i)
		
	def set_loudness(self):
		
		global game_options
		
		name_list = self.sfx_list.keys()
		
		for i in name_list:
			self.sfx_list[i].set_volume(game_options.sfxmode)
							
	def play(self,sfx_name):
		
		global game_options
		
		try:
			if pygame.mixer.get_busy():
				sleep(0.1)
			if game_options.sfxmode != 0:
				self.sfx_list[sfx_name].play()
		except:
			print('SFX error')
