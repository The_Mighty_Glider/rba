#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

class skill():
	
	def __init__(self):
		
		self.weapon_crafting = 'Novice'
		self.metallurgy = 'Novice'
		self.alchemy = 'Novice'
		self.woodcutting = 'Novice'
		self.woodcutting_progress = 0 
		self.mining = 'Novice'
		self.mining_progress = 0
		
	def raise_skill(self,skill):
		
		if skill == 'weapon crafting':
			if self.weapon_crafting == 'Novice':
				self.weapon_crafting = 'Adept'
				return True
			elif self.weapon_crafting == 'Adept':
				self.weapon_crafting = 'Master'
				return True
			else:
				return False
		elif skill == 'metallurgy':
			if self.metallurgy == 'Novice':
				self.metallurgy = 'Adept'
				return True
			elif self.metallurgy == 'Adept':
				self.metallurgy = 'Master'
				return True
			else:
				return False
		elif skill == 'alchemy':
			if self.alchemy == 'Novice':
				self.alchemy = 'Adept'
				return True
			elif self.alchemy == 'Adept':
				self.alchemy = 'Master'
				return True
			else:
				return False
		elif skill == 'woodcutting':
			if self.woodcutting == 'Novice':
				self.woodcutting = 'Adept'
				return True
			elif self.woodcutting == 'Adept':
				self.woodcutting = 'Master'
				return True
			else:
				return False
		elif skill == 'mining':
			if self.mining == 'Novice':
				self.mining = 'Adept'
				return True
			elif self.mining == 'Adept':
				self.mining = 'Master'
				return True
			else:
				return False
		else:
			return False
