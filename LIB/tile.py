#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

class tile():
	
	def __init__(self,techID, name, tile_color, use_group, move_group, grow_group, damage, move_message, damage_message, destroy = False, replace = None, civilisation = False, can_grown = False, build_here = True, tile_pos = (0,0), upper_tile = None, ignore_liquid = False, transparency = True, conected_items = None, conected_tiles = None, conected_resources = None, drops_here = True, special_group = 'None',indoor = False, special_num = 0, light_emit = 0, roofed = False, flower = False):
		
		self.techID = techID
		self.name = name
		self.tile_color = tile_color
		self.use_group = use_group
		self.move_group = move_group
		self.grow_group = grow_group
		self.damage = damage
		self.move_mes = move_message
		self.damage_mes = damage_message
		self.destroy = destroy
		self.replace = replace
		self.civilisation = civilisation
		self.can_grown = can_grown
		self.build_here = build_here
		self.indoor = indoor
		self.tile_pos = tile_pos
		self.upper_tile = upper_tile
		self.ignore_liquid = ignore_liquid
		self.transparency = transparency
		self.conected_items = conected_items
		self.conected_tiles = conected_tiles
		self.conected_resources = conected_resources
		self.drops_here = drops_here
		self.special_group = special_group
		self.special_num = special_num
		self.light_emit = light_emit
		self.roofed = roofed
		self.flower = flower
		self.no_spawn = False
