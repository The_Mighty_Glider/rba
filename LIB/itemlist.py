#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

import pickle as p
from item import item_food, item_misc, decorative_clothing, item_wear

class itemlist():
	
	def __init__(self):
		
		self.ilist = {'misc' : [], 'food' : [], 'clothe' : [], 'special_clothe' : [], 'artefact' : []}
		techID = 0
		
		t=item_misc(techID, 'Fountain', 'place(outdoor)', 'functional', 7)#0
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Chest', 'place(indoor)', 'functional' ,3)#empty chest
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Bed', 'place(indoor)', 'functional', 8)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Carpenter\'s workbench', 'place(indoor)', 'functional', 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Carver\'s workbench', 'place(indoor)', 'functional', 10)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Stonecutter\'s workbench', 'place(indoor)', 'functional', 11)#5
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Forger\'s workbench', 'place(indoor)', 'functional', 12)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Alchemist\'s workshop', 'place(indoor)', 'functional', 13)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Furnace', 'place(everywhere)', 'functional', 14)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Altar', 'place(indoor)', 'functional', 15)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Table', 'place(indoor)', 'functional', 16)#10
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Wooden seat', 'place(indoor)', 'functional', 17)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Stone seat', 'place(indoor)', 'functional', 18)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Bookshelf', 'place(indoor)', 'functional', 19)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Fishing rod', 'use', 0, 0)
		self.ilist['misc'].append(t)
		techID += 1
		#For blueprints it is important to use the une_name 'apply'. This one is only allowed for blueprints.
		#place_cat and place_num must show on the coresponding floor. The wall to the floor must be the next item inside the tiles list.
		t=item_misc(techID, 'Ordinary Blueprint', 'apply', 'building', 0)#15
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Blue Blueprint', 'apply', 'building', 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Green Blueprint', 'apply', 'building', 11)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Red Blueprint', 'apply', 'building', 13)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Orange Blueprint', 'apply', 'building', 15)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Purple Blueprint', 'apply', 'building', 17)#20
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Pilar', 'place(indoor)', 'functional', 22)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Orcish Blueprint', 'apply', 'building', 19)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Nobel Blueprint', 'apply', 'building', 21)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Bomb', 'place(everywhere)', 'effect', 0, max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		#only scrolls and spellbooks can use the use_name 'read'
		t=item_misc(techID, 'Scroll of Identify', 'read', None, None, 0, max_stack_size = 5)#25
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spellbook of Identify', 'read', None, None, 0)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Scroll of Repair', 'read', None, None, 1, max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spellbook of Idetify', 'read', None, None, 0)#doubled item replace at any point
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Scroll of Healing', 'read', None, None, 2, max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spellbook of Healing', 'read', None, None, 2)#30
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Scroll of Teleport', 'read', None, None, 3, max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spellbook of Teleport', 'read', None, None, 3)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Scroll of Return', 'read', None, None, 4, max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spellbook of Return', 'read', None, None, 4)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Scroll of Flames', 'read', None, None, 5, max_stack_size = 5)#35
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spellbook of Flames', 'read', None, None, 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Scroll of Healing Aura', 'read', None, None, 6, max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spellbook of Healing Aura', 'read', None, None, 6)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Elfish Blueprint', 'apply', 'building', 23)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Heart-Shaped Crystal', 'break', None, None, None)#40
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Mysterious Blue Crystal', 'break', None, None, None, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Enchanted Enhancement Powder', 'use', None, None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Heavy Sack', 'open', None, None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Torch', 'light', 'functional', 58, None, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Scroll of Light', 'read', None, None, 7, max_stack_size = 5)#45
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spellbook of Light', 'read', None, None, 7)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Scrub seed', 'plant', 'local', 6, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sapling', 'plant', 'local', 10, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Small cactus', 'plant', 'extra', 3, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Palm sapling', 'plant', 'extra', 10, max_stack_size = 9)#50
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Chalk', 'use', None, None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Fridge', 'place(indoor)', 'functional', 24)#empty fridge
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Camera', 'use', None, None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Crystal orb', 'use', None, None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Naga statue', 'place(everywhere)', 'statue', 0)#55
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Orc statue', 'place(everywhere)', 'statue', 1)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Blob statue', 'place(everywhere)', 'statue', 2)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Elf statue', 'place(everywhere)', 'statue', 3)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Neko statue', 'place(everywhere)', 'statue', 4)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Pendulum clock', 'place(everywhere)', 'deco', 0)#60
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Coffin', 'place(everywhere)', 'deco', 1)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Candleholder', 'place(indoor)', 'deco', 2)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Hourglass', 'place(everywhere)', 'deco', 3)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Ancient idol', 'place(everywhere)', 'deco', 4)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sarcophagus', 'place(everywhere)', 'deco', 5)#65
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Bonsai', 'place(indoor)', 'deco', 6)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Houseplant', 'place(indoor)', 'deco', 7)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Wooden throne', 'place(indoor)', 'deco', 8)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Stony throne', 'place(indoor)', 'deco', 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Book of Skill', 'read', None, None, 8)#70
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Heated Stone', 'place(everywhere)', 'functional', 28, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Explosive', 'place(everywhere)', 'effect', 7,max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Brimstone', 'place(everywhere)', 'functional', 29, max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Bandage', 'use', max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Eyedrops', 'use', max_stack_size = 5)#75
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Antidote', 'use', max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Holy Water', 'use', max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Stone', 'throw', None, None, 2, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Dart', 'throw', None, None, 3, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Throwing Knife', 'throw', None, None, 4, max_stack_size = 9) #80
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Magic Map', 'use')
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Scroll of Stasis', 'read', None, None, 9, max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spellbook of Stasis', 'read', None, None, 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Bucket', 'fill')
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Water Bucket', 'place(everywhere)','misc',0) #85
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Lava Bucket', 'place(everywhere)','global_caves',4)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Monster Egg', 'use',None,None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Life Essence', 'use',None,None,max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Copy of Ancient Text', 'use',None,None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sealed Envelope', 'use',None,None) #90
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Dirty Water Bucket', 'place(everywhere)','sewer',2)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Lost Tool Box', 'use',None,None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Transmitter', 'place(everywhere)','toys',0,max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Signal', 'place(everywhere)','toys',3,max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Switch', 'place(everywhere)','toys',5,max_stack_size = 5)#95
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Auto-Door', 'place(everywhere)','toys',6,max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Pressure Plate', 'place(everywhere)','toys',7,max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Fire Pit', 'place(everywhere)','toys',8,max_stack_size = 3)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Timed Emitter', 'place(everywhere)','toys',10,max_stack_size = 3)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Thinkerer\'s Workshop', 'place(indoor)','toys',11)#100
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Ancient Hammer', 'use',None,None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Neko Amulet', 'use',None,None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Chicken Nest', 'place(indoor)', 'functional', 43)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Pet Medicine', 'use',None,None,max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Evolution Stone', 'use',None,None) #105
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Pet Candy', 'use',None,None,max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Fence', 'place(outdoor)', 'toys', 13,max_stack_size = 16)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'White Flowers', 'plant', 'flower', 0, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Red Flowers', 'plant', 'flower', 1, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Blue Flowers', 'plant', 'flower', 2, max_stack_size = 9) #110
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Purple Flowers', 'plant', 'flower', 3, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Yellow Flowers', 'plant', 'flower', 4, max_stack_size = 9)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Menhir', 'place(everywhere)', 'deco', 10, max_stack_size = 5)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Enchantment Table', 'place(indoor)', 'functional', 48)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Planter', 'place(outdoor)', 'functional', 49, max_stack_size = 5) #115
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Simple Key', 'use',None,None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Shovel', 'dig hole','hole',0)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Red Carpet', 'place(indoor)', 'deco', 11, max_stack_size = 18, stack_size=9) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Blue Carpet', 'place(indoor)', 'deco', 12, max_stack_size = 18, stack_size=9) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Orange Carpet', 'place(indoor)', 'deco', 13, max_stack_size = 18, stack_size=9) #120
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Green Carpet', 'place(indoor)', 'deco', 14, max_stack_size = 18, stack_size=9) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Purple Carpet', 'place(indoor)', 'deco', 15, max_stack_size = 18, stack_size=9) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Beehive', 'place(everywhere)', 'functional', 51) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Wax', 'process', place_cat='wool', place_num=1, effect=125)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Candle', 'light', 'functional', 59, None, max_stack_size = 9) #125
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Fragments of Crystal', 'process', place_cat='ore', place_num=3, effect=127)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Cut-Glass Window', 'place(everywhere)', 'deco', 16,max_stack_size = 5) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Spinning Wheel', 'place(indoor)', 'functional', 53) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Tent', 'place(everywhere)', 'functional', 54) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Empty Sack', 'fill with resources', None, None) #130
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sack (10 wood inside)', 'unpack wood', None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sack (10 stone inside)', 'unpack stone', None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sack (5 ore inside)', 'unpack ore', None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sack (5 gems inside)', 'unpack gems', None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sack (10 herbs inside)', 'unpack herbs', None, None) #135
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sack (10 seeds inside)', 'unpack seeds', None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Sack (10 wool inside)', 'unpack wool', None, None)
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Fireplace', 'place(outdoor)', 'functional', 55) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Royal Trading Banner', 'place(everywhere)', 'functional', 57) 
		self.ilist['misc'].append(t)
		techID += 1
		t=item_misc(techID, 'Golden Key', 'use',None,None) #140
		self.ilist['misc'].append(t)
		techID += 1
		
		#food items

		t=item_food(techID, 'Red Berries', 200,600, 0, 0, 0, 0, 0, 0, 1, 1, 'You eat some sweet red berries.', cooking_result = 29)#0
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Blue Mushroom', 200, 1040, 0, 0, 0, 0, 0, 0, 4, 0, 'You eat a jucy blue mushroom.')#1
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Brown Mushroom', 800, 0, 0, 0, 0, 0, 0, 0, 4, 0, 'You eat a tasty brown mushroom.')#2
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Purple Mushroom', 200, -400, 800, 0, 0, 0, 0, 0, 4, 0, 'You eat a refreshing purple mushroom.')#3
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Fish', 600, 0, 0, 0, 0, 0, 0, 0, 1, 0, 'Bah! This is raw!.', cooking_result = 5)#4
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Grilled Fish', 1200, 0, 0, 0, 0, 0, 0, 0, 5, 0, 'Delicious!')#5
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Crops', 100, -400, 0, 0, 0, 0, 0, 0, False, 3, 'Dry and tasteless.', cooking_result = 7)#6
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Bread', 1200, 0, 0, 0, 0, 0, 0, 0, 6, 0, 'You eat a bread.', cooking_result = 8)#7
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Rusk', 1200, -400, 0, 0, 0, 0, 0, 0, False, 0, 'You eat some rusk.')#8
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Piece of Raw Meat', 700, 0, 0, 0, 0, 0, 0, 0, 2, 0, 'Bah! This is Raw!', cooking_result = 10)#9
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Piece of Grilled Meat', 1400, 0, 0, 0, 0, 0, 0, 0, 6, 0, 'Delicious!')#10
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Cultivated Mushroom', 200, 200, 0, 0, 0, 0, 0, 0, 6, 2, 'Tasteless...', cooking_result = 12)#11
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Grilled Mushroom', 600, 0, 0, 0, 0, 0, 0, 0, 6, 0, 'Delicious!')#12
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Potion of Healing', 0, 0, 0, 7, 0, 0, 0, 0, False, 0, 'You drink a potion of healing.', 'drink', cooking_result = 48)#13
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Potion of Feeding', 700, 0, 0, 0, 0, 0, 0, 0, False, 0, 'You drink a potion of feeding.', 'drink', cooking_result = 48)#14
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Potion of Refreshing', 0, 700, 0, 0, 0, 0, 0, 0, False, 0, 'You drink a potion of refreshing.', 'drink', cooking_result = 48)#15
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Potion of Vitalising', 0, 0, 700, 0, 0, 0, 0, 0, False, 0, 'You drink a potion of vitalising.', 'drink', cooking_result = 48)#16
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Strong Potion of Healing', 0, 0, 0, 15, 0, 0, 0, 0, False, 0, 'You drink a strong potion of healing.', 'drink', cooking_result = 48)#17
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Strong Potion of Feeding', 1400, 0, 0, 0, 0, 0, 0, 0, False, 0, 'You drink a strong potion of feeding.', 'drink', cooking_result = 48)#18
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Strong Potion of Refreshing', 0, 1400, 0, 0, 0, 0, 0, 0, False, 0, 'You drink a strong potion of refreshing.', 'drink', cooking_result = 48)#19
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Strong Potion of Vitalising', 0, 0, 1400, 0, 0, 0, 0, 0, False, 0, 'You drink a strong potion of vitalising.', 'drink', cooking_result = 48)#20
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Potion of Hunger', 0, 0, 0, 0, 100, 0, 0, 0, False, 0, 'Your stomach grows.', 'drink', cooking_result = 48)#21
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Potion of Thirst', 0, 0, 0, 0, 0, 100, 0, 0, False, 0, 'You feel like a camel.', 'drink', cooking_result = 48)#22
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Potion of Tiredness', 0, 0, 0, 0, 0, 0, 100, 0, False, 0, 'You feel like a owl.', 'drink', cooking_result = 48)#23
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Potion of Life', 0, 0, 0, 0, 0, 0, 0, 1, False, 0, 'You feel stronger now.', 'drink', cooking_result = 48)#24
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Jellyfish', 0, 0, 0, -2, 0, 0, 0, 0, False, 0, 'Ugh... That must have been poisoness.', cooking_result = 26)#25
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Grilled Jellyfish', 800, 0, 0, 0, 0, 0, 0, 0, False, 0, 'Tastes like chicken...')#26
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Blue Berries', 100,1000, 0, 0, 0, 0, 0, 0, 1, 1, 'You eat some juicy blue berries.', cooking_result = 30)#27
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Yellow Berries', 700,200, 0, 0, 0, 0, 0, 0, 1, 1, 'You eat some tasty yellow berries.', cooking_result = 31)#28
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Red Jelly', 300,200, 0, 0, 0, 0, 0, 0, False, 0, 'You eat some tasty red jelly.')#29
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Blue Jelly', 200,200, 0, 0, 0, 0, 0, 0, False, 0, 'You eat some tasty blue jelly.')#30
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Yellow Jelly', 800,100, 0, 0, 0, 0, 0, 0, False, 0, 'You eat some tasty yellow jelly.')#31
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Grilled Yellow Mushroom', 700 ,0 , 0, 0, 0, 0, 0, 0, False, 0, 'You have night vision now.', 'eat', effect='night vision', effect_duration=240 )#32
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Iron Weed', 0 ,0 , 0, 0, 0, 0, 0, 0, False, 0, 'Your skin feels stronger.', 'eat', effect='ironskin', effect_duration=30 )#33
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Blood Moss', 0 ,0 , 0, 0, 0, 0, 0, 0, False, 0, 'You feel the rage.', 'eat', effect='berserk', effect_duration=30 )#34
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Dried Fire Leaves', 0 ,0 , 0, 0, 0, 0, 0, 0, False, 0, 'You got fire resistant.', 'eat', effect='fire resistance', effect_duration=180 )#35
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Fire Leaves', 0 ,0 , 0, 0, 0, 0, 0, 0, 3, 0, 'You got fire resistant.', 'eat', effect='fire resistance', effect_duration=20, cooking_result = 35)#36
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Yellow Mushroom', 800, 0, 0, 0, 0, 0, 0, 0, 4, 0, 'You eat a tasty yellow mushroom.', 'eat', effect='night vision', effect_duration=40, cooking_result=32)#37
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Corn', 400,600, 0, 0, 0, 0, 0, 0, 3, 4, 'You eat tasty corn.', cooking_result = 39)#38
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Popcorn', 300,00, 0, 0, 0, 0, 0, 0, False, 0, 'You eat some sweet popcorn.')#39
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Apple', 300,100, 0, 1, 0, 0, 0, 0, 3, 1, 'You eat a tasty apple.', cooking_result = 41)#40
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Grilled Apple', 600,50, 0, 2, 0, 0, 0, 0, 7, 0, 'You eat a grilled apple.')#41
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Golden Apple', 600,200, 0, 2, 0, 0, 0, 0, 3, 1, 'You eat a tasty golden apple.', cooking_result = 43)#42
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Grilled Golden Apple', 900,100, 0, 5, 0, 0, 0, 0, 7, 0, 'You eat a grilled golden apple.')#43
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Egg', 300,0, 0, 0, 0, 0, 0, 0, 2, 0, 'You eat a egg.', cooking_result = 45)#44
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Omelette', 600,0, 0, 0, 0, 0, 0, 0, 0, 0, 'You eat a omelette.')#45
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Elfish Beer', 0 ,400 , 0, 0, 0, 0, 0, 0, False, 0, 'You feel drunk.', 'drink', effect='drunk', effect_duration=120, cooking_result = 49)#46
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Burned Food', 60,0, 0, 0, 0, 0, 0, 0, 0, 0, 'This tastes strange.')#47
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Strange Potion', 0, 60, 0, 0, 0, 0, 0, 0, False, 0, 'This tastes strange.', 'drink')#48
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Warm Elfish Beer', 0 ,400 , 0, 0, 0, 0, 0, 0, False, 0, 'You feel very drunk.', 'drink', effect='drunk', effect_duration=240 )#49
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Weak Potion of Healing', 0, 0, 0, 3, 0, 0, 0, 0, False, 0, 'You drink a weak potion of healing.', 'drink', cooking_result = 48)#50
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Weak Potion of Feeding', 400, 0, 0, 0, 0, 0, 0, 0, False, 0, 'You drink a weak potion of feeding.', 'drink', cooking_result = 48)#51
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Weak Potion of Refreshing', 0, 400, 0, 0, 0, 0, 0, 0, False, 0, 'You drink a weak potion of refreshing.', 'drink', cooking_result = 48)#52
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Weak Potion of Vitalising', 0, 0, 400, 0, 0, 0, 0, 0, False, 0, 'You drink a weak potion of vitalising.', 'drink', cooking_result = 48)#53
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Ghost Leaf', 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 'You can see invisible now.', 'eat', effect='see invisible', effect_duration=240, cooking_result = 56)#54
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Mummy Dust', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'You are invisible now.', 'eat', effect='invisible', effect_duration=30)#55
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Dried Ghost Leaf', 0, 0, 0, 0, 0, 0, 0, 0, False, 0, 'You can see invisible now.', 'eat', effect='see invisible', effect_duration=240)#56
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Spider Eye', 0, 0, 0, 0, 0, 0, 0, 0, False, 0, 'Bah.. This tastes poisones', 'eat', effect='poisoned', effect_duration=240, cooking_result = 58)#57
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Grilled Spider Eye', 0, 0, 0, 0, 0, 0, 0, 0, False, 0, 'You can see invisible now.', 'eat', effect='see invisible', effect_duration=240)#58
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Green Jelly', 0, 0, 0, 3, 0, 0, 0, 0, False, 0, 'You eat tasty green jelly.', 'eat')#59
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Purple Jelly', 0, 0, 0, -2, 0, 0, 0, 0, False, 0, 'Bah... That hurts!', 'eat',cooking_result=61)#60
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Medicine', 0, 0, 0, 0, 0, 0, 0, 0, False, 0, 'You feel very healthy.', 'eat', effect='healthy', effect_duration=120)#61
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Bitter Moss', 50, 50, 0, 0, 0, 0, 0, 0, False, 0, 'Tastes bitter...', 'eat', cooking_result=61)#62
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Watermelon', 50, 1040, 0, 0, 0, 0, 0, 0, 5, 5, 'Tasty and very jucy!')#63
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Junk Leafs', 0, 0, 200, 0, 0, 0, 0, 0, 2, 0, 'Tastes kinda strange.',cooking_result=65)#64
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Dried Junk Leafs', 0, 0, 1200, 0, 0, 0, 0, 0, False, 0, 'Tasteless... but adrenalizing!.')#65
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Glass Weed', 50, 50, 0, 0, 0, 0, 0, 0, 2, 5, 'This tastes strange.', effect='invisible', effect_duration=90)#66
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Burning Weed', 50, -500, 0, 0, 0, 0, 0, 0, 2, 5, 'You feel fire resistent.', effect='fire resistance', effect_duration=90, cooking_result=68)#67
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Burning Biscuits', 300, -250, 0, 0, 0, 0, 0, 0, False, 0, 'Sweet and hot...', effect='fire resistance', effect_duration=240)#68
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Fire Flower', 30, -600, 0, 0, 0, 0, 0, 0, 1, 4, 'Your body starts emitting heat.', effect='magmaskin', effect_duration=60, cooking_result=70)#69
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Fire Powder', 10, -250, 0, 0, 0, 0, 0, 0, False, 0, 'Your body starts emitting heat.', effect='magmaskin', effect_duration=240)#70
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Waterbag Mushroom', 0, 500, 0, 0, 0, 0, 0, 0, 6, 1, 'Very refreshing.', cooking_result = 52)#71
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Goo', 0 ,0 , 0, 0, 0, 0, 0, 0, False, 2, 'Bah.. this seems to be toxic!', 'eat', effect='poisoned', effect_duration=300, cooking_result=73)#72
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Black Jelly', 300,200, 0, 0, 0, 0, 0, 0, False, 0, 'You eat some strange black jelly.', effect='haste', effect_duration=240)#73
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Ironshroom', 200 ,100 , 0, 0, 0, 0, 0, 0, 2, 4, 'Your skin feels stronger.', 'eat', effect='ironskin', effect_duration=45, cooking_result=75)#74
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Dried Ironshroom', 200 ,-100 , 0, 0, 0, 0, 0, 0, False, 0, 'Your skin feels stronger.', 'eat', effect='ironskin', effect_duration=240 )#75
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Glowing Moss', 100 ,100 , 0, 0, 0, 0, 0, 0, 1, 1, 'Your skin starts to glow.', 'eat', effect='light', effect_duration=360, cooking_result=77)#76
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Dried Glowing Moss', 50 ,-100 , 0, 0, 0, 0, 0, 0, False, 0, 'Your skin starts to glow.', 'eat', effect='light', effect_duration=360 )#77
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Red Mushroom', 300 ,200 , 0, 0, 0, 0, 0, 0, 2, 3, 'You feel the rage.', 'eat', effect='berserk', effect_duration=60, cooking_result=79)#78
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Dried Red Mushroom', 200 ,-100 , 0, 0, 0, 0, 0, 0, False, 0, 'You feel the rage.', 'eat', effect='berserk', effect_duration=90)#79
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Milk', 150, 30, 0, 0, 0, 0, 0, 0, 1, 0, 'Fresh and tasty.', 'drink', cooking_result = 81)#80
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Cheese', 500, 0, 0, 0, 0, 0, 0, 0, False, 0, 'Hearty and delicious!.', 'eat')#81
		self.ilist['food'].append(t)
		techID += 1
		t=item_food(techID, 'Honey', 400, 0, 0, 0, 0, 0, 0, 0, False, 0, 'Nice and sweet!', 'eat')#82
		self.ilist['food'].append(t)
		techID += 1
		

		#clothing items
		
		t=decorative_clothing(techID,'Red Tunic','Clothing',(2,0),(2,1))#0
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Blue Tunic','Clothing',(0,0),(0,1))#1
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Green Tunic','Clothing',(1,0),(1,1))#2
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Neko Ears','Hat',(0,2),(0,2))#3
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Simple Hat','Hat',(0,3),(0,4))#4
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Orcish Helmet','Hat',(0,5),(0,5),override_hair=True)#5
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Eye Patch','Hat',(1,3),(1,4),override_hair=False)#6
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Woodlander Cloths','Clothing',(3,0),(3,1))#7
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Red Beard','Hat',(2,3),(2,3),override_hair=False)#8
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Black Beard','Hat',(2,4),(2,4),override_hair=False)#9
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'White Beard','Hat',(2,5),(2,5),override_hair=False)#10
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Miner\'s Helmet','Hat',(1,5),(1,5),override_hair=True)#11
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Horns','Hat',(3,2),(3,3))#12
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Demon Cloths 1','Clothing',(4,0),(5,0))#13
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Demon Cloths 2','Clothing',(4,1),(5,1))#14
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Red Cloak','Background',(4,2),(4,2))#15
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Green Cloak','Background',(4,3),(4,3))#16
		self.ilist['clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Blue Cloak','Background',(4,4),(4,4))#17
		self.ilist['clothe'].append(t)
		techID += 1
		
		#special clothes
		
		t=decorative_clothing(techID,'Dark Wings','Background',(2,2),(2,2))#0
		self.ilist['special_clothe'].append(t)
		techID += 1
		t=decorative_clothing(techID,'Light Wings','Background',(1,2),(1,2))#1
		self.ilist['special_clothe'].append(t)
		techID += 1
		
		#artefacts
		#0
		t=item_wear('talisman',16,0,known=True,suffix='Hunting',artefact=('Cylira\'s Talisman','cyliras_talisman'))
		self.ilist['artefact'].append(t)
		#1
		t=item_wear('ring',16,0,known=True,suffix='Life Extr.',artefact=('Ring of Life Extraction','ring_of_life_extraction'))
		self.ilist['artefact'].append(t)
		#2
		t=item_wear('amulet',16,0,known=True,suffix='See Ore',artefact=('Dwarfen Amulet','dwarfen_amulet'))
		self.ilist['artefact'].append(t)
		#3
		t=item_wear('pickaxe',20,0,known=True,suffix='Mining',artefact=('Miner\'s Pickaxe','miners_pickaxe'))
		self.ilist['artefact'].append(t)
		#4
		t=item_wear('ring',10,0,known=True,suffix='Drop Treas.',artefact=('Treasure Ring','treasure_ring'))
		self.ilist['artefact'].append(t)
