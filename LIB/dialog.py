#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

import os

class dialog():
	
	def __init__(self):
		
		d_path = os.path.dirname(os.path.realpath('main.py')) + os.sep + 'LIB' + os.sep + 'DIALOG' 
		
		fp = d_path + os.sep + 'dialog_util.py'
		fi = open(fp,"r")
		tl = fi.readlines()
		lnminus = str(len(tl)+10)
		fi.close()
		
		d_files = os.listdir(d_path)
		
		self.dlist = {}
		
		for i in d_files:
			
			if i.find('.py') != -1:
				full_path = d_path + os.sep + i
				f = open(full_path,"r")
				
				name = i.replace('.py','')
				text_list = f.readlines()
				
				pre_string = '\ntry:\n'
				post_string = '''\nexcept Exception as e:
    import traceback
    exc_type, exc_obj, exc_tb = sys.exc_info()
    s = traceback.extract_tb(exc_tb, limit=9)
    line = int(s[0][1] - *lnminus*) 
    error = s[0][3]
    message.add("ERROR: " + str(exc_type) + " in line " + str(line) + " of *code_name* ")'''
				
				text = pre_string
				
				longstring = False
				
				for i in range(0,len(text_list)):
							
					if longstring == False:
						string = '    '+text_list[i]
					else:
						string = text_list[i]
					
					text += string
					
					if text_list[i].find("'''") != -1:
						if longstring == False:
							longstring = True
						else:
							longstring = False
					
				text += post_string
				
				self.dlist[name] = text
				
				f.close()