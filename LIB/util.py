#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

try:
 	import cPickle as p
except:
 	import pickle as p

from preset_settings import *
from version import release_number
import os
from random import randint

try:
	from urllib import urlopen
except:
	from urllib.request import urlopen as urlopen

def save(world,player,time,gods,path,sep):
	#this function is called if the game is closed
	#it needs the world object, the player object, the time object, the basic_path and os.sep from the main programm
	
	if world != None:
		name1 = path + sep + 'world.data'
		
		f = open(name1, 'wb')
		p.dump(world,f,0)
		f.close()
	
	if player != None:
		name2 = path + sep + 'player.data'
		
		f = open(name2, 'wb')
		p.dump(player,f,0)
		f.close()
	
	if time != None:
		name3 = path + sep + 'time.data'
		
		f = open(name3, 'wb')
		p.dump(time,f,0)
		f.close()
	
	if gods != None:
		name4 = path + sep + 'gods.data'
		
		f = open(name4, 'wb')
		p.dump(gods,f,0)
		f.close()

def save_options(options,path,sep):
	
	name2 = path + sep + 'options.data'
	
	f = open(name2, 'wb')
	p.dump(options,f,0)
	f.close()

class game_options():
	
	def __init__(self,basic_path,home_save):
		
		if home_save == False:
			name = basic_path + os.sep + 'SAVE' + os.sep + 'options.data'
		else:
			name = os.path.expanduser('~') + os.sep + '.config' + os.sep + 'RogueBox-Adventures' + os.sep + 'SAVE' + os.sep + 'options.data'
		
		try:
			f = open(name, 'rb')
			temp = p.load(f)
			self.screenmode = temp.screenmode
			self.bgmmode = temp.bgmmode
			self.sfxmode = temp.sfxmode
			self.turnmode = temp.turnmode
			self.mousepad = temp.mousepad
			self.check_version = temp.check_version
			self.rendermode = temp.rendermode
			self.input_nomination = temp.input_nomination
			self.grit = temp.grit
		except:
			if screenmode == 'default':
				self.screenmode = 1 #0:windowed(small),1:windowed(big),2:fullscreen
			else:
				self.screenmode = screenmode
			if bgmmode == 'default':
				self.bgmmode = 1 #0:bgm off, 1:bgm on
			else:
				self.bgmmode = bgmmode
			if sfxmode == 'default':
				self.sfxmode = 1 #0:sfx off, 1:sfx on
			else:
				self.sfxmode = sfxmode
			if turnmode == 'default':
				self.turnmode = 1 #0:classic, 1:Semi-Real-Time
			else:
				self.turnmode = turnmode
			if mousepad == 'default':
				self.mousepad = 0 #0:mouse off, 1:mouse on
			else:
				self.mousepad = mousepad
			if version_check == 'default':
				self.check_version = 0 #0:check off 1:check on
			else:
				self.check_version = version_check
			if rendermode == 'default':
				self.rendermode = 0 #0:fancy 1:fast
			else:
				self.rendermode = rendermode
			if input_nomination == 'default':
				self.input_nomination = 0 #0:keyboard 1:joystick numeric 2:joystick(ABXY)
			else:
				self.input_nomination = input_nomination
			if grit == 'default':
				self.grit = 0 #0: grit off, 1: grit on
			else:
				self.grit = grit
				
			replace_string = os.sep + 'options.data'
			save_path = name.replace(replace_string,'')
			save_options(self,save_path,os.sep)

def check_version():
	#this function compares the release_number from version.py whit a number that from the URL https://rogueboxadventures.tuxfamily.org/version.htm
	#and returns: 'This version is up to date', 'Old version!!! Please update.', 'Can't reach server!'
	try:
		f = urlopen('https://rogueboxadventures.tuxfamily.org/version.htm')
		vers_test = int(f.read())
		
		if release_number ==  vers_test:
			return 'This version is up to date.'
		else:
			return 'Old version!!! Please update.'
	except:
			return 'Can\'t reach server!'

def enter_py_command():
	ui = input('>')
	try:
		exec(ui)
	except:
		'EXECUTION ERROR'

def name_generator(gender='male'):
	#this function generates a random name for a NPC
	num_syl = randint(1,2)
	vowels = ('a','e','i','o','u','y')#I know 'y' is no real vowel but names with a 'y' instad of an 'i' look more fantasy-like ;-)
	consonants = ('b','c','d','f','g','h','j','k','l','m','n','p','r','s','t','v','w','x','z')
	consonants2 = ('b','c','d','f','g','h','j','k','l','m','n','p','r','s','t','v','w','x','z','st','lm','ln','th','ph','ll','ss')
	male_ending = ('tos','tas','los','las','nor','nar','kop','kaz')
	female_ending =('la','ly','ni','nu','ry','ru','za','ca')
	neutral_ending = ('tos','tas','los','las','nor','nar','kop','kaz','la','ly','ni','nu','ry','ru','za','ca')
	special_symbols = ('\'','-')
	
	name = ''
	
	special_check = False
	
	name += consonants[randint(0,len(consonants)-1)]
	name += vowels[randint(0,len(vowels)-1)]
	
	for i in range(1,num_syl):
		name += consonants2[randint(0,len(consonants2)-1)]
		special = randint(0,9)
		if special < 2 and special_check == False:
			name += special_symbols[randint(0,len(special_symbols)-1)]
			special_check = True
		else:
			name += vowels[randint(0,len(vowels)-1)]
		
	if gender == 'male':
		name += male_ending[randint(0,len(male_ending)-1)]
	elif gender == 'female':
		name += female_ending[randint(0,len(female_ending)-1)]
	else:
		name += neutral_ending[randint(0,len(neutral_ending)-1)]
		
	name = name.capitalize()
	
	return name

def get_opposite_direction(x1,x2,y1,y2):
	#this function returns a table object thet points in the opposite direction like object1 to object2
	#o1->o2    return: <-
	result = [0,0]
	
	if x1 < x2:
		result[0] = 1
	elif x1 > x2:
		result[0] = -1
	
	if y1 < y2:
		result[1] = 1
	elif y1 > y2:
		result[1] = -1
		
	return result
