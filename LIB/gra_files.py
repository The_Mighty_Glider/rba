#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

import os
try:
	import pygame_sdl2 as pygame
except:
	import pygame

class g_files():
	
	def __init__(self):
		
		self.gdic = { 'tile1' : {} , 'char' : {} , 'display' : [] , 'built' : [], 'monster' : {} , 'monster_shadow' : {},  'num' : {}}
		
		b_path = os.path.dirname(os.path.realpath('gra_files.py'))
		basic_path = b_path.replace('/LIB','')
		
		t32_path = basic_path + os.sep + 'GRAPHIC' + os.sep + 'TILE32' + os.sep
		t1_path = basic_path + os.sep + 'GRAPHIC' + os.sep + 'TILE1' + os.sep
		char_path = basic_path + os.sep + 'GRAPHIC' + os.sep + 'CHAR' + os.sep
		display_path = basic_path + os.sep + 'GRAPHIC' + os.sep + 'DISPLAY' + os.sep
		built_path = basic_path + os.sep + 'GRAPHIC' + os.sep + 'BUILT' + os.sep
		monster_path = basic_path + os.sep + 'GRAPHIC' + os.sep + 'MONSTER' + os.sep
		num_path = basic_path + os.sep + 'GRAPHIC' + os.sep + 'NUM' + os.sep
		light_path = basic_path + os.sep + 'GRAPHIC' + os.sep + 'LIGHT' + os.sep
		border_path = basic_path + os.sep + 'GRAPHIC' +os.sep + 'BORDER' + os.sep
		portrait_path = basic_path + os.sep + 'GRAPHIC' +os.sep + 'PORTRAIT' + os.sep
		level_path = basic_path + os.sep + 'GRAPHIC' +os.sep + 'LEVEL' + os.sep
		
		#chars
		
		gender_list = {'MALE', 'FEMALE'}
		amo_list = ('helmet', 'armor', 'cuisse', 'shoes')
		weapon_list = ('knife' , 'sword' , 'axe', 'dagger', 'rune', 'wand', 'rune staff', 'artefact', 'pickaxe', 'ring', 'seal ring', 'talisman', 'amulet')
		weapon_list2 = ('knife' , 'sword' , 'axe', 'dagger', 'rune', 'wand', 'runestaff', 'artefact', 'pickaxe', 'ring', 'sealring', 'talisman', 'amulet')
		material_list = ('wooden', 'tin', 'bronze', 'steel', 'titan', 'magnicum')
		other_list = ('SKIN', 'HAIR')
		
		for h in gender_list:
			for j in amo_list:
				for k in material_list:
					
					key_string = h + '_' + k + '_'+ j
					load_string =  char_path + h + os.sep + 'ARMOR' + os.sep + k +'_' + j + '.png'
					test_string = char_path + h + os.sep + 'ARMOR' + os.sep + 'alt_' + k +'_' + j + '.png'
					if os.path.isfile(test_string):
						load_string = test_string
		
					i_name = load_string
					i = pygame.image.load(i_name)
					i.set_colorkey((255,0,255),pygame.RLEACCEL)
					i = i.convert_alpha()
					self.gdic['char'][key_string] = i
					
		for k in other_list:			
			for h in gender_list:
				for j in range (1,5):
				
					key_string = k + '_' + h + '_' + str(j)
					load_string =  char_path + h + os.sep + k + os.sep + str(j) + '.png'
					test_string = char_path + h + os.sep + k + os.sep + 'alt_' + str(j) + '.png'
					if os.path.isfile(test_string):
						load_string = test_string
	
					i_name = load_string
					i = pygame.image.load(i_name)
					i.set_colorkey((255,0,255),pygame.RLEACCEL)
					i = i.convert_alpha()
					self.gdic['char'][key_string] = i
					
		for g in gender_list:
			
			key_string = g + '_underwear'
			load_string =  char_path + g + os.sep + 'underwear.png'
			test_string = char_path + g + os.sep + 'alt_underwear.png'
			if os.path.isfile(test_string):
				load_string = test_string
				
			i_name = load_string
			i = pygame.image.load(i_name)
			i.set_colorkey((255,0,255),pygame.RLEACCEL)
			i = i.convert_alpha()
			self.gdic['char'][key_string] = i
					
		for h in range (0, len(weapon_list)):
			for j in material_list:
				 
				key_string = 'WEAPONS_' + j + '_' + weapon_list[h]
				load_string =  char_path + 'WEAPONS' + os.sep + j +'_' + weapon_list2[h] + '.png'
				test_string = 'alt_'+load_string
				if os.path.isfile(test_string):
					load_string = test_string
				i_name = load_string
				i = pygame.image.load(i_name)
				i.set_colorkey((255,0,255),pygame.RLEACCEL)
				i = i.convert_alpha()
				self.gdic['char'][key_string] = i
		
		artefact_path = char_path + os.sep + 'WEAPONS'+ os.sep + 'ARTEFACT' + os.sep
		a_files = os.listdir(artefact_path)
		
		for i in a_files:
			if i.find('.png') != -1:
				key_string = i.replace('.png','')
				a = pygame.image.load(artefact_path+i)
				a.set_colorkey((255,0,255),pygame.RLEACCEL)
				a = a.convert_alpha()
				self.gdic['char'][key_string] = a
		
		clothe_path = char_path + os.sep + 'CLOTHES' + os.sep + 'clothes.png'
		clothe_object = pygame.image.load(clothe_path)
		
		clothemap = []
		
		for i in range (0,11):
			clothemap.append([])
			for j in range (0,11):
				clothemap[i].append(0)
		
		for x in range (0,11):
			for y in range (0,11):
				
				j = pygame.Surface((32,32))
				j.blit(clothe_object,(-x*32,-y*32))
				j.set_colorkey((255,0,255),pygame.RLEACCEL)
				j = j.convert_alpha()
				clothemap[x][y] = j
				test_path = clothe_path + 'alt_'+str(x)+'_'+str(y)+'.png'
				if os.path.isfile(test_path):
					k = pygame.image.load(test_path)
					k = pygame.transform.scale(k,(32,32))
					k.set_colorkey((255,0,255),pygame.RLEACCEL)
					k = k.convert_alpha()
					tilemap[x][y] = k
		
		self.gdic['clothe'] = clothemap
				
#############################################################################

		#display stuff
		
		display_names = ('gui',#0
						'game_menu_bg',#1,
						'tab_unmarked',#2
						'tab_marked',#3
						'marker',#4
						'gui_transparent',#5
						'dark',#6
						'unknown_monster',#7
						'mouse_pad',#8
						'mouse_pad_fire',#9
						'gui_fire',#10
						'fire_path',#11
						'fire_path_monster',#12
						'miss',#13
						'hit',#14
						'critical',#15 
						'main_menu',#16
						'game_menu_bg_warning',#17 
						'marker_warning',#18
						'xp_bar',#19
						'prog_bar_empty',#20
						'prog_bar_full',#21
						'main_menu_low_res',#22
						'heal',#23
						'teleport',#24
						'gui_warning',#25
						'minus_gem',#26
						'plus_gem',#27
						'monster_peaceful',#28
						'monster_flee',#29
						'monster_hostile',#30
						'icon_bg_active',#31
						'icon_bg_passive',#32
						'icon_use_active',#33
						'icon_use_passive',#34
						'icon_magic_active',#35
						'icon_magic_passive',#36
						'icon_focus_active',#37
						'icon_focus_passive',#38
						'no_ring',#39
						'no_necklace',#40
						'choose_dir',#41
						'plus_fish',#42
						'plus_shoe',#43
						'no_axe',#44
						'no_pickaxe',#45
						'tool_active',#46
						'item_bar_empty',#47
						'item_bar_full',#48
						'game_menu_bg_alt_big',#49
						'game_menu_bg_alt_small',#50
						'monster_lvl_up',#51
						'no_melee',#52
						'no_magic',#53
						'no_helmet',#54
						'no_armor',#55
						'no_cuisse',#56
						'no_shoes',#57
						'monster_die',#58
						'attention',#59
						'found',#60
						'drop',#61
						'fear',#62
						'minus_blood',#63
						'plus_blood',#64
						'throw',#65
						'minus_item',#66
						'plus_item',#67
						'choose_dir_throw',#68
						'hourglass',#69
						'spellwave',#70
						'monster_company',#71
						'pet_txt',#72
						'minus_skill',#73
						'monster_blind',#74
						'zzz',#75
						'npc_txt',#76
						'grit',#77
						'page_up',#78
						'page_down',#79
						'warning_page_up',#80
						'warning_page_down',#81
						'boss_defeated',#82
						'bg_resource_sell',#83
						'resource_bar',#84
						'full',#85
						'overview_lvl_up',#86
						'hit_wall',#87
						'explode',#88
						)

		for c in display_names:
			i_name = display_path + c + '.png'
			test_string = display_path + 'alt_' + c + '.png'
			if os.path.isfile(test_string):
				i_name = test_string
			i = pygame.image.load(i_name)
			i.set_colorkey((255,0,255),pygame.RLEACCEL)
			i = i.convert_alpha()
			self.gdic['display'].append(i)

#############################################################################

		#numbers
		
		num_names = {'0','1','2','3','4','5','6','7','8','9','+'}
		for c in num_names:
			n_name = num_path + 'num' + c + '.png'
			test_string = num_path + 'alt_num' + c + '.png'
			if os.path.isfile(test_string):
				n_name = test_string
			n = pygame.image.load(n_name)
			self.gdic['num'][c] = n 

#############################################################################

		#light
		l_name = light_path + 'lightmap.png'
		test_string = 'alt_'+l_name
		if os.path.isfile(test_string):
			l_name = test_string
		l = pygame.image.load(l_name)
		self.gdic['lightmap'] = l
		
#############################################################################

		#tiles
		
		tile_path = t32_path + 'tiles.png'
		tile_object = pygame.image.load(tile_path)
		size = tile_object.get_rect().size
		x_tiles = int(size[1]/32)
		y_tiles = int(size[0]/32)
		
		shadow_object = pygame.image.load(tile_path)
		
		for y in range(0,size[1]):
			for x in range(0,size[0]):
				if shadow_object.get_at((x,y)) != (255,0,255,255):
					shadow_object.set_at((x,y),(48,48,48,255))
		
		tilemap = []
		shadowmap = []
		
		for i in range (0,y_tiles):
			tilemap.append([])
			shadowmap.append([])
			for j in range (0,x_tiles):
				tilemap[i].append(0)
				shadowmap[i].append(0)
		
		for x in range (0,y_tiles):
			for y in range (0,x_tiles):
				
				j = pygame.Surface((32,32))
				j.blit(tile_object,(-x*32,-y*32))
				j.set_colorkey((255,0,255),pygame.RLEACCEL)
				j = j.convert_alpha()
				tilemap[x][y] = j
				
				j = pygame.Surface((32,32))
				j.blit(shadow_object,(-x*32,-y*32))
				j.set_colorkey((255,0,255),pygame.RLEACCEL)
				j = j.convert_alpha()
				shadowmap[x][y] = j
				
				test_path = t32_path + 'alt_'+str(x)+'_'+str(y)+'.png'
				if os.path.isfile(test_path):
					k = pygame.image.load(test_path)
					k = pygame.transform.scale(k,(32,32))
					k.set_colorkey((255,0,255),pygame.RLEACCEL)
					k = k.convert_alpha()
					tilemap[x][y] = k
					
					k = pygame.image.load(test_path)
					k = pygame.transform.scale(k,(32,32))
					for y in range(0,32):
						for x in range(0,32):
							if k.get_at((x,y)) != (255,0,255,255):
								k.set_at((x,y),(48,48,48))
					k.set_colorkey((255,0,255),pygame.RLEACCEL)
					#k.set_alpha(128)
					k = k.convert_alpha()
					shadowmap[x][y] = k
						
		self.gdic['tile32'] = tilemap
		self.gdic['shadow_tile32'] = shadowmap
		
		###############################################################
		
		#border
		b_path = border_path + 'bordercolor.png'
		b_object = pygame.image.load(b_path)
		
		bordermap = []
		
		for i in range (0,y_tiles):
			bordermap.append([])
			for j in range (0,x_tiles):
				bordermap[i].append(0)
				
		for x in range (0,y_tiles):
			for y in range (0,x_tiles):
				try:
					color = b_object.get_at((x,y))
				except:
					color = (255,0,255,255)
					
				if color != (255,0,255,255):
					bordermap[x][y] = {'h' : pygame.Surface((32,2)), 'v' : pygame.Surface((2,32)), 'c' : pygame.Surface((2,2))}
					bordermap[x][y]['h'].fill(color)
					bordermap[x][y]['v'].fill(color)
					bordermap[x][y]['c'].fill(color)
				else:
					bordermap[x][y] = False
		
		self.gdic['border'] = bordermap
		
		nlist = ('n1','n2','s1','s2','e1','e2','w1','w2')
		mlist = ('n','w')
		olist = ('hn','hs','he','hw')
		plist = ('cn','cs','ce','cw') 
		qlist = ('ln','ls','le','lw')
		edgelist = {}
		
		for i in nlist:
			e_path = border_path + 'liquid_border_'+i+'.png'
			e_object = pygame.image.load(e_path)
			e_object.set_colorkey((255,0,255),pygame.RLEACCEL)
			e_object = e_object.convert_alpha()
			edgelist[i] = e_object
			
		for i in mlist:
			e_path = border_path + 'same_border_'+i+'.png'
			e_object = pygame.image.load(e_path)
			e_object.set_colorkey((255,0,255),pygame.RLEACCEL)
			e_object = e_object.convert_alpha()
			edgelist[i] = e_object
			
		for i in olist:
			e_path = border_path + 'hole_'+i+'.png'
			e_object = pygame.image.load(e_path)
			e_object.set_colorkey((255,0,255),pygame.RLEACCEL)
			e_object = e_object.convert_alpha()
			edgelist[i] = e_object
			
		for i in plist:
			e_path = border_path + 'carpet_'+i+'.png'
			e_object = pygame.image.load(e_path)
			e_object.set_colorkey((255,0,255),pygame.RLEACCEL)
			e_object = e_object.convert_alpha()
			edgelist[i] = e_object
			
		for i in qlist:
			e_path = border_path + 'light_'+i+'.png'
			e_object = pygame.image.load(e_path)
			e_object.set_colorkey((0,0,0),pygame.RLEACCEL)
			e_object = e_object.convert_alpha()
			edgelist[i] = e_object
		
		self.gdic['edge'] = edgelist
		
		###############################################################
		
		#portrait
		
		p_files = os.listdir(portrait_path)
		self.portraits = {}
		
		for i in p_files:
			if i.find('.png') != -1:
				s = pygame.image.load(portrait_path+i)
				s.set_colorkey((255,0,255),pygame.RLEACCEL)	
				s = s.convert_alpha()
				name = i.replace('.png','')
				self.portraits[name] = s
		
		###############################################################
		
		#level
		
		l_files = os.listdir(level_path)
		self.level = {}
		
		for i in l_files:
			if i.find('.png') != -1:
				s = pygame.image.load(level_path+i)
				#s.set_colorkey((255,0,255),pygame.RLEACCEL)	
				#s = s.convert_alpha()
				name = i.replace('.png','')
				self.level[name] = s
				
		self.level['demonic_keep'] = {}
		
		entrance_path = level_path + 'demonic_keep' + os.sep + 'entrance.png'
		s = pygame.image.load(entrance_path)
		self.level['demonic_keep']['entrance'] = s
		
		self.level['demonic_keep']['open'] = {}
		self.level['demonic_keep']['close'] = {}
		self.level['demonic_keep']['final'] = {}
		
		hl = ('open','close','final')
		
		for h in hl:
			dk_path = level_path + 'demonic_keep' + os.sep + h + os.sep
			dk_files = os.listdir(dk_path)
		
			for j in dk_files:
				if j.find('.png') != -1:
					s = pygame.image.load(dk_path+j)
					#s.set_colorkey((255,0,255),pygame.RLEACCEL)	
					#s = s.convert_alpha()
					name = j.replace('.png','')
					self.level['demonic_keep'][h][name] = s
		
		###############################################################
		
		#tile1
		
		color_list = ('light_brown','brown','light_blue','blue','light_red','red','light_purple','purple','light_green','green','light_yellow','yellow','light_grey','grey','black','white','white_stair')
		
		for c in color_list:
			
			i_name = t1_path + c + '.png'
			test_string = 'alt_'+i_name
			if os.path.isfile(test_string):
				i_name = test_string
			i = pygame.image.load(i_name)
			self.gdic['tile1'][c] = i
		
		###############################################################
		
		#built
		
		built_names = ('wall_false',#0
						'wall_true',#1
						'door_false',#2
						'door_true',#3
						'floor_false',#4
						'floor_true',#5
						'wall_over',#6
						'floor_over',#7
						'door_over',#8
						'remove',#9
						'stairup_true',#10
						'stairup_false',#11
						'stairdown_true',#12
						'stairdown_false',#13
						'agriculture_false',#14
						'agriculture_true',#15
						'agriculture_over',#16
						'place_false',#17
						'place_true',#18
						'remove_cursor')#19
		
		for c in built_names:
			i_name = built_path + c + '.png'
			test_string = 'alt_'+i_name
			if os.path.isfile(test_string):
				i_name = test_string
			i = pygame.image.load(i_name)
			i.set_colorkey((255,0,255),pygame.RLEACCEL)
			i = i.convert_alpha()
			self.gdic['built'].append(i)

		#######################################################
		
		#monsters
		
		m_path = monster_path + 'monsters.png'
		m_object = pygame.image.load(m_path)
		size = m_object.get_rect().size
		
		shadow_object = pygame.image.load(m_path)
		
		for y in range(0,size[1]):
			for x in range(0,size[0]):
				if shadow_object.get_at((x,y)) != (255,0,255,255):
					shadow_object.set_at((x,y),(48,48,48,255))
		
		x_tiles = int(size[0]/32)
		y_tiles = int(size[1]/32)
		
		tilemap = []
		shadowmap = []
		
		for i in range (0,y_tiles):
			tilemap.append([])
			shadowmap.append([])
			for j in range (0,x_tiles):
				tilemap[i].append(0)
				shadowmap[i].append(0)
		
		for x in range (0,y_tiles):
			for y in range (0,x_tiles):
				
				j = pygame.Surface((32,32))
				j.blit(m_object,(-x*32,-y*32))
				j.set_colorkey((255,0,255),pygame.RLEACCEL)
				j = j.convert_alpha()
				tilemap[x][y] = j
				
				j = pygame.Surface((32,32))
				j.blit(shadow_object,(-x*32,-y*32))
				j.set_colorkey((255,0,255),pygame.RLEACCEL)
				#j.set_alpha(128)
				j = j.convert_alpha()
				shadowmap[x][y] = j
				
				test_path = monster_path + 'alt_'+str(x)+'_'+str(y)+'.png'
				if os.path.isfile(test_path):
					k = pygame.image.load(test_path)
					k = pygame.transform.scale(k,(32,32))
					k.set_colorkey((255,0,255),pygame.RLEACCEL)
					k = k.convert_alpha()
					
					
					k = pygame.image.load(test_path)
					k = pygame.transform.scale(k,(32,32))
					for y in range(0,32):
						for x in range(0,32):
							if k.get_at((x,y)) != (255,0,255,255):
								k.set_at((x,y),(48,48,48))
					k.set_colorkey((255,0,255),pygame.RLEACCEL)
					#k.set_alpha(128)
					k = k.convert_alpha()
					shadowmap[x][y] = k
					
					tilemap[x][y] = k
		
		self.gdic['monster'] = tilemap
		self.gdic['monster_shadow'] = shadowmap
