#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

from copy import deepcopy

class quest():
	def __init__(self,name,info,status=0):
		
		self.name = name
		self.info = info
		self.status = status

class questlog():
	
	def __init__(self):
		
		self.log ={}
	
	def add_quest(self,quest):
		
		self.log[quest.name] = deepcopy(quest)
	
	def remove_quest(self,questname):
		
		try:
			del self.log[questname]
		except:
			print('ERROR: Could not remove quest.')
	
	def set_solved(self,questname):
		
		try:
			self.log[questname].status = 2
		except:
			print('ERROR: Unknown quest')
	
	def get_quests(self,status='unsolved'):
		
		hl ={}
		for i in self.log:
			if status == 'solved':
				if self.log[i].status == 2:
					hl[i] = self.log[i]
			else:
				if self.log[i].status != 2:
					hl[i] = self.log[i]
		if len(hl) == 0:
			hl['No Quests'] = quest('No Quests',(('No quests yet.'),))
			hl['No Quests'].status = 1
		return hl
