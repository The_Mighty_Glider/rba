#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

welcome = '''Can\'t you leave an old man alone with his pain?
But wait...
You are a wanderer, aren\'t you?
The heavens must have send you to help me!'''
explain1 = '''It\'s about my son...
I told him all this stupid stories...
And now his imortal soul is lost...'''
explain2 = '''My family takes care for this chicken farm for generations. Chicken meat is one of our most important foods, you know?
But this farm only hides our family secret...
Inside you will find some stairs leading down into an ancient tomb.
This tomb was already old when the first nekos came to this world. Nobody knows who was burried down there.
But this place is full of undead creatures! My family takes care that they never come up...'''
explain3 = '''I told my son all this stupid stories how I explored this tomb when I was young...
The true is... I steped down the first stairs only once and ran back to the surface immediately when I heared the sounds of the undead...
But my son belived the stories and went down there...
This was four weeks ago...
I\'m sure he didn\'t made it... How should he have survived this long?
I\'m such an old fool!!!
The worst thing is that my son\'s immortal soul is traped down there and his flesh is doomed to move eternal!
Can you go down there and give my son the eternal peace he deserves? And maybe bring something back that I can burry?'''
got_quest = 'The gods may bless you! I\'ll open the door.'
not_done = 'Please save my sons\'s soul!'
done = '''You returned!
And you bring my son\'s amulet?
... ... ...
This means you rescued his soul...
Thank you...
Please give me his amulet so I have at least something I can burry. I will give you some reward in exchange for it.'''

if not 'meet_old_neko' in player.quest_variables:
	longsay(name,None,welcome)
	ui = ask('How can I help?','Maybe later!',None)
	if ui == 1:
		longsay(name,None,explain1)
		ask('I don\'t understand.','Please tell me more.','What?!')
		longsay(name,None,explain2)
		ask('And how can I help you?','Okay. But whats the problem?','I am listening...')
		longsay(name,None,explain3)
		ask('I will take care of this!','I\'ll see what I can do.','Okay!')
		longsay(name,None,got_quest)
		player.quest_variables.append('meet_old_neko')
		screen.render_fade(True,False)
		screen.render_load(12)
		sfx.play('open')
		for y in range(0,max_map_size):
			for x in range(0,max_map_size):
				if world.maplist[mob_z][mob_on_map].tilemap[y][x].techID == tl.tlist['functional'][47].techID:
					world.maplist[mob_z][mob_on_map].tilemap[y][x] = deepcopy(tl.tlist['dungeon'][2])
		screen.render_fade(False,True)
		add_quest(ql.qlist['The old neko'])
	else:
		say(name,'I understand...',' ')
else:
	found_amulet = -1
	for i in range(0,len(player.inventory.misc)):
		if player.inventory.misc[i] != player.inventory.nothing:
			if player.inventory.misc[i].techID == il.ilist['misc'][102].techID:
				found_amulet = i
				
	if found_amulet != -1:
		longsay(name,None,done)
		player.inventory.misc[found_amulet] = deepcopy(il.ilist['misc'][70])
		sfx.play('got_item')
		say('He gives you a Book of Skill',' ',' ')
		screen.render_fade(True,False)
		screen.render_load(12)
		sfx.play('break')
		for y in range(0,max_map_size): #set neko grave
			for x in range(0,max_map_size):
				if world.maplist[mob_z][mob_on_map].tilemap[y][x].techID == tl.tlist['functional'][45].techID:
					world.maplist[mob_z][mob_on_map].tilemap[y][x] = deepcopy(tl.tlist['functional'][46])
		player.quest_variables.append('saved_neko_soul')
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].message = 'shop'
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
		screen.render_fade(False,True)
		solved_quest('The old neko')
	else:
		longsay(name,None,not_done)
