#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

text0 = '''Greetings wanderer!
I came to this world to find a place where my people can build a new mine.
This world is rich! I can feel this.
But it seems most of the ore and gems are on a deeper level.
If I could go down...
But wait!
Couldn\'t you create a down leading stair for me?'''
text1 ='Please built a stair down!'
text2 = '''You did it!
Now I can explore the deeper caves!
Please take this gems as reward!'''

if first_meet == True:
	longsay('Dwarfen Scout',None,text0)
	first_meet = False
	mem_string = 'first_meet = '+str(first_meet)
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
else:
	test = world.maplist[mob_z][mob_on_map].find_first(tl.tlist['functional'][1])
	if test == False:
		longsay('Dwarfen Scout',None,text1)
		add_quest(ql.qlist['Going down!'])
	else:
		longsay('Dwarfen Scout',None,text2)
		sfx.play('got_item')
		say('He gives you 5 gems',' ',' ')
		player.inventory.materials.gem += 5
		screen.render_fade(True,False)
		screen.render_load(12)
		sfx.play('walk_dry')
		pos = world.maplist[mob_z+1][mob_on_map].find_any(tl.tlist['global_caves'][0])
		world.maplist[mob_z+1][mob_on_map].npcs[pos[1]][pos[0]] = deepcopy(ml.mlist['special'][35])
		world.maplist[mob_z+1][mob_on_map].set_monster_strength(pos[0],pos[1],2)
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
		solved_quest('Going down!')
		screen.render_fade(False,True)
