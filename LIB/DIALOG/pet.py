#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	say('A '+world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' has hached!',' ','What do you like to do?')
	ui = ask('Keep it!','Dismiss it!',None)
	if ui == 1:
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name = screen.string_input('Give your new pet a name!',15)
		first_meet = False
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = 'first_meet = False;'
	else:
		message.add('The '+world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' has gone forever!')
		screen.write_hit_matrix(mob_x,mob_y,7)
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
		
if first_meet == False:
	if player.pet_pos != [mob_x,mob_y,mob_z] or player.pet_on_map != mob_on_map:
		if player.pet_pos != False and player.pet_on_map != False:
			test = pet_return(player.pet_pos[0],player.pet_pos[1],player.pet_pos[2],player.pet_on_map)
			if test == False:
				message.add('You can\'t adopt one more pet!')
		player.pet_pos = [mob_x,mob_y,mob_z]
		player.pet_on_map = mob_on_map
		player.pet_lp = world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].basic_attribute.max_lp
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].AI_style = 'company'
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].hunger = 0
		message.add(world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' follows you now.')
	elif player.pet_pos == [mob_x,mob_y,mob_z] and player.pet_on_map == mob_on_map:
		if player.buffs.get_buff('immobilized') == 0:
			if world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].hunger == 300:
				mes = world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name + ' seems to be hungry.'
				say(mes,' ',' ')
				ui = ask('Feed '+world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name,'Don\'t feed '+world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name,None)
				gave_food = False
				if ui == 1:
					name_list = []
					num_list = []
					for i in range(0,len(player.inventory.food)):
						if player.inventory.food[i] != player.inventory.nothing:
							if player.inventory.food[i].rotten == False and player.inventory.food[i].eat_name == 'eat':
								name_list.append(player.inventory.food[i].name)
								num_list.append(i)
					if len(name_list) == 0:
						message.add('You own no food you could give to '+world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+'.')
					elif len(name_list) == 1:
						sfx.play('eat')
						message.add('You give your last '+name_list[0]+' to '+world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+'.')
						world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].relation += 200
						world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].hunger = 0
						gave_food = True
						player.inventory.food[0] = player.inventory.nothing
					else:
						ui2 = screen.get_choice('Choose a food item!',name_list,True)
						if ui2 != 'Break':
							sfx.play('eat')
							message.add('You give your '+name_list[ui2]+' to '+world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+'.')
							world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].relation += 100
							world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].hunger = 0
							gave_food = True
							player.inventory.food[num_list[ui2]] = player.inventory.nothing
					
					if gave_food == False:
						message.add(world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' looks disappointed and returns home.')
						world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].relation = max(world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].relation-300,0)
						pet_return(mob_x,mob_y,mob_z,mob_on_map)
			
			else:
				if 'found_item' in world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].properties:
					item_list = []
					item_list.append(il.ilist['misc'][104])#pet medicine
					item_list.append(il.ilist['misc'][106])#pet candy
					
					ran2 = random.randint(0,len(item_list)-1)
					hc = container([item_list[ran2],])
					mes = world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' found something! ('+hc.items[0].name+')'
					test = hc.loot(0)
					if test == True:
						say(mes,' ',' ')
						world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].properties.remove('found_item')
			
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].relation += 1
			swap_places(mob_x,mob_y,mob_z,mob_on_map)
