#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

introduction = '''When I was a wanderer myself I was always out for adventure and seeking for treasures.
Now I take care for the treasury.
You can send items home to the Elysium whenever you like.
Do you want to know more?'''
bank_info1 = 'Furthermore I can store your coins for you. Do you like to hear more?'
bank_info2 = '''I can take care for your coins, so you won't loose them if you die.
Just give them to me. I will give them back whenever you need them.'''
bank1 = 'Do you like to store some coins?'
bank2 = '''You have %num% coins stored.
Do you like to have them back?'''
bank2 = bank2.replace('%num%',str(stored_coins))

if first_meet == True:
	name = name_generator()
	say('Seraph','Greetings! My name is '+name+'.','I am the treasurer of this place.')

	longsay(name,None,introduction)
	ui = ask('No','Yes',None)
	if ui == 2:
		say(name,'In order to send an item home:','Open your inventory and bring the')
		say(name,'marker over the item you like to send.',' ')
		say(name,'Now press ['+key_name['e']+'].','Next choose "manage" and then')
		say(name,'"send home".','We can store up to 7 items of every')
		say(name,'category (Equipment,Food,Misc)','at this elysium.')
	longsay(name,None,bank_info1)
	ui = ask('No','Yes',None)
	if ui == 2:
		longsay(name,None,bank_info2)

if first_meet == False:
	if stored_coins == 0:
		longsay(name,None,bank1)
		ui = ask('No','Yes',None)
		if ui == 2:
			if player.coins > 0:
				say(name,'I will take care for this!',' ')
				stored_coins = deepcopy(player.coins)
				player.coins = 0
				sfx.play('shop')
			else:
				say(name,'Sorry but you have nothing to store.',' ')
	else:
		longsay(name,None,bank2)
		ui = ask('No','Yes',None)
		if ui == 2:
			say(name,'Here is your money!',' ')
			player.coins += deepcopy(stored_coins)
			stored_coins = 0
			sfx.play('shop')

if first_meet == True:
	help_con = container([il.ilist['misc'][51]])
	say(name,'...',' ')
	say(name,'Oh... I almost forgot!','I have something for you.')
	test = help_con.loot(0)
	if test == True:
		sfx.play('got_item')
		say(name+' hands over some chalk',' ',' ')
		say(name,'This is magic chalk. You can use it to','write a magic word on the floor.')
		say(name,'Monsters can not pass this sign. This','can help you to escape dangerous')
		say(name,'situations.','But be careful! It will break if')
		say(name,'you use it to often.',' ')
		player.quest_variables.append('met_treasurer')
		first_meet = False
	else:
		say(name,'Seems your inventory is full!','Come back later, please.')
	

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)+';stored_coins = '+str(stored_coins)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
