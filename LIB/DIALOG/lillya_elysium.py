#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

welcome = 'Greetings my friend. I hope you are well.'
follow = '''You are looking for my company??
Sure, why not.'''

if player.pet_pos != [mob_x,mob_y,mob_z] or player.pet_on_map != mob_on_map:
	longsay('Lillya','Lillya_happy',welcome)
	ui = ask('Let\'s party up!','Goodbye',None)
	if ui == 1:
		longsay('Lillya','Lillya_surprise',follow)
		if player.pet_pos != False and player.pet_on_map != False:
			test = pet_return(player.pet_pos[0],player.pet_pos[1],player.pet_pos[2],player.pet_on_map)
			if test == False:
				mes = world.maplist[player.pet_pos[2]][player.pet_on_map].npcs[player.pet_pos[1]][player.pet_pos[0]].name + ' can\'t return to the elysium and stays here.'
				message.add(mes)
		player.pet_pos = [mob_x,mob_y,mob_z]
		player.pet_on_map = mob_on_map
		player.pet_lp = world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].basic_attribute.max_lp
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].AI_style = 'company'
		message.add(world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].name+' follows you now.')
elif player.pet_pos == [mob_x,mob_y,mob_z] and player.pet_on_map == mob_on_map:
	if player.buffs.get_buff('immobilized') == 0:
		swap_places(mob_x,mob_y,mob_z,mob_on_map)
