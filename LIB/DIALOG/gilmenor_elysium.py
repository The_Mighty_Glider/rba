#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

txt1 = '''It seems the wizard has left his home and gone into the desert all the way down to the dwarf bastion in order to find new victims...
We need to stop him before he will bring even more misery over this world...
I was able to listen on a conversation a few of his minions had when I was locked in this cage.
It looks like he\'s trying to create his very own army of animated statues and needs the life essence of the residents of the grasslands to do so.'''

say('Gilmenor','I\'m glad to see you made it back well.',' ','gilmenor_happy')
longsay('Gilmenor','gilmenor_serious',txt1)
say('Gilmenor','If we only had a chance to pass the','magic doors of the dwarf bastion and','gilmenor_think')
say('Gilmenor','stop the wizard\'s evil plan.',' ','gilmenor_think')
say('Gilmenor','BUT WAIT!','','gilmenor_happy')
say('Gilmenor','Maybe we should talk to Lillya.','She is a wanderer as well but most','gilmenor_happy')
say('Gilmenor','often you can find her in the library','at elfish fortress.','gilmenor_happy')
say('Gilmenor','I am sure she knows a way!',' ','gilmenor_happy')
say('Gilmenor','If she knows no way then there is none.',' ','gilmenor_think')
solved_quest('[Main] The Wizards Dungeon')
add_quest(ql.qlist['[Main] Ask Lillya'])
player.quest_variables.append('meet_lillya')
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].message = 'gilmenor_follow'
