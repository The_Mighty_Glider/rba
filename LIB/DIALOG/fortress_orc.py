#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

text1 = '''Oh stranger! I\'m happy to meet you again!
My people found a new home at this place. Some of us have even formed a brigade to patrol the streets. I\'m proud that there is almost no more crime because of this.
There is something I\'d like to give you stranger because you helped me when I was really in need for help.'''
text2 = 'Your inventory seems to be full. Please come back later.'

longsay('Orc',None,text1)

help_container = container([il.ilist['misc'][70],])
test = help_container.loot(0)
if test == True:
	sfx.play('got_item')
	say('The orc gives you a Book of Skill',' ',' ')
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].message = 'orc'
else:
	longsay('Orc',None,text2)
