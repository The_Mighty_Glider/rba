#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

empty = '''You ask where all the animals are?
Well, when I was a wanderer myself this place was full of tame monsters we kept as pets.
But this golden days are long gone...
Unfortunately none of the wanderers today seems to be very interested in pets, so this stables are empty at the moment.
But you can change this. Do you like to know more?'''

explain = '''Monsters hatch from eggs. The best place to find a monster egg is any deep dungeon.
If you have an egg bring it to the Elysium and put it into a nest box.
A tame monster will hatch from it.
You can take this pet monster with you on adventures, where it will help you in battles or otherwise.
A pet even can find some items for you!
If you take good care for your pet your relationship will grow and some day this monster can be evolved with an evolution stone and become even more powerfull.
You ask where you can get an evolution stone?
Well, this could be hard...
There is only one mine where evolution stones can been found, somewhere in the grasslands...
But this place is full uf orcs!
Anyway...
You can send your pet back to Elysium any time or even dismiss it if you open you inventory screen and go to the \'companion page\'.'''

if first_meet == True:
	name = name_generator('female')
	say('Seraphine','Greetings! My name is '+name+'.','Welcome in the stables!')

longsay(name,None,empty)
ui = ask('No','Yes',None)
if ui == 2:
	longsay(name,None,explain)

if first_meet == True:
	help_con = container([il.ilist['misc'][53]])
	say(name,'By the way I have a little present','for you.')
	test = help_con.loot(0)
	if test == True: 
		sfx.play('got_item')
		say(name+' hands over a camera',' ',' ')
		say(name,'This is a camera. You can use it to','take screenshots.')
		say(name,'This way you can keep all the good','moments with your pets forever.')
		say(name,'I hope you will enjoy this gift.','')
		player.quest_variables.append('met_petkeeper')
		first_meet = False
	else:
		say(name,'Seems your inventory is full!','Come back later, please.')

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
