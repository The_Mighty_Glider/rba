#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

text0 = '''By all gods... You are a wanderer! I need your help!
...
Sorry... I am too emotional.
Its just in my 200 years of life I never saw a wanderer.
To be honest, I always belived you were a farytale for young nagas...
The gods may have sent you to rescue us!'''

text1 = '''We nagas lived in a grotto not far from here for aeons.
We are peaceful creatures and just interested in the studies of magic and alchemy.
But a few weeks ago a strange sickness turned many of us into mindless monsters.
The few of us, that are still normal, are hiding inside the ruins of our former settlement and have send me to find someone who may help us.
...
There is one more thing you need to know.
The nagas used their magic to turn invisible. You will need the ability to SEE INVISIBLE if you want to find them.
Now hurry and save the other nagas!'''

text2 = '''You made it!
Thank you wanderer.
As a reward I have a little something for you.'''

text3 = '''Now I will follow the others to the elfish fortress.
The elfs there were so friendly to offer us asylum.
This place will become our new home.
Good bye! I hope we will meet again one day.'''

if not 'saved_all_nagas' in player.quest_variables:
	if not 'naga_rescue' in player.quest_variables:
		longsay(name,None,text0)
		ui = ask('Yes, they did!','Yes, but later.',None)
		if ui == 1:
			longsay(name,None,text1)
			player.quest_variables.append('naga_rescue')
			add_quest(ql.qlist['Naga Rescue'])
		else:
			say(name,'I will wait.',' ')
	else:
		say(name,'Go and rescue the other nagas!',' ')
else:
	longsay(name,None,text2)
	c = container([il.ilist['misc'][70],])
	test = c.loot(0)
	if test != False:
		sfx.play('got_item')
		say('She gives you a Book of Skill',' ',' ')
		longsay(name,None,text3)
		screen.render_fade(True,False)
		screen.render_load(12)
		sfx.play('portal')
		pos = world.maplist[0]['fortress_0_0'].find_any(tl.tlist['functional'][19])
		world.maplist[0]['fortress_0_0'].npcs[pos[1]][pos[0]-1] = deepcopy(ml.mlist['civilian'][7])
		world.maplist[0]['fortress_0_0'].set_monster_strength(pos[0]-1,pos[1],1)
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
		solved_quest('Naga Rescue')
		screen.render_fade(False,True)
	else:
		say(name,'Your inventory is full!','Please come back later.')
