#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

mes1 = 'To enlarge your room will cost %num% gems. Are you interested?'
mes2 = 'Congratulations! Your room has reached the maximum size.'
im_done = '''I\'m done!
Enjoy your bigger room!'''

if 'room2' in player.quest_variables:
	prize = 15
	qv = 'room3'
	plus = 8
elif 'room1' in player.quest_variables:
	prize = 10
	qv = 'room2'
	plus = 7
else:
	prize = 5
	qv = 'room1'
	plus = 6

if 'room3' in player.quest_variables:
	longsay(name,None,mes2)
else:
	mes1 = mes1.replace('%num%',str(prize))
	longsay(name,None,mes1)
	ui = ask('Yes','No',None)
	if ui == 1:
		if player.inventory.materials.gem - prize >= 0:
			player.inventory.materials.gem -= prize
			say(name,'Here we go!',' ')
			screen.render_fade(True,False)
			screen.render_load(12)
			sfx.play('craft')
		
			pos = (2,2)
		
			for y in range(pos[1],pos[1]+plus):
				for x in range(pos[0],pos[0]+plus):
					if world.maplist[mob_z][mob_on_map].tilemap[y][x].techID == tl.tlist['sanctuary'][1].techID:
						world.maplist[mob_z][mob_on_map].tilemap[y][x] = deepcopy(tl.tlist['sanctuary'][0])
		
			for y in range(pos[1]+1,pos[1]+plus-1):
				for x in range(pos[0]+1,pos[0]+plus-1):
					if world.maplist[mob_z][mob_on_map].tilemap[y][x].techID == tl.tlist['sanctuary'][0].techID:
						world.maplist[mob_z][mob_on_map].tilemap[y][x] = deepcopy(tl.tlist['building'][9])
						world.maplist[mob_z][mob_on_map].tilemap[y][x].roofed = False
			
			player.quest_variables.append(qv)
			screen.render_fade(False,True)
		
			longsay(name,None,im_done)
		else:
			say(name,'Sorry, you don\'t have enough gems.',' ')
