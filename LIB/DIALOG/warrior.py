#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	name = name_generator()
	say('Seraph','Hi! I\'m '+name+'. I protect','this elysium.')

say(name,'I can teach you how to fight monsters.','...')
say(name,'I mean real combat. Man versus beast.','Not this wimpy magic stuff...')
say(name,'Interested?',' ')
ui = ask('No','Yes',None)
if ui == 2:
	say(name,'You can identify hostile monsters by','the red dot over their head.')
	say(name,'The number below this dot represents','their level. Stronger monsters give')
	say(name,'good xp while monsters with a lower','level than yours give you no xp at all.')
	say(name,'To attack foes you just need to','bump into them.')
	say(name,'Your strength as well as your weapon','determine if you hit or miss.')
	say(name,'With a little bit of luck you\'ll be','able to do a critical hit for bonus')
	say(name,'damage.','Monsters can hit different parts of')
	say(name,'your body. Make always sure to wear','adequate armor to avoid damage.')

if first_meet == True:
	say(name,'I have something for a unexpirienced','fighter like you!')
	say(name,'It is a knife...','Nothing special but better then')
	say(name,'no weapon at all.','You can find it inside your chest!')
	player.quest_variables.append('met_warrior')
	first_meet = False

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
