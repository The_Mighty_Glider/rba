#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

door_text = 'You shall not pass without the right password!'

wizard_1 = 'Who are you who dares to interrupt me?'
wizard_2 = '''You little wanderer are here to stop me?
How amusing...'''
wizard_3 = 'My statues will kill you!'

longsay('Runed Door',None,door_text)
if 'dwarfish_password' in player.quest_variables:
	ui = ask('Say password','Leave',None)
	if ui == 1:
		sfx.play('open')
		for y in range(0,max_map_size):
			for x in range(0,max_map_size):
				if world.maplist[mob_z][mob_on_map].tilemap[y][x].move_group == 'temp_solid':
					world.maplist[mob_z][mob_on_map].tilemap[y][x].move_group = 'soil'
					
		world.maplist[mob_z][mob_on_map].tilemap[mob_y][mob_x] = deepcopy(tl.tlist['dungeon'][2])
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
		longsay('Wizard','wizard_angry',wizard_1)
		longsay('Wizard','wizard_neutral',wizard_2)
		longsay('Wizard','wizard_angry',wizard_3)
