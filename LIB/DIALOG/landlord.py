#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

say('Landlord','Welcome in my Tavern!', 'How can I help you?')
ui = ask('Give me a drink! [2 Coins]','Give me some food! [2 Coins]','Goodbye')

help_con = container([il.ilist['food'][46],il.ilist['food'][10]])

if ui == 1 or ui == 2:
	if player.coins >= 2:
		if ui == 1:
			test = help_con.loot(0)
		else:
			test = help_con.loot(1)
		if test == True:
			say('Landlord','Here you are! Enjoy!',' ')
			player.coins -= 2
			sfx.play('shop')
		else:
			say('Landlord','Your inventory seems to be full.',' ')
	else:
		say('Landlord','It seems you have not enough money...',' ')
		
elif ui == 3:
	say('Landlord','Goodbye my friend.',' ')
