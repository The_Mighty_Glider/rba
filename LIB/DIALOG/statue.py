#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

#all statues need a bump_mes variable and a vil_num variable

message.add(bump_mes)

if world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].techID == ml.mlist['statue'][4].techID:
	if 'find_gilmenor' in player.quest_variables:
		say('You should revive Cylira first!',' ',' ',portrait='cylira_petrified')
		add_quest(ql.qlist['Revive Cylira'])
	else:
		say('This must be a statue of Cylira.',' ',' ',portrait='cylira_petrified')

found_life_essence = -1

for i in range(0,len(player.inventory.misc)):
	if player.inventory.misc[i] != player.inventory.nothing:
		if player.inventory.misc[i].techID == il.ilist['misc'][40].techID:
			found_life_essence = i
			
if found_life_essence != -1:
	say('REANIMATION','Do you like to reanimate this statue','with one Heart-Shaped Crystal?')
	ui = ask('Yes','No',None)
	if ui == 1:
		sfx.play('aura')
		screen.write_hit_matrix(mob_x,mob_y,7)
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = deepcopy(ml.mlist['villager'][vil_num])
		world.maplist[mob_z][mob_on_map].set_monster_strength(mob_x,mob_y,mob_z)
		player.inventory.misc[found_life_essence].stack_size -= 1
		if player.inventory.misc[found_life_essence].stack_size < 1:
			player.inventory.misc[found_life_essence] = player.inventory.nothing
