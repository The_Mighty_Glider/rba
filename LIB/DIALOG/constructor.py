#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

found_hammer = '''Is this an ancient hammer?
This is awsome! With a thing like this I\'d be able to improve this elysium so much.
Would you like to give me this hammer?
I promise to enlarge your personal room if I\'m allowed to have this thing!'''

im_done = '''I\'m done!
Enjoy your bigger room!
For a little fee I would able to enlarge it some more.'''

if first_meet == True:
	name = name_generator()
	say('Seraph','Good day! My name is '+name+'.','I have built this elysium.')
	say(name,'Isn\'t it beautiful?',' ')
	say(name,'Of course your personal room is a bit','small...')
	say(name,'But with the right tools I would be','able to change this.')
	say(name,'Just come to me if you find something','suitable.')
	add_quest(ql.qlist['Tools needed!'],True)

pos_ancient_hammer = -1

for i in range(0,len(player.inventory.misc)):
	if player.inventory.misc[i] != player.inventory.nothing:
		if player.inventory.misc[i].techID == il.ilist['misc'][101].techID:
			pos_ancient_hammer = i
			
if pos_ancient_hammer != -1:
	longsay(name,None,found_hammer)
	ui = ask('Yes!','No!',None)
	if ui == 1:
		solved_quest('Tools needed!')
		say(name,'Thank you so much!',' ')
		say(name+' takes the hammer',' ',' ')
		player.inventory.misc[pos_ancient_hammer] = player.inventory.nothing
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].sprite_pos = (11,1)
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].message = 'constructor_enlarge'
		say(name,'Here we go!',' ')
		screen.render_fade(True,False)
		screen.render_load(12)
		sfx.play('craft')
		
		pos = (2,2)
		
		for y in range(pos[1],pos[1]+5):
			for x in range(pos[0],pos[0]+5):
				if world.maplist[mob_z][mob_on_map].tilemap[y][x].techID == tl.tlist['sanctuary'][1].techID:
					world.maplist[mob_z][mob_on_map].tilemap[y][x] = deepcopy(tl.tlist['sanctuary'][0])
		
		for y in range(pos[1]+1,pos[1]+4):
			for x in range(pos[0]+1,pos[0]+4):
				if world.maplist[mob_z][mob_on_map].tilemap[y][x].techID == tl.tlist['sanctuary'][0].techID:
					world.maplist[mob_z][mob_on_map].tilemap[y][x] = deepcopy(tl.tlist['building'][9])
		
		screen.render_fade(False,True)
		
		longsay(name,None,im_done)
else:
	say(name,'I can tell you how to build stuff.','Are you interested?')

	ui = ask('No','Yes',None)
	if ui == 2:
		say(name,'First you need some resources.','Most things are constructed from')
		say(name,'wood and stone, but for','agriculture you\'ll need some seeds.')
		say(name,'If you\'d like to start building stuff','press ['+key_name['b']+'].')
		say(name,'Next you can choose what to build.','Finally you can pick position and')
		say(name,'size of your construction.','Press ['+key_name['e']+'] to finish your work.')

if first_meet == True:
	say(name,'If you like to build things you\'ll','need some resources.')
	say(name,'I\'ve put two things in the chest','inside your room.')
	say(name,'The first item is a pickaxe!',' ')
	say(name,'With the pick you can break rocks','in order to obtain some stone.')
	say(name,'The second one is an axe!',' ')
	say(name,'This fine thing allows you to chop down','trees for wood.')
	say(name,'These are good tools!',' ')
	player.quest_variables.append('met_constructor')
	first_meet = False
	sfx.play('got_quest')

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
