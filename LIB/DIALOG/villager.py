#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	name = name_generator(gender)
	say(race,'Thank you for saving me!','I already thought I would die down here.')
	say(race,'My name is '+name+'.','')
	first_meet = False

say(name,'Would you help me to escape from here?',' ')
ui = ask('No','Yes',None)
if ui == 2:
	say(name,'Thank you so much! You are a real','hero!')
	say(name,'There should be a teleporter somewhere','on this floor. If we could reach it')
	say(name,'I can escape from here.','I will follow you.')
	
#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)+'; race ="'+race+'"; gender = "'+gender+'"'
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
