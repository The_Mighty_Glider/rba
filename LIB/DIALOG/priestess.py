#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

say('Priestess','The gods may be with you wanderer!',' ')
ui = ask('Please bless me. (3 Coins)','Goodbye',None)
if ui == 1:
	if player.coins >= 3:
		player.buffs.set_buff('blessed',1440,False)
		player.coins -= 3
		sfx.play('shop')
		say('Priestess','The gods blessed you!',' ')
	else:
		say('Priestess','Come back if you have something','you can donate to the gods.')
elif ui == 2:
	say('Priestess','Farewell wanderer.',' ')
