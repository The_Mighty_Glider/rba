#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.
found_key = -1

for i in range(0,len(player.inventory.misc)):
	if player.inventory.misc[i].name == il.ilist['misc'][116].name:
		found_key = i
if found_key != -1:
	sfx.play(sound)
	message.add('You unlock the locked door [- 1 simple key]')
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
	player.inventory.misc[found_key] = player.inventory.nothing
else:
	say('This door is locked!',' ',' ')
