#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

introduction = '''I am cooking some delicious food for all wanderers at this Elysium.
The art of cooking was something I already enjoyed when I traveled between the worlds myself.
Would you like to now more about the importance of good food?'''

if first_meet == True:
	first_meet = False
	name = name_generator('male')
	say('Seraph','Greetings! My name is '+name+'.',' ')

longsay(name,None,introduction)
ui = ask('No','Yes',None)
if ui == 2:
	say(name,'Eating and drinking are two of your','three basic needs.')
	say(name,'Before you ask:','The third one is sleeping.')
	say(name,'You can see your needs in the top left','coner of your screen.')
	say(name,'From left to right the icons show:','Health, Hunger, Thirst and Tiredness')
	say(name,'If the percentage of one of your needs','drops down on zero your health will')
	say(name,'start to fall as well.','If your health reaches zero you die.')
	say(name,'As you see: Eating and drinking helps','to keep your body strong and alive.')
	say(name,'You can find consumable things like','mushrooms, berries or meat almost')
	say(name,'everywhere.',' ')
	say(name,'Some things have interesting side','effects as well.')
	say(name,'Many kinds of food and even some magic','potions can be enhanced by cooking')
	say(name,'them inside a furnace.','It\'s always worth a try to put things')
	say(name,'in a furnace and see what happens.',' ')
	say(name,'One last important thing to know is','that most food items don\'t last')
	say(name,'forever.',' ')
	say(name,'If you take them with you they can','rot and become dangerous to eat!')

if last_meet != time.day_total:
	foods = [il.ilist['food'][41],il.ilist['food'][5],il.ilist['food'][10],il.ilist['food'][12],il.ilist['food'][45]]
	ran = random.randint(0,len(foods)-1)
	help_con = container(foods)
	test = help_con.loot(ran)
	if test == True:
		say(name,'I have freshly made something','especially for you.')
		sfx.play('got_item')
		say(name+' hands over a some food',' ',' ')
		say(name,'I hope you will enjoy it!',' ')

player.quest_variables.append('met_cook')
last_meet = time.day_total

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+ str(first_meet) + '; last_meet = ' + str(last_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
