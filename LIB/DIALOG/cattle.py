#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

first_line = 'swap places'

if last_interaction != time.day_total:
	if interaction_type == 'milking':
		first_line = 'milk animal'
	elif interaction_type == 'shearing':
		first_line = 'shear animal'
		
ui = ask(first_line,'butcher animal','Cancel')

if ui == 1:
	if first_line == 'milk animal':
		help_con = container([il.ilist['food'][80],])
		test = help_con.loot(0)
		if test == True:
			message.add('+[Milk]')
			sfx.play('pickup')
			last_interaction = time.day_total
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].properties.append('reset sprite')
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = deepcopy(world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x])
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].sprite_pos = (world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].sprite_pos[0],world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].sprite_pos[1]+1)
			if world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].upper_sprite != None:
				world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].upper_sprite = (world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].upper_sprite[0],world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].upper_sprite[1]+1)
		else:
			message.add('Your inventory is full!')
	elif first_line == 'shear animal':	
		sfx.play('scissors')
		player.inventory.materials.add('wool',wool_num)
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = deepcopy(world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x])
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].sprite_pos = (world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].sprite_pos[0],world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].sprite_pos[1]+1)
		if world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].upper_sprite != None:
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].upper_sprite = (world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].upper_sprite[0],world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].upper_sprite[1]+1)
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].properties.append('reset sprite')
		last_interaction = time.day_total
	else:
		swap_places(mob_x,mob_y,mob_z,mob_on_map)

if ui == 2:
	sfx.play('hit')
	world.maplist[mob_z][mob_on_map].monster_die(mob_x,mob_y,butcher =True)		

#now save the new memories
try:
	mem_string = 'interaction_type= "'+ interaction_type +'"; wool_num = '+ str(wool_num) + '; last_interaction = ' + str(last_interaction)
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
except:
	None
