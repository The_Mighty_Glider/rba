#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.
txt1 = '''Oh, you are back!
Please let me see Cyliras translation.'''

txt2 = '''Seems like there is secret password that was used by the dwarfes to open the gateway into their bastion in old times.
Here it is...'''

txt3 = '''I need to return to the elysium for now.
And I guess you know what to do as well.
Go to the dwarf bastion and stop the evil wizard.'''

if 'meet_lillya' not in player.quest_variables:
	say('Lillya','Oh hello...',' ','Lillya_surprise')
	say('Lillya','At the moment I am busy with my','studies. Please come back later.','Lillya_surprise')
elif 'meet_lillya' in player.quest_variables:
	found_trans = -1
	for i in range(0,len(player.inventory.misc)):
		if player.inventory.misc[i] != player.inventory.nothing:
			if player.inventory.misc[i].techID == il.ilist['misc'][90].techID:
				found_trans = i
	if found_trans != -1:
		longsay('Lillya','Lillya_happy',txt1)
		player.inventory.misc[found_trans] = player.inventory.nothing
		say('Lillya takes the envelope',' ',' ')
		longsay('Lillya','Lillya_glad',txt2)
		sfx.play('got_item')
		say('Lillya tells you the password',' ',' ')
		player.quest_variables.append('dwarfish_password')
		longsay('Lillya','Lillya_surprise',txt3)
		screen.render_fade(True,False)
		screen.render_load(12)
		sfx.play('portal')
		pos = world.maplist[0]['elysium_0_0'].find_first(tl.tlist['functional'][18])
		world.maplist[0]['elysium_0_0'].npcs[pos[1]][pos[0]] = deepcopy(ml.mlist['rescued'][3])
		world.maplist[0]['elysium_0_0'].set_monster_strength(pos[0],pos[1],1)
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
		screen.render_fade(False,True)
		solved_quest('[Main] Translsation')
		add_quest(ql.qlist['[Main] Showdown!'])
	elif first_meet == True:
		say('Lillya','What do you say? Gilmenor sends','you to ask for my help?','Lillya_glad')
		say('Lillya','Of course I will help you!','I am always happy to help Gilmenor.','Lillya_happy')
		say('Lillya','I am so glad!','I was already afraid that he has','Lillya_happy')
		say('Lillya','forgotten about me...',' ','Lillya_happy')
		say('Lillya','Oh sorry! Just ignore what I said.','You come because the wizard','Lillya_surprise')
		say('Lillya','from the grasslands?','He has entrenched himself in the dwarf','Lillya_surprise')
		say('Lillya','bastion?','Thats not easy...','Lillya_surprise')
		say('Lillya','But we have luck!','I have done some investigations','Lillya_happy')
		say('Lillya','and accidentally found an acient text','that may help us. But...','Lillya_glad')
		say('Lillya','It is written in very old runes of the','wood elf tribe...','Lillya_surprise')
		say('Lillya','I do not think that I will be able to','translate this.','Lillya_surprise')
		say('Lillya','But I know someone who could help us to','do it!','Lillya_happy')
		say('Lillya','Cylira the leader of the wood elf tribe','in mordern days.','Lillya_happy')
		solved_quest('[Main] Ask Lillya')
		add_quest(ql.qlist['[Main] The ancient Text'])
		first_meet = False
	have_text = False
	for i in range(0,len(player.inventory.misc)):
		if player.inventory.misc[i] != player.inventory.nothing:
			if player.inventory.misc[i].techID == il.ilist['misc'][89].techID:
				have_text = True
	if have_text == False and found_trans == -1:
		help_con = container([il.ilist['misc'][89],il.ilist['misc'][89]])
		test = help_con.loot(0)
		if test == True:
			sfx.play('got_item')
			say('Lillya hands you a copy of the text',' ',' ')
		else:
			say('Lillya','Please come back if you have some','free space in your inventory.','Lillya_surprise')
			say('Lillya','I will make a copy of the text','for you.','Lillya_surprise')
	if found_trans == -1:
		say('Lillya','Please bring this text to Cylira!','She lives in the grasslands.','Lillya_happy')
		world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = True

#now save the new memories
try:
	mem_string = 'first_meet = '+str(first_meet)
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
except:
	None
