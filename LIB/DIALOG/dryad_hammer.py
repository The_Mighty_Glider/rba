#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

first_greeting = '''Oh, hello wanderer!
You are wondering about this strange thing in my hands, aren\'t you?
I\'ve found it under some roots a few days ago. It was all covered with moss. I thought I could use it to squash this ugly bugs that appeared lately and started eating the bark of our loved trees.
But it seems like I\'m not a good fighter at all...
I've got an idea!
If you squash this bugs for me I\'ll give you this thing as reward.
Are you interested?''' 

number_bugs = 'There are still %num% bugs around. Please squash all of them!'

reward = '''You made it!
Thank you so much!
Here is your reward.'''

if first_meet == True:
	longsay(name,None,first_greeting)
	ui = ask('Of course!','Not yet.',None)
	if ui == 1:
		num_bug = 0
		while num_bug < 5:
			pos = world.maplist[mob_z][mob_on_map].find_any(tl.tlist['local'][0])
			dist =((pos[0]-mob_x)**2+(pos[1]-mob_y)**2)**0.5
			if dist > 7:
				world.maplist[mob_z][mob_on_map].npcs[pos[1]][pos[0]] = deepcopy(ml.mlist['desert'][2])
				world.maplist[mob_z][mob_on_map].set_monster_strength(pos[0],pos[1],mob_z)
				num_bug += 1
		say(name,'Thank you so much wanderer!',' ')
		first_meet = False
		add_quest(ql.qlist['The ancient hammer'])
	else:
		say(name,'Oh...Okay...','Maybe later...')
else:
	num_bugs = 0
	for y in range(0,max_map_size):
		for x in range(0,max_map_size):
			if world.maplist[mob_z][mob_on_map].npcs[y][x] != 0:
				if world.maplist[mob_z][mob_on_map].npcs[y][x].techID == ml.mlist['desert'][2].techID:
					num_bugs += 1
					
	if num_bugs == 0:
		longsay(name,None,reward)
		hc = container([il.ilist['misc'][101],])
		test = hc.loot(0)
		if test == True:
			sfx.play('got_item')
			say('She gives you an ancient hammer',' ',' ')
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = deepcopy(ml.mlist['overworld'][0])
			world.maplist[mob_z][mob_on_map].set_monster_strength(mob_x,mob_y,mob_z)
			solved_quest('The ancient hammer')
		else:
			say(name,'Your inventory is full!','Please come back later.')
	else:
		number_bugs = number_bugs.replace('%num%',str(num_bugs))
		if num_bugs == 1:
			number_bugs = number_bugs.replace('are','is')
			number_bugs = number_bugs.replace('bugs','bug')
		longsay(name,None,number_bugs)
	
mem_string = 'name = "'+name+'"; first_meet = '+str(first_meet)+';'
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
