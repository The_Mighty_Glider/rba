#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

mes0 ='''Greetings wanderer!
I'm the one that asked you to save our people from the grotto.
Thank you for your help again. We wouldn't have made it without your help.
Do you remember me?'''
mes1 = '''Anyway, I am a little concerned for our new home too....
I was examining the lokal library of this place. It has a awsome collection of arcane literature, that is famous in many worlds!
But suddenly a strong wave of negative energy striked me...
It's origin seems to be beneath this bookshelf.  When I did further investigations, I have found a draft of cool air comming from below the shelf.
I think it hides some secret passway.
Unfortunately I am not strong enough to move this thing. But maybe together we can move it.'''
mes3 = '''I was right!
Please wanderer find out what is down there!
But be careful! The evil energy comming from downstairs is overwhelming.'''

if first_meet == False:
	longsay('Naga',None,mes0)
	ui = ask('Sure','Not really.',None)
	if ui == 1:
		say('Naga','I am glad to hear this.',' ',None)
	else:
		say('Naga','Oh... Fine.',' ',None)
	longsay('Naga',None,mes1)
	ui2 = ask('Let\'s do it!','Later',None)
	if ui2 == 1:
		screen.render_fade(True,False)
		screen.render_load(12)
		sfx.play('move_boulder')
		world.maplist[mob_z][mob_on_map].tilemap[mob_y][mob_x+1] = deepcopy(tl.tlist['dungeon'][21])
		screen.render_fade(False,True)
		longsay('Naga',None,mes3)
		add_quest(ql.qlist['What is beneath the library?'])
		first_meet = True
else:
	say('Naga','Please! Be careful down there!',' ',None)

#now save the new memories
mem_string = 'first_meet = '+ str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
