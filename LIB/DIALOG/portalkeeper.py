#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	if name == 'Unknown':
		name = name_generator()
		say('Seraph','Hi again!','I forgot to introduce myself earlier')
		say('Seraph','this morning. Sorry!','My name is '+name+'.')
if first_meet == True:
	say(name,'I can open a portal for you.',' ')
else:
	say(name,'I keep an eye on this portal.',' ')

test_list = ('met_mage','met_warrior','met_petkeeper','met_constructor','met_crafter','met_treasurer','met_cook')
test1 = True
for i in test_list:
	if not i in player.quest_variables:
		test1 = False

if test1 == False:
	say(name,'I\'d recommend you to talk to everybody','at this elysium.')

if (player.inventory.wearing['Hold(R)'] == player.inventory.nothing and player.inventory.wearing['Hold(L)'] == player.inventory.nothing) and first_meet == True:
	say(name,'You really should equip your weapons.','Do you know how?')
	ui = ask('Yes','No',None)
else:
	say(name,'Don\'t forget, I can teach you how to','equip your items if you wish.')
	ui = ask('No','Yes',None)

if ui == 2:
	say(name,'First you need to press ['+key_name['i']+'] to open','your inventory.')
	say(name,'The first two pages show equipment.', 'you\'re wearing')
	say(name,'Page 3 shows the eqipment', 'you own but aren\'t wearing.')
	say(name,'Just move your cursor over the item','you\'d like to wear and press ['+key_name['e']+'].')
	say(name,'Next you have to pick "equip" and','viola! But be careful if you find')
	say(name,'items out there they can be cursed!',' ')
else:
	if first_meet == True:
		say(name,'Shall I open the portal now?',' ')
		ui = ask('Yes','No',None)
		if ui == 1:
			say(name,'Okay!','Here we go.')
			sfx.play('teleport')
			screen.render_fade(True,False)
			screen.render_load(12)
			center = int(max_map_size/2)
			world.maplist[mob_z][mob_on_map].tilemap[center-7][center] = deepcopy(tl.tlist['portal'][7])
			world.maplist[mob_z][mob_on_map].tilemap[center-7][center].replace = deepcopy(tl.tlist['sanctuary'][0])
			sleep(0.2)
			screen.render_fade(False,True)
			say(name,'I have another present for you.','I\'ve put it into your chest.')
			say(name,'A tunic. It is just decorative.','You can wear such things to change')
			say(name,'your look.','They have no effect on your attributes.')
			say(name,'But you shouldn\'t travel around naked.',' ')
			say(name,'Good luck on all your adventures!',' ')
			first_meet = False
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
