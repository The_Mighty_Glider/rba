#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.
intro1 = '''Let's see what we have here?!
If this isn't our intruder...'''

intro2='''I SHOULD TEAR YOUR HEAD OFF!
Do you have any idea what disaster your comming has caused?!'''

intro3 = '''...(calm down Karula... calm down...)...
...(It will help nobody if you get overwelmed by your feelings now.)...'''

intro4 = '''So...
So... welcome...
So... welcome... WANDERER!
This is my personal dungeon dimension. I am demonic queen Karula. The leader of this place.
... Your comming caused a lot of trouble to me... 
And maybe the outer worlds as well.'''

intro_interlude1 ='''Karula the elfish leader, that showed her people a new land?
...
Yes, this was me... in a former life.'''

intro5 = '''Short after opening the portal, that leads to this place, I realized what harm I may have unleashed.
Unfortunately, I wasn't able to close the portal once I opened it...
So,  had to stay at this place and cast a spell to make it unpassable for the inhabitants of this dimension.
To be able to hold up this spell I had to become their beloved demonic queen!
Unfortunately, when you stumbled into the portal beneath my old library, you disturbed the flow of magic for a short moment.
Long enough for... someone to escape.
You HAVE to right this deed!'''

intro_interlude2 = '''As a wanderer it is your task to protect the inhabitants of all worlds!
The one that escaped from this dimension is a treat to them!
... And also to herself... I fear...
I can't leave this place and take her back myself. Otherwise my spell would collapse and all monstrosities, that are  imprisoned here, will hit the other worlds as an unstoppable black flood!
You see, you do not have a choice!'''

intro_interlude3  = '''But if you insist not to execute my order, I can throw you into the darkest hole of this dimension and show you a kind of pain, that even a half devine being like you, can't imagine in it's wildest dreams!
Are we agreed now?
GOOD!'''

intro6 = '''Let me explain what I expect from you.
...
The one that escaped is a half-blood called Narasa.
This means she isn't a totally evil being like most of the other demons that are kept at this place.
Half-bloods emerge from the conjunction between demons and mortals. This means, while some demonic blood runs trouh her veins, she still feels and thinks more like a mortal soul.
I assume she will still be in the elfish settlement at the other side of the portal. Probably she has used a diguising spell to look like one of the elfes.'''

intro7 = '''Find her!
And bring her back!'''

intro8 ='''I will put my mark on you.
This should help you to see trough her disguise.
Just keep an eye for the small details.'''

intro9 ='''...
Just one more thing ...
Don't use violence to bring her back, if not absolutely necessary.
I don't want her to get hurt!'''

intro10 = 'Hurry now!'

longsay('???','Karula_happy',intro1)
longsay('???','Karula_angry',intro2)
longsay('???','Karula_sad',intro3)
longsay('???','Karula_happy',intro4)
ui = ask('Wait! That Karula?','What have I done?',None)
if ui == 1:
	longsay('Karula','Karula_sad',intro_interlude1)
longsay('Karula','Karula_happy',intro5)
ui2 = ask('But why me and not you?','What shall I do?',None)
if ui2 == 1:
	longsay('Karula','Karula_sad',intro_interlude2)
	longsay('Karula','Karula_angry',intro_interlude3)
longsay('Karula','Karula_happy',intro6)
longsay('Karula','Karula_angry',intro7)
longsay('Karula','Karula_happy',intro8)
longsay('Karula','Karula_sad',intro9)
longsay('Karula','Karula_happy',intro10)
solved_quest('What is beneath the library?')
add_quest(ql.qlist['A half-blood called Narasa'])
pos = world.maplist[0]['fortress_0_0'].find_any(tl.tlist['elfish'][1])
world.maplist[0]['fortress_0_0'].npcs[pos[1]][pos[0]] = deepcopy(ml.mlist['rescued'][5]) #spawn Narasa
world.maplist[0]['fortress_0_0'].set_monster_strength(pos[0],pos[1],1)
