#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

mes_help1 = 'Please safe me and my people from the evil wizard!'
mes_thanks1 = '''You defeated the wizard! Thank you wanderer!
Thake this as a little reward. '''
mes_help2 = 'Some of my people are still petrified. Can you please revive them?'
mes_thanks2 = '''Now my people are free again!
Thank you so much wanderer! You are the saviour of my folk!
We have walked the path of isolation far to long.
Now I decided that we need to have relations to other nations and other worlds again, like we had in acient times.
Maybe you will meet some of us at other places. When you travel the worlds from now on.'''
final = 'Hail to our saviour!'

found_wizard = False
found_statue = False

for y in range(0,max_map_size):
	for x in range(0,max_map_size):
		if world.maplist[mob_z][mob_on_map].npcs[y][x] != 0:
				if world.maplist[mob_z][mob_on_map].npcs[y][x].techID == ml.mlist['special'][25].techID:
					found_wizard = True
				elif world.maplist[mob_z][mob_on_map].npcs[y][x].techID == ml.mlist['statue'][1].techID:
					found_statue = True
					
if found_wizard == True:
	longsay('Dwarfen King',None,mes_help1)
elif found_statue == True and not 'saved_dwarfen_king' in player.quest_variables:
	longsay('Dwarfen King',None,mes_thanks1)
	hc = container([il.ilist['misc'][70],])
	test = hc.loot(0)
	if test == True:
		sfx.play('got_item')
		say('He gives you a Book of Skill',' ',' ')
		player.quest_variables.append('saved_dwarfen_king')
	else:
		say('Dwarfen King','Please come back when you have some','free space in your inventory.')
elif found_statue == True and 'saved_dwarfen_king' in player.quest_variables:
	longsay('Dwarfen King',None,mes_help2)
	add_quest(ql.qlist['Revive all dwarfes'])
elif found_statue == False and not 'saved_dwarfes' in player.quest_variables:
	longsay('Dwarfen King',None,mes_thanks2)
	player.quest_variables.append('saved_dwarfes')
	pos = world.maplist[0]['fortress_0_0'].find_any(tl.tlist['shop'][0])
	world.maplist[0]['fortress_0_0'].npcs[pos[1]][pos[0]] = deepcopy(ml.mlist['shop'][10])
	world.maplist[0]['fortress_0_0'].set_monster_strength(pos[0],pos[1],1)
	solved_quest('Revive all dwarfes')
else:
	longsay('Dwarfen King',None,final)
