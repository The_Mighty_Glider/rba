#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.
awake ='''Finally I can breath fresh air again!
Thank you wanderer! Me and my people never will forget what you have done for us!'''

save_people='''There are still some of my people who are petrified...
Can you revive them as well please?'''

thanks = '''All my people are alive again! Thank you so much!
Here is a litle reward for you to show you the gratefulness of us wood elves!'''

all_done = 'Greetings wanderer! You saved us all!'

translation1 = 'You say Lillya has found a text and she wants me to translate it?'

translation2 = 'This should be no problem. Please give the text to me.'


translation3 = '''...
... ...
... ... ...'''

translation4 = '''I am done!
Please bring this back to Lillya!'''

if first_meet == True:
	longsay('Cylira','cylira_happy',awake)
	solved_quest('Revive Cylira')
	first_meet = False

found_text = -1
for i in range(0,len(player.inventory.misc)):
	if player.inventory.misc[i] != player.inventory.nothing:
		if player.inventory.misc[i].techID == il.ilist['misc'][89].techID:
			found_text = i

if found_text == -1:
	found_statues = 0
	for y in range(0,max_map_size):
		for x in range(0,max_map_size):
			if world.maplist[mob_z][mob_on_map].npcs[y][x] != 0:
				if world.maplist[mob_z][mob_on_map].npcs[y][x].techID == ml.mlist['statue'][0].techID or world.maplist[mob_z][mob_on_map].npcs[y][x].techID == ml.mlist['statue'][3].techID:
					found_statues += 1
	if found_statues > 0:
		longsay('Cylira','cylira_serious',save_people)
		add_quest(ql.qlist['Revive all elves'])
	else:
		if world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state != 'None':
			longsay('Cylira','cylira_happy',thanks)
			ring = deepcopy(il.ilist['artefact'][0])
			c = container([ring,])
			test = c.loot(0)
			if test == True:
				sfx.play('got_item')
				say('Cylira gives you her talisman',' ',' ')
				world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
				solved_quest('Revive all elves')
				player.quest_variables.append('revived_woodland_elves')
			else:
				say('Cylira','Please come back if you have some','free space in your inventory.','cylira_serious')
		else:
			longsay('Cylira','cylira_happy',all_done)
else:
	longsay('Cylira','cylira_happy',translation1)
	longsay('Cylira','cylira_happy',translation2)
	say('Cylira takes the acient text',' ',' ')
	longsay('Cylira','cylira_serious',translation3)
	longsay('Cylira','cylira_happy',translation4)
	say('Cylira gives you a evelope',' ',' ')
	player.inventory.misc[found_text] = deepcopy(il.ilist['misc'][90])
	solved_quest('[Main] The ancient Text')
	add_quest(ql.qlist['[Main] Translsation'])

#now save the new memories
mem_string = 'first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
