#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

text0 = '''Oh, finally someone found me! Thank you wanderer.
But hurry up now!'''

text1 = 'There are still %num% more nagas hiding at this place.'
text2 = 'There is still one more naga hiding at this place.'
text3 = 'You saved all of us!'
if player.buffs.get_buff('see invisible') == 0:
	swap_places(mob_x,mob_y,mob_z,mob_on_map)
else:
	num_naga = -1
	longsay(name,None,text0)
	
	for y in range(0,max_map_size):
		for x in range(0,max_map_size):
			if world.maplist[mob_z][mob_on_map].npcs[y][x] != 0:
				if world.maplist[mob_z][mob_on_map].npcs[y][x].techID == ml.mlist['special'][31].techID or world.maplist[mob_z][mob_on_map].npcs[y][x].techID == ml.mlist['special'][32].techID:
					num_naga += 1

	if num_naga > 1:
		text1 = text1.replace('%num%',str(num_naga))
		longsay(name,None,text1)
	elif num_naga == 1:
		longsay(name,None,text2)
	elif num_naga == 0:
		longsay(name,None,text3)
		player.quest_variables.append('saved_all_nagas')
		pos = world.maplist[0]['fortress_0_0'].find_any(tl.tlist['shop'][0])
		world.maplist[0]['fortress_0_0'].npcs[pos[1]][pos[0]] = deepcopy(ml.mlist['shop'][2])
		world.maplist[0]['fortress_0_0'].set_monster_strength(pos[0],pos[1],1)
		
	screen.render_fade(True,False)
	screen.render_load(12)
	sfx.play('portal')
	
	num_resc = 0
	for y in range(0,max_map_size):
		for x in range(0,max_map_size):
			if world.maplist[0]['fortress_0_0'].npcs[y][x] != 0:
				if world.maplist[0]['fortress_0_0'].npcs[y][x].techID == ml.mlist['civilian'][4].techID or world.maplist[0]['fortress_0_0'].npcs[y][x].techID == ml.mlist['civilian'][6].techID:
					num_resc += 1
	if num_resc < 10:				
		pos = world.maplist[0]['fortress_0_0'].find_any(tl.tlist['elfish'][1])
		if name == 'Female Golden Naga':
			world.maplist[0]['fortress_0_0'].npcs[pos[1]][pos[0]] = deepcopy(ml.mlist['civilian'][4])
		else:
			world.maplist[0]['fortress_0_0'].npcs[pos[1]][pos[0]] = deepcopy(ml.mlist['civilian'][6])
		world.maplist[0]['fortress_0_0'].set_monster_strength(pos[0],pos[1],1)
	
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
	screen.render_fade(False,True)
