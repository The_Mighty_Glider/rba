#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.
text1 = '''Hi stranger!
My job is to maintain the sewer. Sadly an albino rat has stolen my tools last time I was down there.
Can you please bring them back?'''

text2 = 'Thank you so much! I need to go back to work now. Here is your reward.'

found_tools = -1
for i in range(0,len(player.inventory.misc)):
	if player.inventory.misc[i] != player.inventory.nothing:
		if player.inventory.misc[i].techID == il.ilist['misc'][92].techID:
			found_tools = i

if found_tools == -1:
	longsay(name,None,text1)
	player.quest_variables.append('find_albino_rat')
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
	add_quest(ql.qlist['The lost tool box'])
else:
	longsay(name,None,text2)
	player.quest_variables.append('killed_albino_rat')
	sfx.play('got_item')
	say('He gives you a Book of Skill',' ',' ')
	player.inventory.misc[found_tools] = deepcopy(il.ilist['misc'][70])
	screen.render_fade(True,False)
	screen.render_load(12)
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
	sleep(0.2)
	screen.render_fade(False,True)
	solved_quest('The lost tool box')
