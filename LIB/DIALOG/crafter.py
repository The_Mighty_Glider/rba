#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

if first_meet == True:
	name = name_generator('female')
	say('Seraphine','Hello! My name is '+name+'.','I am about to craft a few things.')

say(name,'I\'m sure you don\'t know how crafting','works. Wanna know?')

ui = ask('No','Yes',None)
if ui == 2:
	say(name,'Crafting stations need to be placed','inside buildings.')
	say(name,'There are several kinds of them that','can be used to make different things.')
	say(name,'Carpenter\'s workbenches allow you to','craft furniture and other workbenches.')
	say(name,'Carver\'s workbenches can be used to','make wooden weapons, armor and tools.')
	say(name,'Stonecutter\'s workbenches are used to','craft stone furniture.')
	say(name,'Forger\'s workbenches allow you to use','ore to make weapons, armor or tools.')
	say(name,'You can use Alchemist\'s workshops to','brew magic potions.')
	say(name,'But enough of this! There are other','important things to know!')
	say(name,'First: Things you craft will appear','in your inventory automatically')
	say(name,'as long you have a free slot for them.','If your inventory is full already the')
	say(name,'items are stored inside the crafting','station instead.')
	say(name,'You have to interact with it one more','time to take the items out.')
	say(name,'Second: If you want to make better','items you have to increase your')
	say(name,'crafting skills with a \'Book of Skill\'.','But these books are very rare.')

if first_meet == True:
	help_con = container([il.ilist['misc'][2]])
	say(name,'By the way I have a little something','for you!')
	say(name,'Ta-daa!',' ')
	test = help_con.loot(0)
	if test == True:
		sfx.play('got_item')
		say(name+' hands over a bed',' ',' ')
		say(name,'I have crafted this bed with my own','hands.')
		say(name,'You can place it inside a house and','sleep there if you are tired.')
		say(name,'No need to say thank you!','I just love to craft things!')
		player.quest_variables.append('met_crafter')
		first_meet = False
	else:
		say(name,'Seems your inventory is full!','Come back later, please.')

#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
