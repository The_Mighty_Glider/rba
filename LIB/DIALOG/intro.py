#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures. If not, see <http://www.gnu.org/licenses/>.

greeting_txt = '''Good morning! I\'m very happy to see you already on your feet. We have an important job for a wanderer like you.
But wait...
I know you just arrived and for sure you are a bit confused now.
Thats why I am here!
I will answer your questions if you have some.'''

i_txt = '''You are a wanderer.
Thats a creature created by the great gods themselves to protect the worlds they made.
You are able to use portals for traveling between the worlds.
You are also able and allowed to form the worlds by your will to a certain extent.
Like all wanderers you simply appeared at this place sleeping tonight.
We\'ve been a bit surprised by your coming so we had to improvise to give you a own room. But I have to admit you come at the right moment. The gods wisdom has to be endless!
Sadly I can\'t tell you were you come from. Wanderers just appear...
It\'s possible that today is the first day of your existence.'''

you_txt = '''I am a seraph.
Once we have been wanderers like you. But when our powers increased we started to change and became more godlike ourselves. Finally we decided to create this place... The Elysium to give new wanderers a home.
Since this time we don\'t travele to the worlds anymore in person. I think in the meanwhile some of the residents think of us as gods too.
Maybe you will reach the level of a seraph yourself one day. But until then we need you to take care for the worlds needs in our name.'''

job_txt = '''The wanderers here in the Elysium always had good relations to the tribe of the woodland elves and their leader Cylira.
But lately we didn\'t get any messages from there. So one of our wanderes traveled to the woodlands yesterday to see what\'s wrong.
He did not return yet.
We want you to go to the woodlands as well and see if you are able to help him. For sure the elves are upset by something... Sometimes they can be a little... difficult.
Take the time you need to prepare yourself and meet me inside the portal room if you are ready. I\'ll open a portal to the woodlands for you.'''

longsay('Seraph',None,greeting_txt)
run = True
while run:
	ui = ask('Who am I?','Who are you?','You talked about a job?')
	if ui == 1:
		longsay('Seraph',None,i_txt)
	elif ui == 2:
		longsay('Seraph',None,you_txt)
	elif ui == 3:
		run = False

longsay('Seraph',None,job_txt)
add_quest(ql.qlist['[Main] The lost Wanderer'])
screen.render_fade(True,False)
screen.render_load(12)
sfx.play('open')
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
screen.render_fade(False,True)
player.quest_variables.append('made_intro')