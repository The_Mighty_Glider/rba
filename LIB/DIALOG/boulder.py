#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

#1.) check direction
dirX = 0
dirY = 0

if player.pos[0] < mob_x:
	dirX = 1
elif player.pos[0] > mob_x:
	dirX = -1
if player.pos[1] < mob_y:
	dirY = 1
elif player.pos[1] > mob_y:
	dirY = -1

#2.) check if direction is free and move boulder

newX = mob_x + dirX
newY = mob_y + dirY

if world.maplist[mob_z][mob_on_map].npcs[newY][newX] == 0 and world.maplist[mob_z][mob_on_map].tilemap[newY][newX].move_group == 'soil' and world.maplist[mob_z][mob_on_map].tilemap[newY][newX].replace == None:
	#move boulder
	sfx.play('move_boulder')
	world.maplist[mob_z][mob_on_map].npcs[newY][newX] = deepcopy(world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x])
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
	message.add('You push the '+name+'.')
elif world.maplist[mob_z][mob_on_map].npcs[newY][newX] == 0 and world.maplist[mob_z][mob_on_map].tilemap[newY][newX].techID == tl.tlist['hole'][0].techID:
	#move boulder in hole
	sfx.play('boulder_drop')
	world.maplist[mob_z][mob_on_map].tilemap[newY][newX] = deepcopy(tl.tlist['hole'][1])
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
	message.add('You push the '+name+' into a hole.')
else:
	message.add('This way seems to be blocked.')
