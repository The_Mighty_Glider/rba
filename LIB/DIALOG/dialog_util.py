#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

def say(name,line1,line2,portrait=None):
	line0 = '['+name+']'
	if len(line0) > 39:
		line0 = line0[:36]+'.]'
	if len(line1) > 39:
		line1 = line1[:38]
	if len(line2) > 39:
		line2 = line2[:38]
	screen.render_request(line0,line1,line2,False,portrait)
	getch(screen.displayx,screen.displayy,0,0,mouse=game_options.mousepad)

def longsay(name,portrait,text):
	global say
	test = text.find('§')
	if test != -1:
		print('[ERROR] Found forbidden char (§)')
		return False
	st1 = text.split('\n')
	linelist = []
	for i in st1:
		i = i + ' '
		space_pos = []
		for j in range(0,len(i)):
			if i[j] == ' ':
				space_pos.append(j)
		if len(space_pos) > 0:
			cur_length = 38
			h1 = list(i)
			for k in range(0,len(space_pos)):
				if space_pos[k] > cur_length:
					h1[space_pos[k-1]] = '§'
					cur_length = space_pos[k-1] + 38
			h2 = ''.join(h1)
			st2 = h2.split('§')
			if len(st2)%2 != 0:
				st2.append(' ')
			for l in st2:
				linelist.append(l)
		else:
			if len(i) > 39:
				print('[ERROR] Found long word. (Max. 39 chars)')
				return False
			else:
				linelist.append(i)
				linelist.append(' ')
	
	for m in range(0,len(linelist),2):
		say(name,linelist[m],linelist[m+1],portrait)	

def ask(choice1,choice2,choice3):
	if choice1 != None:
		line1 = '['+key_name['e']+'] - '+choice1
	else:
		line1 = ' '
	if len(line1) > 39:
		print('[WARNING] Line 1 to long')
		
	if choice2 != None:
		line2 = '['+key_name['i']+'] - '+choice2
	else:
		line2 = ' '	
	if len(line2) > 39:
		print('[WARNING] Line 2 to long')
		
	if choice3 != None:
		line3 = '['+key_name['x']+'] - '+choice3
	else:
		line3 = ' '
	if len(line3) > 39:
		print('[WARNING] Line 3 to long')
	
	screen.render_request(line1,line2,line3,False)
	
	return_value = 0
	run = True

	while run:
		ui = getch(screen.displayx,screen.displayy,0,0,mouse=game_options.mousepad)
		
		if ui == 'e' and choice1 != None:
			return_value = 1
			run = False
		elif ui == 'i' and choice2 != None:
			return_value = 2
			run = False
		elif ui == 'x' and choice3 != None:
			return_value = 3
			run = False
			
	return return_value
	
def swap_places(x,y,z,on_map):
	try:
		test = world.maplist[z][on_map].npcs[y][x].ability['light']
	except:
		test = 'None'
	if test != 'None':
		if time.hour < 6 or time.hour > 19 or player.pos[2] > 0:
			player.buffs.set_buff('light',2,False)
			
	world.maplist[z][on_map].npcs[y][x].move_done = 1
	world.maplist[player.pos[2]][player.on_map].npcs[player.pos[1]][player.pos[0]] = world.maplist[z][on_map].npcs[y][x]
	world.maplist[z][on_map].npcs[y][x] = 0
	if player.pet_pos == [x,y,z] and player.pet_on_map == on_map:
		player.pet_pos = deepcopy(player.pos)
		player.pet_on_map = deepcopy(player.on_map)
	player.pos[0] = x
	player.pos[1] = y
	player.pos[2] = z
	player.on_map = on_map
	
def pet_return(x,y,z,on_map):
	
	if 'npc' in world.maplist[player.pet_pos[2]][player.pet_on_map].npcs[player.pet_pos[1]][player.pet_pos[0]].properties:
		seeked_tile = tl.tlist['functional'][18]
	else:
		seeked_tile = tl.tlist['sanctuary'][4]
		
	for yy in range(0,max_map_size):
		for xx in range(0,max_map_size):
			if world.maplist[0]['elysium_0_0'].tilemap[yy][xx].techID == seeked_tile.techID and world.maplist[0]['elysium_0_0'].npcs[yy][xx] == 0:
				string = world.maplist[z][on_map].npcs[y][x].name + ' returns home.'
				message.add(string)
				screen.write_hit_matrix(x,y,7)
				world.maplist[z][on_map].npcs[y][x].AI_style = 'ignore'
				world.maplist[0]['elysium_0_0'].npcs[yy][xx] = deepcopy(world.maplist[z][on_map].npcs[y][x])
				world.maplist[z][on_map].npcs[y][x] = 0
				player.pet_pos = False
				player.pet_on_map = False
				return True
	return False

def add_quest(quest,no_sound=False):
	try:
		test = player.questlog.log[quest.name]
	except:
		player.questlog.add_quest(quest)
		message.add('[NEW QUEST: '+quest.name+']')
		if no_sound != True:
			sfx.play('got_quest')

def solved_quest(name):
	try:
		player.questlog.set_solved(name)
		message.add('[QUEST SOLVED: '+name+']')
		#add sfx here!!!
	except:
		None