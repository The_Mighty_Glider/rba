#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.
if first_meet == False:
	if 'wizard_defeated' in player.quest_variables:
		say(name,'Thank you for defeating the wizard!',' ')
		if 'revived_woodland_elves' in player.quest_variables:
			say(name,'And be praised for reviving my','brothers and sisters!')
			say(name,'You are indeed a friend of our','tribe. Hail to you!')
		else:
			say(name,'Please revive my brothers and','sisters I miss them badly.')
	else:
		say(name,'Please kill the wizard and','save our tribe!')
		
elif first_meet == True:
	say(name,'Greetings Wanderer!','I am happy to see you.')
	run = True
	while run:
		if name == 'Wood Elf':
			ui = ask('Who are you?','What happened here?','Goodbye')
		else:
			ui = ask('Tell me about your people.','What happened here?','Goodbye')
		
		if ui == 1:
			if name == 'Wood Elf':
				name = name_generator()
				say('Wood Elf','My name is '+name+'.','I am a wood elf.')
			else:
				say(name,'My "people" like you call them live','at the surface of this world')
				say(name,'since its beginning.','We foster and protect this woods.')
				say(name,'Our leader is Cylira. She was choosen','because of her gender.')
				say(name,'It is a very rare event that a female','elf is born. It happens only once')
				say(name,'in 500 years.','An old prophecy says that the female')
				say(name,'born needs to become our next leader!',' ')
				say(name,'Usually our female children are dryads','who go and live in the woods as soon')
				say(name,'they are old enough. Only a few','of them decided to live inside')
				say(name,'walls because of love to an elf.',' ')
				say(name,'But normally our relationships do not','least for very long.')
				say(name,'Elves and Dryads are just too','different from each other.')
				say(name,'Male children become elfs like me.',' ')
				say(name,'We stay inside our halls and take care','for the orcs that would leave their')
				say(name,'caves otherwise to clear our woods!',' ')
				say(name,'But now I am afraid they will come out','soon if they find out what happened')
				say(name,'to us...',' ')
		elif ui == 2:
			say(name,'The nameless wizard happened...',' ')
			say(name,'He terrorised this world already for','a way too long time.')
			say(name,'When he arrived here he had built','this dungeon and gathered his evil')
			say(name,'minions down there.','Once in a time some of them came out')
			say(name,'for a raid.','But a few days ago the wizard himself')
			say(name,'attacked our halls at night.','He ripped out the life of my brothers')
			say(name,'and sisters and turned them into','statues. I think I am the only one')
			say(name,'who managed to escape.',' ')
			say(name,'Yesterday another wanderer arrived.','I think his name was Gilmenon')
			say(name,'or something like this...','I told him the whole story and he')
			say(name,'went into the dungeon to fight','the evil wizard.')
			say(name,'Sadly he did not return.','I think all of us underestimated')
			say(name,'this villain for far too long time!','I hope it is not to late to save my')
			say(name,'tribe.','Please hurry up and do something!')
			solved_quest('[Main] The lost Wanderer')
			add_quest(ql.qlist['[Main] The Wizards Dungeon'])
			first_meet = False
			player.quest_variables.append('find_gilmenor')
			world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].quest_state = 'None'
			run = False
		else:
			say(name,'Farewell',' ')
			run = False
			
#now save the new memories
mem_string = 'name= "'+ name +'"; first_meet = '+str(first_meet)
world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
