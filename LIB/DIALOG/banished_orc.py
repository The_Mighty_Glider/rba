#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

text1 = '''Hello stranger!
Don\'t worry I won't attack you. I was banished from our mine by my brothers. Now I\'m wandering around without purpose.
I\'m so hungry and even more thirsty!
Say stranger... can you bring me a beer please?'''
text2 = '''This elfish beer was very tasty!
But I\'m still so hungry. I need some fresh meat! Can you bring me some please?'''
text3 = '''That was very good!
But I still need some more food! A piece of bread between my fangs would be great now!'''
text4 = '''That was so good!
But stranger... Can I ask you for one more thing?
Can I have one more beer? But this time a warm one for my stomach.'''
text5 = '''I made a decision stranger!
I will go to the place where this tasty beer comes from! Maybe this can be a new home for me and for other banished orcs too.
Maybe we will meet again some day!'''

no_save = False

if quest_status == 0:
	longsay('Orc',None,text1)
	add_quest(ql.qlist['The banished Orc (Part 1)'])
	foodpos = -1
	for i in range(0,len(player.inventory.food)):
		if player.inventory.food[i] != player.inventory.nothing:
			if player.inventory.food[i].techID == il.ilist['food'][46].techID:
				if player.inventory.food[i].rotten == False:
					foodpos = i
	if foodpos != -1:
		ui = ask('Give beer to orc','Don\'t give beer to orc',None)
		if ui == 1:
			player.inventory.food[foodpos] = player.inventory.nothing
			quest_status = 1
			solved_quest('The banished Orc (Part 1)')

if quest_status == 1:
	longsay('Orc',None,text2)
	add_quest(ql.qlist['The banished Orc (Part 2)'])
	foodpos = -1
	for i in range(0,len(player.inventory.food)):
		if player.inventory.food[i] != player.inventory.nothing:
			if player.inventory.food[i].techID == il.ilist['food'][9].techID:
				if player.inventory.food[i].rotten == False:
					foodpos = i
	if foodpos != -1:
		ui = ask('Give meat to orc','Don\'t give meat to orc',None)
		if ui == 1:
			player.inventory.food[foodpos] = player.inventory.nothing
			quest_status = 2
			solved_quest('The banished Orc (Part 2)')
			
if quest_status == 2:
	longsay('Orc',None,text3)
	add_quest(ql.qlist['The banished Orc (Part 3)'])
	foodpos = -1
	for i in range(0,len(player.inventory.food)):
		if player.inventory.food[i] != player.inventory.nothing:
			if player.inventory.food[i].techID == il.ilist['food'][7].techID:
				if player.inventory.food[i].rotten == False:
					foodpos = i
	if foodpos != -1:
		ui = ask('Give bread to orc','Don\'t give bread to orc',None)
		if ui == 1:
			player.inventory.food[foodpos] = player.inventory.nothing
			quest_status = 3
			solved_quest('The banished Orc (Part 3)')
			
if quest_status == 3:
	longsay('Orc',None,text4)
	add_quest(ql.qlist['The banished Orc (Part 4)'])
	foodpos = -1
	for i in range(0,len(player.inventory.food)):
		if player.inventory.food[i] != player.inventory.nothing:
			if player.inventory.food[i].techID == il.ilist['food'][49].techID:
				if player.inventory.food[i].rotten == False:
					foodpos = i
	if foodpos != -1:
		ui = ask('Give beer to orc','Don\'t give beer to orc',None)
		if ui == 1:
			player.inventory.food[foodpos] = player.inventory.nothing
			quest_status = 4
			solved_quest('The banished Orc (Part 4)')

if quest_status == 4:
	longsay('Orc',None,text5)
	screen.render_fade(True,False)
	screen.render_load(12)
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x] = 0
	no_save = True
	
	pos = world.maplist[0]['fortress_0_0'].find_any(tl.tlist['shop'][0])
	world.maplist[0]['fortress_0_0'].npcs[pos[1]][pos[0]] = deepcopy(ml.mlist['shop'][1])
	world.maplist[0]['fortress_0_0'].set_monster_strength(pos[0],pos[1],1)
	
	world.maplist[0]['fortress_0_0'].make_special_monsters(10,15,tl.tlist['elfish'][1],5,'tame_orc')
	pos = world.maplist[0]['fortress_0_0'].find_first(tl.tlist['portal'][2])
	world.maplist[0]['fortress_0_0'].npcs[pos[1]-5][pos[0]] = deepcopy(ml.mlist['special'][22])
	world.maplist[0]['fortress_0_0'].set_monster_strength(pos[0],pos[1]-5,5)
	for y in range(0,max_map_size):
		for x in range(0,max_map_size):
			if world.maplist[0]['fortress_0_0'].npcs[y][x] != 0:
				if world.maplist[0]['fortress_0_0'].npcs[y][x].techID == ml.mlist['elfish_special'][4].techID:
					world.maplist[0]['fortress_0_0'].npcs[y][x] = deepcopy(ml.mlist['elfish_special'][6])
					world.maplist[0]['fortress_0_0'].set_monster_strength(x,y,5)
				if world.maplist[0]['fortress_0_0'].npcs[y][x].techID == ml.mlist['elfish_special'][5].techID:
					world.maplist[0]['fortress_0_0'].npcs[y][x] = deepcopy(ml.mlist['elfish_special'][7])
					world.maplist[0]['fortress_0_0'].set_monster_strength(x,y,5)
			
	screen.render_fade(False,True)

#now save the new memories
if no_save == False:
	mem_string = 'quest_status = '+str(quest_status)
	world.maplist[mob_z][mob_on_map].npcs[mob_y][mob_x].memory = mem_string
