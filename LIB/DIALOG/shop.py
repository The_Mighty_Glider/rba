#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

greeting_list = {
'naga' : 'Be welcome stranger! How may I serve you?',
'orc' : 'Hello! You like to see my wares?',
'fortress_elf' : 'Greetings stranger! I have the best wares from close and far. Wanna take a look?',
'pharmacy' : 'Greetings! You have a health problem? I will sell you a cure!',
'pickaxe' : 'Welcome to my world famous pickaxe emporium! How can I help you?',
'hardware' : 'Hello stranger! Are you looking for the right equipment? I can help you!',
'deco' : 'Hey stranger! Looking for a souvenir?',
'book' : 'Be welcome! I can sell you all kinds of arcane knowlege!',
'bomb' : 'Hey you! Wanna blow something up?',
'general' : 'Greetings! Welcome to my general store!',
'dwarf' : 'How may I serve you saviour of my people?',
'old_neko' : 'Thanks for all your help! All I can do is to sell you some items for a good price.',
'thinkerer' : 'Ah a customer! Come and see the wonders of science!',
'artefact' : 'Come closer and see the most extraordinaire artefacts!',
'farmer' : 'Howdy stranger! Take a look at my wares!'
}

bye_list ={
'naga' : 'It was a pleasure to trade with you!',
'orc' : 'Please come again and buy more.',
'fortress_elf' : 'Farewell stranger!',
'pharmacy' : 'Good bye and stay healthy!',
'pickaxe' : 'Enjoy digging!',
'hardware' : 'Good bye!',
'deco' : 'Have a nice day!',
'book' : 'Enjoy your day!',
'bomb' : 'Go and blow something up!',
'general' : 'Good bye and have a nice day!',
'dwarf' : 'Farewell wanderer.',
'old_neko' : 'May we meet again soon.',
'thinkerer' : 'Good bye customer!',
'artefact' : 'Farwell!',
'farmer' : 'Good bye stranger!'
}

shop_list = {
'naga' : ('Scroll of Return','Enchanted Enhancement Powder','Bitter Moss','Fire Leaves', 'Grilled Fish', 'Scroll of Light'),
'orc' : ('Scroll of Return','Bomb','Crystal Orb','Blood Moss', 'Grilled Meat', 'Torch'),
'fortress_elf' : ('Scroll of Return','Mysterious Blue Crystal','Chalk','Potion of Healing','Red Berries','Torch'),
'pharmacy' : ('Bandage','Eyedrops','Antidote','Holy Water','Medicine', 'Potion of Healing', 'Strong Potion of Healing'),
'pickaxe' : ('Wooden Pickaxe','Tin Pickaxe','Bronze Pickaxe','Steel Pickaxe', 'Titan Pickaxe', 'Magnicum Pickaxe'),
'hardware' : ('Steel Helmet','Steel Armor','Steel Cuisse','Steel Shoes', 'Steel Sword', 'Steel Rune Staff'),
'deco' : ('Bonsai','Houseplant','Pendolum Clock','Hourglass', 'Sarcophagus', 'Candleholder'),
'book' : ('Spellbook of Light','Spellbook of Flames','Scroll of Identify','Scroll of Repair', 'Spellbook of Teleport', 'Spellbook of Healing'),
'bomb' : ('boMb','eXplozive','BriMstone','HeAtet stOne', 'darT', 'Tork'),
'general' : ('Scroll of Return','Magic Map','Wooden Pickaxe','Wooden Axe', 'Grilled Fish', 'Torch'),
'dwarf' : ('Thinkerer\'s Workshop','Dwarfen Amulet','Miner\'s Pickaxe','Treasure Ring','Titan Pickaxe','Rusk','Torch'),
'old_neko' : ('Chicken Nest','Egg','Omelette','Grilled Meat','[D] Neko Ears'),
'thinkerer' : ('Thinkerer\'s Workshop','Transmitter','Switch','Pressure Plate','Auto-Door','Signal','Fire Pit'),
'artefact' : ('Cylira\'s Talisman','Ring of Life Extraction','Dwarfen Amulet','Miner\'s Pickaxe','Treasure Ring'),
'farmer' : ('Chicken Nest', 'Crops','Corn','Egg','Houseplant','Bonsai', '[D] Neko Ears')
}

axe = item_wear('axe',0,0)

pickaxe1 = item_wear('pickaxe',0,0)
pickaxe2 = item_wear('pickaxe',6,0)
pickaxe3 = item_wear('pickaxe',11,0)
pickaxe4 = item_wear('pickaxe',15,0)
pickaxe5 = item_wear('pickaxe',18,0)
pickaxe6 = item_wear('pickaxe',20,0)

item1 = item_wear('helmet',15,0)
item2 = item_wear('armor',15,0)
item3 = item_wear('cuisse',15,0)
item4 = item_wear('shoes',15,0)
item5 = item_wear('sword',15,0)
item6 = item_wear('rune staff',15,0)

item_list = {
'naga' : (il.ilist['misc'][33],il.ilist['misc'][42],il.ilist['food'][62],il.ilist['food'][36],il.ilist['food'][5],il.ilist['misc'][45]),
'orc' : (il.ilist['misc'][33],il.ilist['misc'][24],il.ilist['misc'][54],il.ilist['food'][34],il.ilist['food'][10],il.ilist['misc'][44]),
'fortress_elf' : (il.ilist['misc'][33],il.ilist['misc'][41],il.ilist['misc'][51],il.ilist['food'][13],il.ilist['food'][0],il.ilist['misc'][44]),
'pharmacy' : (il.ilist['misc'][74],il.ilist['misc'][75],il.ilist['misc'][76],il.ilist['misc'][77],il.ilist['food'][61],il.ilist['food'][13],il.ilist['food'][17]),
'pickaxe' : (pickaxe1,pickaxe2,pickaxe3,pickaxe4,pickaxe5,pickaxe6),
'hardware' : (item1,item2,item3,item4,item5,item6),
'deco' : (il.ilist['misc'][66],il.ilist['misc'][67],il.ilist['misc'][60],il.ilist['misc'][63],il.ilist['misc'][65],il.ilist['misc'][62]),
'book' : (il.ilist['misc'][46],il.ilist['misc'][36],il.ilist['misc'][25],il.ilist['misc'][27],il.ilist['misc'][32],il.ilist['misc'][30]),
'bomb' : (il.ilist['misc'][24],il.ilist['misc'][72],il.ilist['misc'][73],il.ilist['misc'][71],il.ilist['misc'][79],il.ilist['misc'][44]),
'general' : (il.ilist['misc'][33],il.ilist['misc'][81],pickaxe1,axe,il.ilist['food'][5],il.ilist['misc'][44]),
'dwarf' : (il.ilist['misc'][100],il.ilist['artefact'][2],il.ilist['artefact'][3],il.ilist['artefact'][4],pickaxe5,il.ilist['food'][8],il.ilist['misc'][44]),
'old_neko' : (il.ilist['misc'][103],il.ilist['food'][44],il.ilist['food'][45],il.ilist['food'][10],il.ilist['clothe'][3]),
'thinkerer' : (il.ilist['misc'][100],il.ilist['misc'][93],il.ilist['misc'][95],il.ilist['misc'][97],il.ilist['misc'][96],il.ilist['misc'][94],il.ilist['misc'][98]),
'artefact' : (il.ilist['misc'][0],il.ilist['misc'][1],il.ilist['misc'][2],il.ilist['misc'][3],il.ilist['misc'][4]),
'farmer' : (il.ilist['misc'][103],il.ilist['food'][6],il.ilist['food'][38],il.ilist['food'][44],il.ilist['misc'][67],il.ilist['misc'][66],il.ilist['clothe'][3])
}

price_list = {
'naga' : (3,7,4,2,2,1),
'orc' : (3,1,7,2,2,1),
'fortress_elf' : (3,3,5,4,1,1),
'pharmacy' : (4,4,4,4,4,4,6),
'pickaxe' : (2,5,8,14,23,42),
'hardware' : (14,14,14,14,14,14),
'deco' : (4,4,2,2,2,2),
'book' : (25,25,5,5,25,25),
'bomb' : (1,1,1,1,1,1),
'general' : (3,15,2,2,2,1),
'dwarf' : (20,25,25,25,23,2,1),
'old_neko' : (15,1,2,2,20),
'thinkerer' : (20,20,20,20,20,20,20),
'artefact' : (25,25,25,25,25),
'farmer' : (15,1,1,1,2,2,20)
}

try:
	longsay(name,None,greeting_list[shop_style])
except:
	shop_style = 'general'
	longsay(name,None,greeting_list[shop_style])
	
ui = ask('Show me your wares!','I want to trade resouces!','Farewell')

if ui == 1:
	final_shop_list = []
	for i in range(0,len(shop_list[shop_style])):
		final_shop_list.append(shop_list[shop_style][i] + ' (' + str(price_list[shop_style][i]) + ' coins)')
	run = True
	
	while run:
		headline = 'You have ' + str(player.coins) + ' coins. Please choose an item.'
		choice = screen.get_choice(headline,final_shop_list,True)
		if choice != 'Break':
			ui2 = ask('Buy!','Info','Chancel')
			if ui2 == 1:
				item = deepcopy(item_list[shop_style][choice])
				price = deepcopy(price_list[shop_style][choice])
				ui3 =1
				try:
					if item.max_stack_size > 1:
						ui3 = ask('Buy 1 for ' + str(price) + ' coins.', 'Buy ' + str(item.max_stack_size) + ' for ' + str(price*item.max_stack_size)+ ' coins','Chancel')
				except:
					None
					
				if ui3 != 3:
					if ui3 == 2:
						item.stack_size = item.max_stack_size
						price *= item.max_stack_size
					if player.coins >= price:
						help_container = container([item,])
						test = help_container.loot(0)
						if test == True:
							player.coins -= price
							sfx.play('shop')
							say(name,'Thanks for your purchase!', ' ')
						else:
							say(name,'Sorry! Your inventory is too full.',' ')
					else:
						say(name,'Sorry you do not have enough money to','buy this.')
							
			elif ui2 == 2:
					item = deepcopy(item_list[shop_style][choice])
					try:
						#assuming this is eqipment
						if item.name.find('[D]') != -1:
							txt = texts['decorative_clothes']
						else:
							txt = texts[item.classe]
					except:
						try:
							if item.name.find('Blueprint') != -1:#assuming this is a misc item
								txt = texts['Blueprint']
							elif item.name.find('Scroll') != -1:
								txt = texts['Scroll']
							elif item.name.find('Spellbook') != -1:
								txt = texts['Spellbook']
							else:
								txt = texts[item.name]
						except:
							try:
								txt = [] #assuming this is a consumable
								txt.append(item.name)
								txt.append(' ')
								if item.satisfy_hunger > 0:
									txt.append('Reduces hunger.')
								if item.satisfy_hunger < 0:
									txt.append('Raises hunger.')
								if item.satisfy_thirst > 0:
									txt.append('Reduces thirst.')
								if item.satisfy_thirst < 0:
									txt.append('Raises thirst.')
								if item.satisfy_tiredness > 0:
									txt.append('Can adrenalise!')
								if item.rise_hunger_max > 0:
									txt.append('Lets you go longer without food.')
								if item.rise_thirst_max > 0:
									txt.append('Lets you go longer without water.')
								if item.rise_tiredness_max > 0:
									txt.append('Lets you go longer without sleep.')
								if item.heal > 0:
									txt.append('Can heal your wounds.')
								if item.heal < 0:
									txt.append('Can hurt you!')
								if item.rise_lp_max > 0:
									txt.append('Raises your max. LP.')
								if item.give_seed > 0:
									txt.append('Seeds can be extracted.')
								if item.rotten:
									txt.append('Rotten food may harm you!')
								elif item.life_period < 720:
									if item.life_period != False:
										txt.append('May rot soon.')
									else:
										txt.append('Won\'t rot.')
								if item.effect != None:
									txt.append('Grants '+item.effect.title())
							except:
								txt = texts['info_soon']#if everything else fails
					screen.render_text(txt)
		else:
			run = False
elif ui == 2:
	ui4 = ask('Sell Resources','Buy Resources','Chancel')
	if ui4 == 1:
		run2 = True
		while run2:
			screen.render_resource_sell()
			ui5 = ask('Sell 1 gem for 5 coins ('+str(player.inventory.materials.gem)+' gems)','Sell 1 ore for 1 coin ('+str(player.inventory.materials.ore)+' ore)','Chancel')
			if ui5 == 1:
				if player.inventory.materials.gem > 0:
					sfx.play('shop')
					player.inventory.materials.gem -= 1
					player.coins += 5
				else:
					say(name,'You have no more gems to sell.',' ')
					run2 = False
			elif ui5 == 2:
				if player.inventory.materials.ore > 0:
					sfx.play('shop')
					player.inventory.materials.ore -= 1
					player.coins += 1
				else:
					say(name,'You have no more ore to sell.',' ')
					run2 = False
			else:
				run2 = False
	elif ui4 == 2:
		run2 = True
		while run2:
			screen.render_resource_sell()
			ui5 = ask('Buy 1 gem for 10 coins','Buy 1 ore for 2 coins','Chancel')
			if ui5 == 1:
				if player.inventory.materials.gem < player.inventory.materials.gem_max and player.coins >= 10:
					sfx.play('shop')
					player.inventory.materials.gem += 1
					player.coins -= 10
				else:
					say(name,'Sorry, this won\'t work.',' ')
					run2 = False
			elif ui5 == 2:
				if player.inventory.materials.ore < player.inventory.materials.ore_max and player.coins >= 2:
					sfx.play('shop')
					player.inventory.materials.ore += 1
					player.coins -= 2
				else:
					say(name,'Sorry, this won\'t work.',' ')
					run2 = False
			else:
				run2 = False
longsay(name,None,bye_list[shop_style])
