#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

try:
	import pygame_sdl2 as pygame
except:
	import pygame
import time
import os
pygame.init()

try:
	test = pygame.joystick.Joystick(0)
	found_joystick = True
	del test
except:
	found_joystick = False

if found_joystick == True:
	key_name = {'e':'1','b':'3','x':'2','f':'5','i':'4','.':'6','wasd':'D-Pad','ws':'D-Pad'}
else:
	key_name = {'e':'e','b':'b','x':'x','f':'f','i':'i','.':'.','wasd':'w,s,a,d','ws':'w,s'}
	
basic_path = os.path.dirname(os.path.realpath('getch.py')) #just get the execution path for resources
basic_path = basic_path.replace('/LIB','')
sfx_path = basic_path + os.sep + 'AUDIO' + os.sep + 'SFX' + os.sep
clock = pygame.time.Clock()
sfxlist = {'wasd': pygame.mixer.Sound(sfx_path + 'wasd.ogg'), 'e' : pygame.mixer.Sound(sfx_path + 'e.ogg'), 'b' : pygame.mixer.Sound(sfx_path + 'b.ogg'), 'i' : pygame.mixer.Sound(sfx_path + 'i.ogg'), 'x' : pygame.mixer.Sound(sfx_path + 'x.ogg'), 'f' : pygame.mixer.Sound(sfx_path + 'f.ogg')}


def getch(x=640,y=360,sfx=0,mode=0,mouse=1):#x and y are the resolution of the surface
	
	g = 'foo'
	run = 0
	try:
		j = pygame.joystick.Joystick(0)
		j.init()
	except:
		None
	
	while g == 'foo':
		clock.tick(30)
		run +=1
		for event in pygame.event.get():
			
			try:		
				button_out = []
				
				for b in range (0,4): 
					button = j.get_button(b)
					if button == True:
						button_out.append(1)
					else:
						button_out.append(0)
					
					hat = j.get_hat(0)
						
				if hat[1] == 1:
					g='w'
				elif hat[1] == -1:
					g='s'
				elif hat[0] == 1:
					g='d'
				elif hat[0] == -1:
					g='a'
				elif button_out[0] == 1:
					g='e'
				elif button_out[1] == 1:
					g='x'
				elif button_out[2] == 1:
					g='b'
				elif button_out[3] == 1:
					g='i'
				elif button_out[4] == 1:
					g='f'
				elif button_out[5] == 1 or button_out[6] == 1:
					g='.'
					
			except:
				None	
				
			if event.type == pygame.QUIT:
				g = 'exit'
							
			if event.type == pygame.KEYDOWN:

				if event.key == pygame.K_w or event.key == pygame.K_UP:
					g='w'
					
				if event.key == pygame.K_a or event.key == pygame.K_LEFT:
					g='a'
					
				if event.key == pygame.K_s or event.key == pygame.K_DOWN:
					g='s'
					
				if event.key == pygame.K_d or event.key == pygame.K_RIGHT:
					g='d'
					
				if event.key == pygame.K_e or event.key == pygame.K_RETURN:
					g='e'
					
				if event.key == pygame.K_i or event.key == pygame.K_BACKSPACE:
					g='i'
				
				if event.key == pygame.K_b or event.key == pygame.K_SPACE:
					g='b'
				
				if event.key == pygame.K_x or event.key == pygame.K_ESCAPE:
					g='x'
					
				if event.key == pygame.K_f:
					g='f'
				
				if event.key == pygame.K_q:
					g='q'
				
				if event.key == pygame.K_c:
					g='c'
				
				if event.key == pygame.K_t:
					g='t'
					
				if event.key == pygame.K_r:
					g='r'
				
				if event.key == pygame.K_PERIOD:
					g='.'
				
				if event.key == pygame.K_TAB:
					g='TAB'
					
				if event.key == pygame.K_1:
					g='1'
					
				if event.key == pygame.K_2:
					g='2'
					
				if event.key == pygame.K_3:
					g='3'
					
				if event.key == pygame.K_4:
					g='4'
					
				if event.key == pygame.K_5:
					g='5'
				
				if event.key == pygame.K_6:
					g='6'
				
				if event.key == pygame.K_7:
					g='7'
			
			if mouse == 1:
					
				mouse_pos = pygame.mouse.get_pos()
				mouse_press = pygame.mouse.get_pressed()
				
				if mouse_pos[0] > (534*x)/640 and mouse_pos[0] < (586*x)/640 and mouse_pos[1] > (0*y)/360 and mouse_pos[1] < (52*y)/360 and mouse_press[0] == True:
					g='w'
			
				if mouse_pos[0] > (483*x)/640 and mouse_pos[0] < (533*x)/640 and mouse_pos[1] > (53*y)/360 and mouse_pos[1] < (104*y)/360 and mouse_press[0] == True:
					g='a'
				
				if mouse_pos[0] > (586*x)/640 and mouse_pos[0] < (638*x/640) and mouse_pos[1] > (53*y)/360 and mouse_pos[1] < (104*y)/360 and mouse_press[0] == True:
					g='d'
				
				if mouse_pos[0] > (534*x)/640 and mouse_pos[0] < (586*x)/640 and mouse_pos[1] > (105*y)/360 and mouse_pos[1] < (157*y)/360 and mouse_press[0] == True:
					g='s'
				
				if mouse_pos[0] > (483*x)/640 and mouse_pos[0] < (533*x)/640 and mouse_pos[1] > (202*y)/360 and mouse_pos[1] < (254*y)/360 and mouse_press[0] == True:
					g='e'
				
				if mouse_pos[0] > (534*x)/640 and mouse_pos[0] < (585*x)/640 and mouse_pos[1] > (202*y)/360 and mouse_pos[1] < (254*y)/360 and mouse_press[0] == True:
					g='b'
				
				if mouse_pos[0] > (586*x)/640 and mouse_pos[0] < (638*x)/640 and mouse_pos[1] > (202*y)/360 and mouse_pos[1] < (254*y)/360 and mouse_press[0] == True:
					g='i'
				
				if mouse_pos[0] > (534*x)/640 and mouse_pos[0] < (585*x)/640 and mouse_pos[1] > (255*y)/360 and mouse_pos[1] < (307*y)/360 and mouse_press[0] == True:
					g='x'
				
				if mouse_pos[0] > (534*x)/640 and mouse_pos[0] < (585*x)/640 and mouse_pos[1] > (308*y)/360 and mouse_pos[1] < (361*y)/360 and mouse_press[0] == True:
					g='.'
				
				if mouse_pos[0] > (534*x)/640 and mouse_pos[0] < (586*x)/640 and mouse_pos[1] > (53*y)/360 and mouse_pos[1] < (104*y)/360 and mouse_press[0] == True:
					g='f'
		
		if mode == 2:
			if g == 'foo':
				g = 'none'
			run = 0
					
		if run > 59:
			if mode == 1:
				g='none'
			run = 0
			
		if sfx != 0:
			if g == 'w' or g == 'a' or g == 's' or g == 'd':
				file_name = 'wasd'
			else:
				file_name = g
						
			if g != 'none' and g != 'foo':
				try:
					sfxlist[file_name].set_volume(sfx)
					sfxlist[file_name].play(maxtime=1000)
				except:
					None
					
	return g
