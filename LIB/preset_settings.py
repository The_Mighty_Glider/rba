#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

screenmode = 'default' #0:windowed(small),1:windowed(big),2:fullscreen
bgmmode = 'default' #0:bgm off, 1:bgm full
sfxmode = 'default' #0:sfx off, 1:sfx full
turnmode = 'default' #0:classic, 1:Semi-Real-Time
mousepad = 'default' #0:mouse off, 1:mouse on
version_check = 'default' #0:check off 1:check on
rendermode = 'default' #0:fancy 1:fast
input_nomination = 'default' #0:keyboard 1:joystick numeric 2:joystick(ABXY)
grit = 'default' #0: grit off, 1: grit on

#FOR ANDROID
#screenmode = 2
#mousepad = 1
#input_nomination = 0