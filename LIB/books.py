#! /usr/bin/python
# -*- coding: utf-8 -*-

#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.
 
books = {}

books['The History Of Elvenkind'] = '''In the beginning all elfs populated a heavily forested world.
The tribe was always led by a single female elf. While most female children were born as dryads, the birth of such a female elf was a very rare event, that only took place once in a several centuries.
Everything was fine, until in a stormy night female twins were born. Their names were Sarula and Karula...
Sarula was a very conservative leader and tried to rule the elven tribe the same way like all her precursors did. Most of the elfs and dryads followed her.
Karula on the other hand was progressiv and wanted to break with the old traditions. She was gifted with great magical abilities and was able to gather a small group of loyal followers around her.
To avoid further conflicts with her sister, Karula managed to open a portal to another world and made this world the new home of her people.
It is said that only gods choosen avatars, the Wanderers, are able to open portals between the worlds. This only shows how enormous Karula's magical talent must have been.
Anyway, the portal collapsed after a short amount of time and left Karula's tribe disconnected from their broters and sisters in the old forested home of elvenkind.
The new world that lay in front of our ancestors was a wide fertile plain. In the underground they discovered a strange purple kind of rock, which they used to build their new town.
It is theorized, that this purple rock is also the source of the strange magical radiation that is omnipresent in this world. Furthermore some scholars belive, that this radiation is also the reason for the changes that started happening to our people soon after.
The first changes were small. People started to develop purple eyes. Later, also the color of their skin changed to a light purple tone. When children were born into this world, they were completely hairless.
But the strangest anormaly was, that feemale children were not born as dryads anymore but always as female elfes. This left our ancestors with the serious question how leadership should be organized in future generations.
Karula turned out to be very reasonable in this situation and encouraged the creation of a council.
As soon the council was formed and had proven that it was able to perform it's tasks as authority, Karula steped back from all her obligations as a leader and only focused on her studies.
She build the great library that we still use today and that was even used by some wanderers for research workes in later centuries.
Here she worked until one night, when she disappeared suddenly without a warning.
She was alone this night, so nobody can tell what exactly happened to her...
Some say that a magical experiment went wrong and she dismaterialized herself. Others belive she was able to open another portal and embarked on new adventures. A few people even belive her wisdome reached a state where she was able to enter a higher plain of existence and leave this earthbound life behind her.
Probably we will never know what is true...'''

books['Karula\'s Notes'] = '''[First entry]
There is no doubt!
This portal emits some strange energy. It's magic aura feels almost... dirty.
I want to know who has made this. It is completely different from the one I've made to bring our people to this world. But it's also different from the portals which wanderers would use. It is a mystery...
... and I will solve it!
[Second entry]
It is so frustrating!
I have studied this thing every night for months now but I made zero progress.
I need some kind of focus to canalize the magic powers of the portal in order to form a passway between the worlds.
Normally this can done by using gems... but no matter how pure the gems are, I get no reaction from this thing! This must indeed be a very strange kind of magic... Maybe the strangest I've ever seen....
But I won't give up!
[Third entry]
Today I hurt my finger on a sharp crystal, which I was using as focus. A drop of blood hit the surface of the portal.
Finally I got some kind of reaction from this thing!
But to be honest, I'm not sure if I like this. This magic seems to be impure... maybe even dangerous.,,
Anyway, I have invested far to much time and energy in this "project" to stop now.
[Final entry]
I'm done! The portal is open. I hope this was no mistake...
I've used a significant amount of my own blood to open this portal and now I'm writing this lines in the pale red light it emits.
I tried to use animal blood but this doesn't work. Also some blood samples from a few voluntary donators among my people didn't cause any reaction. It seems this portal needed blood full of magic energy to be activated. My blood...
I feel weak now... but I need to see whats on the other side.
...
... ...
... ... ...
BY THE GODS! I HAVE MADE A TERRIBLE MISTAKE!'''

books['Karula\'s Confession'] = '''It is all my fault! I have placed my people in great danger by opening this portal.
I am still not entirely sure who has built it but it seems like we elves aren't the first inteligent creatures who inhabit this world. Someone must have created this portal to ban the evil forces from our world behind it.
No doubt, this is a dungeon dimension... a terrible demonic keep for all abmormalities born in the deepest shadows.
And I have opened the gates just because of my curiousity...
I can hear them in the dark... lurking... waiting for their chance to sneak into our world and feasting on my peoples lifes.
I need to take the consequences for my deeds!
I don't know how to close this portal again but I can create a seal that will stop evil creatures from using it.
Unfortunately this seal will also hinder me from returning to my people on the other side. But as long at least my bones or my ash will be at this place this seal will protect them.
I only hope that nobody will ever find the entrance to the secret laboratory I have build around the portal on the other side...
'''
