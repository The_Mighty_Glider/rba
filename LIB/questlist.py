#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

from quest import quest

class questlist():
	
	def __init__(self):
		self.qlist = {}
		
		#0
		name = 'Tools needed!'
		info = (('Location: Elysium'),
			(' '),
			('The constructor of the elysium'),
			('asked you to bring him better'),
			('tools.'),
			('He promised to enlarge your room'),
			('to reward you.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#1
		name = 'The ancient hammer'
		info = (('Location: Grassland (Overworld)'),
			(' '),
			('You met a dryad with an ancient'),
			('hammer.'),
			('She will give you the tool if'),
			('you kill all giant bugs who'),
			('hurt the trees of this world.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#2
		name = '[Main] The lost Wanderer'
		info = (('Location: Grassland (Overworld)'),
			(' '),
			('A wanderer visited the elves at'),
			('the grasslands and disappeared.'),
			('Find out what happened to him!'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#3
		name = '[Main] The Wizards Dungeon'
		info = (('Location: Grassland (Dungeon)'),
			(' '),
			('The evil wizard came out of his'),
			('dungeon and petrified all elves.'),
			('The missing wanderer went down'),
			('this dungeon to fight the wizard.'),
			('Find him!'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#4
		name = '[Main] Ask Lillya'
		info = (('Location: Elvish Fortress'),
			(' '),
			('The wizard escaped into the dwarfen'),
			('bastion and hides behind its magic'),
			('door.'),
			('Maybe Lillya has an idea what to do.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#5
		name = '[Main] The ancient Text'
		info = (('Location: Grassland (Overworld)'),
			(' '),
			('Lillya found an ancient text that'),
			('may offer a way into the dwarfen'),
			('bastion.'),
			('Unfortunately this text needs to be'),
			('translated first. Cylira can do this.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#6
		name = '[Main] Translsation'
		info = (('Location: Elvish Fortress'),
			(' '),
			('Bring the sealed envelope back to '),
			('Lillya!'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#7
		name = '[Main] Showdown!'
		info = (('Location: Desert (Deep Underground)'),
			(' '),
			('Kill the evil wizard!'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#8
		name = 'Revive Cylira'
		info = (('Location: Grassland (Overworld)'),
			(' '),
			('The leader of the grassland elves has'),
			('been petrified.'),
			('Use a heart-shaped crystal to revive'),
			('her.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#9
		name = 'Revive all elves'
		info = (('Location: Grassland (Overworld)'),
			(' '),
			('Revive all petrified grassland elves'),
			('with a heart-shaped crystal!'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#10
		name = 'Revive all dwarfes'
		info = (('Location: Desert (Deep Underground)'),
			(' '),
			('Revive all petrified dwarfws by using a'),
			('heart-shaped crystal!'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#11
		name = 'The banished Orc (Part 1)'
		info = (('Location: Grassland (Underground)'),
			(' '),
			('The banished Orc asks for elvish beer.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#12
		name = 'The banished Orc (Part 2)'
		info = (('Location: Grassland (Underground)'),
			(' '),
			('The banished Orc asks for meat.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#13
		name = 'The banished Orc (Part 3)'
		info = (('Location: Grassland (Underground)'),
			(' '),
			('The banished Orc asks for bread.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#14
		name = 'The banished Orc (Part 4)'
		info = (('Location: Grassland (Underground)'),
			(' '),
			('The banished Orc asks for warm beer.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#15
		name = 'The old neko'
		info = (('Location: Desert (Tomb)'),
			(' '),
			('The old neko\'s son went into the tomb and'),
			('was turned into an undead.'),
			('Give him the eternal peace and return with'),
			('something his father can bury!'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#16
		name = 'The lost tool box'
		info = (('Location: Elvish Fortress (Sewers)'),
			(' '),
			('An albino rat has stolen the elvish worker\'s'),
			('tool box.'),
			('Bring it back!'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#17
		name = 'Naga Rescue'
		info = (('Location: Grotto'),
			(' '),
			('Save all golden nagas in the grotto and'),
			('return to the naga at the desert caves.'),
			(' '),
			('The nagas used a spell to become invisible.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#18
		name = 'Going down!'
		info = (('Location: Grassland (Underground)'),
			(' '),
			('Build a stair for the dwarfen scout.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#19
		name = 'What is beneath the library?'
		info = (('Location: Elfish Fortress (Underground)'),
			(' '),
			('You found a secret stair under a bookshelf.'),
			('Find out what emits a evil aura'), 
			('from down there.'))
		q = quest(name,info)
		self.qlist[q.name] = q
		#20
		name = 'A half-blood called Narasa'
		info = (('Location: Elfish Fortress'),
			(' '),
			('Narasa has escaped from the dungeon'),
			('dimension.'), 
			('Find her and bring her back!'),
			('(Alive and unharmed)'))
		q = quest(name,info)
		self.qlist[q.name] = q
