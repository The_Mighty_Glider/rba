#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

import pickle as p
from tile import tile
from itemlist import itemlist

il = itemlist()

class tilelist():
	def __init__(self):
	
		self.tlist = {}
		techID = 0
		
		self.tlist['global_caves'] = []
	#0
		t=tile(techID = techID,
					name = 'Cave ground',
					tile_color = 'light_brown',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A dark cave surrounds you.', 
					damage_message = None,
					tile_pos = (9,0))
		self.tlist['global_caves'].append(t)
		techID+=1
	#1
		t=tile(techID = techID,
					name = 'Worn rock',
					tile_color = 'light_grey',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You crack your way through worn rocks.', 
					damage_message = None,
					destroy = 2,
					replace = self.tlist['global_caves'][0],
					tile_pos = (0,0),
					transparency = False,
					conected_resources = ('stone',3))
		self.tlist['global_caves'].append(t)
		techID+=1
	#2
		t=tile(techID = techID,
					name = 'Soft soil',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You dig through soft soil.', 
					damage_message = None,
					destroy = 1,
					replace = self.tlist['global_caves'][0],
					tile_pos = (0,1),
					transparency = False)
		self.tlist['global_caves'].append(t)
		techID+=1
	#3
		t=tile(techID = techID,
					name = 'Hard rock',
					tile_color = 'grey',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You crack your way through solid rocks.', 
					damage_message = None,
					destroy = 4,
					replace = self.tlist['global_caves'][0],
					tile_pos = (0,10),
					transparency = False,
					conected_resources = ('stone',5))
		self.tlist['global_caves'].append(t)
		techID+=1
	#4
		t=tile(techID = techID,
					name = 'Lava',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'low_liquid',
					grow_group = 'None',
					damage = 1,
					special_group = 'hot',
					move_message = 'You stand in a hot stream of lava.', 
					damage_message = 'The lava burns your flesh',
					light_emit = 3,
					tile_pos = (0,8,11,4))
		self.tlist['global_caves'].append(t)
		
		techID =1000
		
		self.tlist['misc'] = []
		#0
		t=tile(techID = techID,
					name = 'Low water',
					tile_color = 'light_blue',
					use_group = 'drink',
					move_group = 'low_liquid',
					grow_group = 'None',
					damage = 0,
					special_group = 'vaporable',
					special_num = 2,
					move_message = 'Your feet become wet.', 
					damage_message = None,
					tile_pos = (0,7,11,3))
		self.tlist['misc'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Mud',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The ground under your feet feels soft and wet.', 
					damage_message = None,
					tile_pos = (6,10))
		self.tlist['misc'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Hot caveground',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					special_group = 'hot',
					move_message = 'The ground under your feet feels like hot coals.', 
					damage_message = None,
					light_emit = 2,
					tile_pos = (5,5))
		self.tlist['misc'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Water',
					tile_color = 'blue',
					use_group = 'drink',
					move_group = 'swim',
					grow_group = 'None',
					damage = 0,
					special_group = 'vaporable',
					special_num = 3,
					move_message = 'Cool water surrounds you.', 
					damage_message = None,
					tile_pos = (1,2,12,3))
		self.tlist['misc'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Ore',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You brake some ore out of the solid rock here.', 
					damage_message = None,
					destroy = 2,
					replace = self.tlist['global_caves'][0],
					tile_pos = (0,2),
					transparency = False,
					conected_resources = ('ore',1))
		self.tlist['misc'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Gem',
					tile_color = 'light_blue',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You brake a gem out of the solid rock here.', 
					damage_message = None,
					destroy = 4,
					replace = self.tlist['global_caves'][0],
					tile_pos = (5,1),
					transparency = False,
					conected_resources = ('gem',1))
		self.tlist['misc'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Blue mushroom',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'mushroom_mud',
					damage = 0, 
					move_message = 'A blue mushroom grows here.', 
					damage_message = None,
					tile_pos = (1,6),
					conected_tiles = ['misc',6],
					conected_items = (il.ilist['food'][1]))
		self.tlist['misc'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Brown mushroom',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'mushroom_treelike',
					damage = 0, 
					move_message = 'A brown mushroom grows here.', 
					damage_message = None,
					tile_pos = (5,8),
					conected_tiles = [('misc',7),('misc',15)],
					conected_items = (il.ilist['food'][2]))
		self.tlist['misc'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Purple mushroom',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'mushroom',
					damage = 0, 
					move_message = 'A purple mushroom grows here.', 
					damage_message = None,
					tile_pos = (5,6),
					conected_tiles = ['misc',8],
					conected_items = (il.ilist['food'][3]))
		self.tlist['misc'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Lost gem',
					tile_color = 'red',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'A gem lies on the ground right here.', 
					damage_message = None,
					tile_pos = (1,8),
					conected_resources = ('gem',1))
		self.tlist['misc'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Water lily',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'herblike',
					damage = 0, 
					move_message = 'A water lily grows here.', 
					damage_message = None,
					tile_pos = (1,1,12,5),
					conected_tiles = ['misc',14])
		self.tlist['misc'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Lost ore',
					tile_color = 'red',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'Some ore lies on the ground right here.', 
					damage_message = None,
					tile_pos = (1,7),
					conected_resources = ('ore',1))
		self.tlist['misc'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Orcish deco',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid_deco',
					grow_group = 'None',
					damage = 0, 
					move_message = 'None', 
					damage_message = None,
					build_here = False,
					roofed = True,
					tile_pos = (0,11),
					transparency = False)
		self.tlist['misc'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Present',
					tile_color = 'red',
					use_group = 'container',
					move_group = 'soil',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'A present lies on the ground right here.', 
					damage_message = None,
					tile_pos = (6,4))
		self.tlist['misc'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Water lily with blossom',
					tile_color = 'white',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'herblike',
					damage = 0, 
					move_message = 'A water lily with a beautiful blossom grows here.', 
					damage_message = None,
					tile_pos = (1,0,12,4),
					conected_tiles = ['misc',10],
					conected_resources = ('herbs',1))
		self.tlist['misc'].append(t)
		techID+=1
		#15
		t=tile(techID = techID,
					name = 'Giant mushroom',
					tile_color = 'white',
					use_group = 'tree',
					move_group = 'tree',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'There is a giant mushroom here.', 
					damage_message = None,
					tile_pos = (15,15),
					upper_tile = (14,15),
					transparency = False,
					special_group = 'flamable',
					special_num = 4,
					conected_tiles = [('misc',7),('global_caves',0)],
					conected_resources = ('wood',3))
		self.tlist['misc'].append(t)
		techID+=1
		#16
		t=tile(techID = techID,
					name = 'solid orcish wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'None', 
					damage_message = None,
					roofed = True,
					tile_pos = (0,4),
					transparency = False)
		self.tlist['misc'].append(t)
		techID+=1
		#17
		t=tile(techID = techID,
					name = 'Crystal orb socle',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see an empty socle here.', 
					damage_message = None,
					roofed = True,
					tile_pos = (5,11))
		self.tlist['misc'].append(t)
		techID+=1
		#18
		t=tile(techID = techID,
					name = 'Crystal orb',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					replace = self.tlist['misc'][17],
					damage = 0, 
					move_message = 'You see a crystal orb here.', 
					damage_message = None,
					roofed = True,
					light_emit = 2,
					tile_pos = (4,11),
					conected_items = (il.ilist['misc'][54]))
		self.tlist['misc'].append(t)
		techID+=1
		#19
		t=tile(techID = techID,
					name = 'Green floor',
					tile_color = 'light_green',
					use_group = 'None',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over solid green tiles.', 
					damage_message = None,
					build_here = False,
					drops_here = False,
					roofed = True,
					tile_pos = (8,12))
		self.tlist['misc'].append(t)
		techID+=1
		#20
		t=tile(techID = techID,
					name = 'Green pilar',
					tile_color = 'green',
					use_group = 'None',
					move_group = 'solid_pilar',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a green pilar.', 
					damage_message = None,
					build_here = False,
					drops_here = False,
					transparency = False,
					roofed = True,
					tile_pos = (8,11))
		self.tlist['misc'].append(t)
		techID+=1
		#21
		t=tile(techID = techID,
					name = 'Sandstone Ore',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You brake some ore out of the sandstone here.', 
					damage_message = None,
					destroy = 2,
					replace = self.tlist['global_caves'][0],
					tile_pos = (12,10),
					transparency = False,
					conected_resources = ('ore',1))
		self.tlist['misc'].append(t)
		techID+=1
		#22
		t=tile(techID = techID,
					name = 'Sandstone gem',
					tile_color = 'light_blue',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You brake a gem out of the sandstone here.', 
					damage_message = None,
					destroy = 4,
					replace = self.tlist['global_caves'][0],
					tile_pos = (12,9),
					transparency = False,
					conected_resources = ('gem',1))
		self.tlist['misc'].append(t)
		techID+=1
		#23
		t=tile(techID = techID,
					name = 'Worn sandstone',
					tile_color = 'light_grey',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You crack your way through worn sandstone.', 
					damage_message = None,
					destroy = 2,
					replace = self.tlist['global_caves'][0],
					tile_pos = (12,7),
					transparency = False,
					conected_resources = ('stone',3))
		self.tlist['misc'].append(t)
		techID+=1
		#24
		t=tile(techID = techID,
					name = 'Sandstone',
					tile_color = 'grey',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You crack your way through sandstone.', 
					damage_message = None,
					destroy = 4,
					replace = self.tlist['global_caves'][0],
					tile_pos = (12,8),
					transparency = False,
					conected_resources = ('stone',5))
		self.tlist['misc'].append(t)
		techID+=1
		#25
		t=tile(techID = techID,
					name = 'Lichten',
					tile_color = 'white',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A lichten grows at the ground.', 
					damage_message = None,
					tile_pos = (1,13),
					conected_resources = ('herbs',1))
		self.tlist['misc'].append(t)
		techID+=1
		#26
		t=tile(techID = techID,
					name = 'Yellow mushroom',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'mushroom_mud',
					damage = 0, 
					move_message = 'A yellow mushroom grows here.', 
					damage_message = None,
					tile_pos = (0,13),
					conected_tiles = ['misc',26],
					conected_items = (il.ilist['food'][37]))
		self.tlist['misc'].append(t)
		techID+=1
		#27
		t=tile(techID = techID,
					name = 'Fire leaves',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'mushroom',
					damage = 0, 
					move_message = 'A fire leaves grows here.', 
					damage_message = None,
					light_emit = 2,
					tile_pos = (2,13),
					conected_tiles = ['misc',27],
					conected_items = (il.ilist['food'][36]))
		self.tlist['misc'].append(t)
		techID+=1
		#28
		t=tile(techID = techID,
					name = 'Obsidian',
					tile_color = 'grey',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You crack your way through solid obsidian.', 
					damage_message = None,
					destroy = 6,
					replace = self.tlist['global_caves'][0],
					tile_pos = (3,13),
					transparency = False,
					conected_resources = ('stone',7))
		self.tlist['misc'].append(t)
		techID+=1
		#29
		t=tile(techID = techID,
					name = 'Ghost leaves',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'mushroom',
					damage = 0, 
					move_message = 'A ghost leaves grows here.', 
					damage_message = None,
					tile_pos = (11,14),
					conected_tiles = ['misc',29],
					conected_items = (il.ilist['food'][54]))
		self.tlist['misc'].append(t)
		techID+=1
		#30
		t=tile(techID = techID,
					name = 'Dwarfen Floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'temp_solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over wooden floor.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (7,5))
		self.tlist['misc'].append(t)
		techID+=1
		#31
		t=tile(techID = techID,
					name = 'Dwarfen bridge',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over a solid bridge.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (10,6))
		self.tlist['misc'].append(t)
		techID+=1
		#32
		t=tile(techID = techID,
					name = 'Ring',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A ring lies on the ground here.', 
					damage_message = None,
					tile_pos = (9,15),
					conected_tiles = ['misc',32],
					conected_items = (il.ilist['artefact'][1]))
		self.tlist['misc'].append(t)
		techID+=1
		#33
		t=tile(techID = techID,
					name = 'Lost coin',
					tile_color = 'red',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'A coin lies on the ground right here.', 
					damage_message = None,
					tile_pos = (14,0),
					conected_resources = ('coin',1))
		self.tlist['misc'].append(t)
		techID+=1
		#34
		t=tile(techID = techID,
					name = 'Broken green floor',
					tile_color = 'light_green',
					use_group = 'None',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The green tiles of the floor are broken here.', 
					damage_message = None,
					build_here = False,
					drops_here = False,
					tile_pos = (15,7))
		self.tlist['misc'].append(t)
		techID+=1
		#35
		t=tile(techID = techID,
					name = 'Broken green pilar',
					tile_color = 'green',
					use_group = 'None',
					move_group = 'solid_pilar',
					grow_group = 'None',
					damage = 0, 
					move_message = 'This pilar is broken.', 
					damage_message = None,
					build_here = False,
					drops_here = False,
					tile_pos = (15,6))
		self.tlist['misc'].append(t)
		techID+=1
		#36
		t=tile(techID = techID,
					name = 'Evolution stone in rock',
					tile_color = 'yellow',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You brake an evolution stone out of the rock here.', 
					damage_message = None,
					destroy = 4,
					replace = self.tlist['global_caves'][0],
					light_emit = 2,
					tile_pos = (15,9),
					transparency = False,
					conected_resources = ('stone',3))
		self.tlist['misc'].append(t)
		techID+=1
		#37
		t=tile(techID = techID,
					name = 'Evolution stone',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'An evolution stone lies on the ground here.', 
					damage_message = None,
					light_emit = 3,
					tile_pos = (15,8),
					conected_tiles = ['misc',37],
					conected_items = (il.ilist['misc'][105]))
		self.tlist['misc'].append(t)
		
		techID = 2000
		
		self.tlist['sewer'] = []
		#0
		t=tile(techID = techID,
					name = 'Sewer Wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					transparency = False,
					tile_pos = (2,6))
		self.tlist['sewer'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Sewer Floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over a cobbled floor.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (10,6))
		self.tlist['sewer'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Sewer Water',
					tile_color = 'light_blue',
					use_group = 'dont_drink',
					move_group = 'low_liquid',
					grow_group = 'None',
					damage = 0,
					special_group = 'vaporable',
					special_num = 2,
					move_message = 'Your feet become wet.', 
					damage_message = None,
					tile_pos = (11,6,11,7))
		self.tlist['sewer'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Sewer Entrance',
					tile_color = 'white',
					use_group = 'sewer_down',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a entrance to the sewers here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (11,8),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['sewer'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Sewer Exit',
					tile_color = 'white',
					use_group = 'sewer_up',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a ladder here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (11,9),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['sewer'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Deep Sewer water',
					tile_color = 'blue',
					use_group = 'dont_drink',
					move_group = 'swim',
					grow_group = 'None',
					damage = 0,
					special_group = 'vaporable',
					special_num = 3,
					move_message = 'You swim in dirty water.', 
					damage_message = None,
					tile_pos = (11,10,12,14))
		self.tlist['sewer'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Bitter moss',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'Some bitter moss grows on the ground here.', 
					damage_message = None,
					conected_items = il.ilist['food'][62],
					tile_pos = (6,15))
		self.tlist['sewer'].append(t)
		
		techID = 3000
		
		self.tlist['mine'] = []
	
		t=tile(techID = techID,
					name = 'Orcish mine floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over dry mine ground.', 
					damage_message = None,
					roofed = True,
					tile_pos = (0,5))
		self.tlist['mine'].append(t)
		techID+=1
		
		t=tile(techID = techID,
					name = 'Orcish mine wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					replace = self.tlist['mine'][0],
					move_message = 'You crack your way through this wall.', 
					damage_message = None,
					destroy = 2,
					tile_pos = (0,4),
					transparency = False)
		self.tlist['mine'].append(t)
		techID+=1
	
		t=tile(techID = techID,
					name = 'Blood moss',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'Some blood moss grows on the ground here.', 
					damage_message = None,
					conected_items = il.ilist['food'][34],
					tile_pos = (8,5))
		self.tlist['mine'].append(t)
		
		techID = 4000
	
		self.tlist['functional'] = []
		#0
		t=tile(techID = techID,
					name = 'Border',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'solid_border',
					grow_group = 'None',
					damage = 0, 
					move_message = 'HERE BE DRAGONS', 
					damage_message = None,
					build_here = False,
					tile_pos = (9,1),
					transparency = False)
		self.tlist['functional'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Stair down',
					tile_color = 'white_stair',
					use_group = 'stair_down',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a down leading stairway here.', 
					damage_message = None,
					civilisation = True,
					build_here = False,
					tile_pos = (3,4),
					ignore_liquid = True)
		self.tlist['functional'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Stair up',
					tile_color = 'white_stair',
					use_group = 'stair_up',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a up leading stairway here.', 
					damage_message = None,
					civilisation = True,
					build_here = False,
					tile_pos = (3,3),
					ignore_liquid = True)
		self.tlist['functional'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Empty chest',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of an empty chest.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 3,
					conected_items = il.ilist['misc'][1],
					tile_pos = (5,10))
		self.tlist['functional'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Chest',
					tile_color = 'white',
					use_group = 'container',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a chest.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (6,9))
		self.tlist['functional'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Stack',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'Something lies on the ground here.', 
					damage_message = None,
					tile_pos = (3,5))
		self.tlist['functional'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Humanoid remains',
					tile_color = 'white',
					use_group = 'loot',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'Here are some humanoid remains.', 
					damage_message = None,
					tile_pos = (3,9))
		self.tlist['functional'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Fountain',
					tile_color = 'blue',
					use_group = 'drink',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a fountain.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (6,3,10,12))
		self.tlist['functional'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Bed',
					tile_color = 'white',
					use_group = 'sleep',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You lie down inside a comfortable bed.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (8,6))
		self.tlist['functional'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Carpenter\'s workbench',
					tile_color = 'white',
					use_group = 'carpenter',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a carpenter\'s workbench.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (8,2))
		self.tlist['functional'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Carvers\'s workbench',
					tile_color = 'white',
					use_group = 'carver',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a carvers\'s workbench.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (8,1))
		self.tlist['functional'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Stonecutter\'s workbench',
					tile_color = 'white',
					use_group = 'stonecutter',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a stonecutter\'s workbench.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (3,2))
		self.tlist['functional'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Forger\'s workbench',
					tile_color = 'white',
					use_group = 'forger',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a forger\'s workbench.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (8,0))
		self.tlist['functional'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Alchemist\'s workshop',
					tile_color = 'white',
					use_group = 'alchemist',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a alchemist\'s workshop.', 
					damage_message = None,
					civilisation = True,
					special_group = 'explosive',
					tile_pos = (8,4))
		self.tlist['functional'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Furnace',
					tile_color = 'white',
					use_group = 'furnace',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a furnace.', 
					damage_message = None,
					civilisation = True,
					light_emit = 4,
					tile_pos = (5,2,10,5))
		self.tlist['functional'].append(t)
		techID+=1
		#15
		t=tile(techID = techID,
					name = 'Altar',
					tile_color = 'white',
					use_group = 'altar',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a holy altar.', 
					damage_message = None,
					civilisation = True,
					light_emit = 3,
					tile_pos = (8,3))
		self.tlist['functional'].append(t)
		techID+=1
		#16
		t=tile(techID = techID,
					name = 'Table',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'table',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand on a table.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (3,1))
		self.tlist['functional'].append(t)
		techID+=1
		#17
		t=tile(techID = techID,
					name = 'Wooden seat',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'Here is a comfortable looking wooden seat.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 2,
					conected_items = il.ilist['misc'][11],
					tile_pos = (3,6))
		self.tlist['functional'].append(t)
		techID+=1
		#18
		t=tile(techID = techID,
					name = 'Stone seat',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'Here is a comfortable looking stone seat.', 
					damage_message = None,
					civilisation = True,
					conected_items = il.ilist['misc'][12],
					tile_pos = (3,7))
		self.tlist['functional'].append(t)
		techID+=1
		#19
		t=tile(techID = techID,
					name = 'Bookshelf',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'Here is a bookshelf.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (9,2),
					transparency = False)
		self.tlist['functional'].append(t)
		techID+=1
		#20
		t=tile(techID = techID,
					name = 'Magic chest',
					tile_color = 'light_purple',
					use_group = 'None',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a magic chest.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (6,8))
		self.tlist['functional'].append(t)
		techID+=1
		#21
		t=tile(techID = techID,
					name = 'Animal remains',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'Here lies a piece of raw flesh.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (8,7),
					conected_items = il.ilist['food'][9])
		self.tlist['functional'].append(t)
		techID+=1
		#22
		t=tile(techID = techID,
					name = 'Pilar',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'solid_pilar',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a pilar.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (5,9),
					transparency = False)
		self.tlist['functional'].append(t)
		techID+=1
		#23
		t=tile(techID = techID,
					name = 'Master forge',
					tile_color = 'purple',
					use_group = 'forger',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a legendary master forge.', 
					damage_message = None,
					tile_pos = (0,6))
		self.tlist['functional'].append(t)
		techID+=1
		#24
		t=tile(techID = techID,
					name = 'Empty fridge',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a empty... fridge?!', 
					damage_message = None,
					civilisation = True,
					conected_items = (il.ilist['misc'][52]),
					tile_pos = (2,11))
		self.tlist['functional'].append(t)
		techID+=1
		#25
		t=tile(techID = techID,
					name = 'Fridge',
					tile_color = 'white',
					use_group = 'container',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a... fridge?!', 
					damage_message = None,
					civilisation = True,
					tile_pos = (1,11))
		self.tlist['functional'].append(t)
		techID+=1
		#26
		t=tile(techID = techID,
					name = 'Rubble',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over rubble.', 
					damage_message = None,
					tile_pos = (3,11))
		self.tlist['functional'].append(t)
		techID+=1
		#27
		t=tile(techID = techID,
					name = 'Grave',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = '[R.I.P]', 
					damage_message = None,
					tile_pos = (11,5))
		self.tlist['functional'].append(t)
		techID+=1
		#28
		t=tile(techID = techID,
					name = 'Heated stone',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A heated stone lies on the ground.', 
					damage_message = None,
					special_group = 'hot',
					conected_items = il.ilist['misc'][71],
					light_emit = 2,
					tile_pos = (12,6))
		self.tlist['functional'].append(t)
		techID+=1
		#29
		t=tile(techID = techID,
					name = 'Brimstone',
					tile_color = 'yellow',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A heated stone lies on the ground.', 
					damage_message = None,
					special_group = 'highly flamable',
					special_num = 3,
					conected_items = il.ilist['misc'][73],
					tile_pos = (5,13))
		self.tlist['functional'].append(t)
		techID+=1
		#30
		t=tile(techID = techID,
					name = 'Stone',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A stone lies on the ground.', 
					damage_message = None,
					conected_items = il.ilist['misc'][78],
					tile_pos = (13,1))
		self.tlist['functional'].append(t)
		techID+=1
		#31
		t=tile(techID = techID,
					name = 'Apple',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'An apple lies on the ground.', 
					damage_message = None,
					conected_items = il.ilist['food'][40],
					tile_pos = (13,5))
		self.tlist['functional'].append(t)
		techID+=1
		#32
		t=tile(techID = techID,
					name = 'Golden Apple',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A golden apple lies on the ground.', 
					damage_message = None,
					conected_items = il.ilist['food'][42],
					tile_pos = (13,6))
		self.tlist['functional'].append(t)
		techID+=1
		#33
		t=tile(techID = techID,
					name = 'Egg',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A egg lies on the ground.', 
					damage_message = None,
					conected_items = il.ilist['food'][44],
					tile_pos = (5,14))
		self.tlist['functional'].append(t)
		techID+=1
		#34
		t=tile(techID = techID,
					name = 'Monster Egg',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A monster egg lies on the ground.', 
					damage_message = None,
					conected_items = il.ilist['misc'][87],
					tile_pos = (6,14))
		self.tlist['functional'].append(t)
		techID+=1
		#35
		t=tile(techID = techID,
					name = 'Heart-Shaped Crystal',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a heart-shaped crystal here.', 
					damage_message = None,
					conected_items = il.ilist['misc'][40],
					light_emit = 1,
					tile_pos = (13,14))
		self.tlist['functional'].append(t)
		techID+=1
		#36
		t=tile(techID = techID,
					name = 'Heap of Mummy Dust',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a small heap of dust here.', 
					damage_message = None,
					conected_items = il.ilist['food'][55],
					tile_pos = (10,14))
		self.tlist['functional'].append(t)
		techID+=1
		#37
		t=tile(techID = techID,
					name = 'Spider eye',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A ugly spiders eye lies on the ground here.', 
					damage_message = None,
					conected_items = il.ilist['food'][57],
					tile_pos = (0,15))
		self.tlist['functional'].append(t)
		techID+=1
		#38
		t=tile(techID = techID,
					name = 'Green jelly',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see some green jelly here.', 
					damage_message = None,
					conected_items = il.ilist['food'][59],
					tile_pos = (1,15))
		self.tlist['functional'].append(t)
		techID+=1
		#39
		t=tile(techID = techID,
					name = 'Red jelly',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see some red jelly here.', 
					damage_message = None,
					conected_items = il.ilist['food'][29],
					tile_pos = (2,15))
		self.tlist['functional'].append(t)
		techID+=1
		#40
		t=tile(techID = techID,
					name = 'Yellow jelly',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see some yellow jelly here.', 
					damage_message = None,
					conected_items = il.ilist['food'][31],
					tile_pos = (3,15))
		self.tlist['functional'].append(t)
		techID+=1
		#41
		t=tile(techID = techID,
					name = 'Blue jelly',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see some blue jelly here.', 
					damage_message = None,
					conected_items = il.ilist['food'][30],
					tile_pos = (4,15))
		self.tlist['functional'].append(t)
		techID+=1
		#42
		t=tile(techID = techID,
					name = 'Purple jelly',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see some purple jelly here.', 
					damage_message = None,
					conected_items = il.ilist['food'][60],
					tile_pos = (5,15))
		self.tlist['functional'].append(t)
		techID+=1
		#43
		t=tile(techID = techID,
					name = 'Chicken nest',
					tile_color = 'light_red',
					use_group = 'chicken_nest',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a nest here.', 
					damage_message = None,
					build_here = False,
					civilisation = True,
					tile_pos = (15,2))
		self.tlist['functional'].append(t)
		techID+=1
		#44
		t=tile(techID = techID,
					name = 'Chicken nest with egg',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'house',
					grow_group = 'hatch_chicken',
					damage = 0, 
					move_message = 'You see a nest with an egg inside.', 
					damage_message = None,
					build_here = False,
					civilisation = True,
					tile_pos = (15,3))
		self.tlist['functional'].append(t)
		techID+=1
		#45
		t=tile(techID = techID,
					name = 'Sand (Grave Location)',
					tile_color = 'light_yellow',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over hot sand.', 
					damage_message = None,
					build_here = True,
					can_grown = True,
					tile_pos = (4,5))
		self.tlist['functional'].append(t)
		techID+=1
		#46
		t=tile(techID = techID,
					name = 'Grave of the neko\'s son',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = '[R.I.P.]', 
					damage_message = None,
					tile_pos = (15,4))
		self.tlist['functional'].append(t)
		techID+=1
		#47
		t=tile(techID = techID,
					name = 'Sealed door',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'sealed_door',
					grow_group = 'None',
					damage = 0, 
					move_message = 'This door is sealed with strong magic!', 
					damage_message = None,
					transparency = False,
					build_here = False,
					can_grown = False,
					tile_pos = (15,5))
		self.tlist['functional'].append(t)
		#48
		t=tile(techID = techID,
					name = 'Enchantment table',
					tile_color = 'white',
					use_group = 'enchantment',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of an enchantment table.', 
					damage_message = None,
					civilisation = True,
					light_emit = 2,
					tile_pos = (15,12))
		self.tlist['functional'].append(t)
		techID+=1
		#49
		t=tile(techID = techID,
					name = 'Planter',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'herblike',
					damage = 0, 
					move_message = 'You stand in front of a planter.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (16,14),
					conected_tiles = ['functional',50],
					conected_items = il.ilist['misc'][115])
		self.tlist['functional'].append(t)
		techID+=1
		#50
		t=tile(techID = techID,
					name = 'Planter with herbs',
					tile_color = 'white',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'None',
					flower = True,
					damage = 0, 
					move_message = 'You stand in front of a planter with herbs.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (16,15),
					conected_tiles = ['functional',49],
					conected_resources = ('herbs',1))
		self.tlist['functional'].append(t)
		techID+=1
		#51
		t=tile(techID = techID,
					name = 'Beehive',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a beehive. Don\'t get stung!', 
					damage_message = None,
					civilisation = True,
					tile_pos = (18,5,18,6),
					conected_items = il.ilist['misc'][123])
		self.tlist['functional'].append(t)
		techID+=1
		#52
		t=tile(techID = techID,
					name = 'Fragments of Crystal',
					tile_color = 'light_blue',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'Some fragments of crystal lie on the ground here..', 
					damage_message = None,
					tile_pos = (18,9),
					conected_items = il.ilist['misc'][126])
		self.tlist['functional'].append(t)
		techID+=1
		#53
		t=tile(techID = techID,
					name = 'Spinning wheel',
					tile_color = 'white',
					use_group = 'spinning',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a spinning wheel.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (17,15))
		self.tlist['functional'].append(t)
		techID+=1
		#54
		t=tile(techID = techID,
					name = 'Tent',
					tile_color = 'white',
					use_group = 'sleep',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You lie down inside a tent.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (17,16))
		self.tlist['functional'].append(t)
		techID+=1
		#55
		t=tile(techID = techID,
					name = 'Fireplace',
					tile_color = 'white',
					use_group = 'fireplace',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a fireplace. The fire isn\'t burning.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (18,12),
					conected_items = il.ilist['misc'][138])
		self.tlist['functional'].append(t)
		techID+=1
		#56
		t=tile(techID = techID,
					name = 'Bonfire',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'jump',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a bonfire.', 
					damage_message = None,
					civilisation = True,
					special_group = 'hot',
					light_emit = 5,
					tile_pos = (18,13,18,14))
		self.tlist['functional'].append(t)
		techID+=1
		#57
		t=tile(techID = techID,
					name = 'Royal Trading Banner',
					tile_color = 'white',
					use_group = 'trading_banner',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a royal banner.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (4,18),
					upper_tile = (3,18),
					conected_items = il.ilist['misc'][139])
		self.tlist['functional'].append(t)
		techID+=1
		#58
		t=tile(techID = techID,
					name = 'Torch',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a burning torch.', 
					damage_message = None,
					civilisation = True,
					light_emit = 5,
					tile_pos = (6,18,7,18),
					conected_items = il.ilist['misc'][44])
		self.tlist['functional'].append(t)
		techID+=1
		#59
		t=tile(techID = techID,
					name = 'Candle',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a burning candle.', 
					damage_message = None,
					civilisation = True,
					light_emit = 5,
					tile_pos = (8,18,9,18),
					conected_items = il.ilist['misc'][125])
		self.tlist['functional'].append(t)
		techID+=1
		
		techID = 5000
	
		self.tlist['local'] = []
		#0
		t=tile(techID = techID,
					name = 'Grass',
					tile_color = 'light_green',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over soft grass.', 
					damage_message = None,
					can_grown = True,
					tile_pos = (5,0))
		self.tlist['local'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Scrub',
					tile_color = 'green',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'scrub',
					damage = 0, 
					move_message = 'There is a scrub here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (6,1),
					conected_tiles = ['local',3])
		self.tlist['local'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Scrub with red berries',
					tile_color = 'red',
					use_group = 'gather_scrub',
					move_group = 'soil',
					grow_group = 'scrub_berries',
					damage = 0, 
					move_message = 'There is a scrub with red berries here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (3,10),
					conected_items = (il.ilist['food'][0]),
					conected_tiles = [('local',5),('local',6),('local',1)])
		self.tlist['local'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Scrub with buds',
					tile_color = 'green',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'scrub_buds',
					damage = 0, 
					move_message = 'There is a scrub with some buds here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (5,4),
					conected_tiles = ['local',4])
		self.tlist['local'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Scrub with blossoms',
					tile_color = 'green',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'scrub_blossom',
					damage = 0, 
					move_message = 'There is a scrub with beautiful blossoms here.', 
					damage_message = None,
					special_group = 'flamable',
					flower = True,
					special_num = 2,
					tile_pos = (1,4),
					conected_tiles = [('local',2),('local',17),('local',18)])
		self.tlist['local'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Scrub with scruffy berries',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'scrub_scruffy',
					damage = 0, 
					move_message = 'There is a scrub with scruffy berries here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (6,2),
					conected_tiles = [('local',1),('local',9)])
		self.tlist['local'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Scrub seed',
					tile_color = 'brown',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'scrub_groew',
					damage = 0, 
					move_message = 'There are some seeds on the ground here.', 
					damage_message = None,
					tile_pos = (5,3),
					conected_items = (il.ilist['misc'][47]),
					conected_tiles = ['local',7])
		self.tlist['local'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Scrub sepling',
					tile_color = 'green',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'scrub_grow',
					damage = 0, 
					move_message = 'Something starts to grow here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 1,
					tile_pos = (5,7),
					conected_tiles = ['local',8])
		self.tlist['local'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Small scrub',
					tile_color = 'green',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'scrub_grow',
					damage = 0, 
					move_message = 'There is a small scrub at this place.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 1,
					tile_pos = (1,3),
					conected_tiles = ['local',1])
		self.tlist['local'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Dead scrub',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'There is a dead scrub here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 1,
					tile_pos = (6,7))
		self.tlist['local'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Tree sepling',
					tile_color = 'brown',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'tree_grow',
					damage = 0, 
					move_message = 'There is a sepling here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 1,
					tile_pos = (4,1),
					conected_items = (il.ilist['misc'][48]),
					conected_tiles = ['local',11])
		self.tlist['local'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Tree young',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'tree',
					grow_group = 'tree_grow',
					damage = 0, 
					move_message = 'There is a young tree here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (4,0),
					conected_tiles = ['local',12],
					conected_resources = ('wood',1))
		self.tlist['local'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Tree',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'tree',
					grow_group = 'tree',
					damage = 0, 
					move_message = 'There is a tree here.', 
					damage_message = None,
					tile_pos = (13,15),
					upper_tile = (12,15),
					transparency = False,
					special_group = 'flamable',
					special_num = 5,
					conected_tiles = [('local',10),('local',13)],
					conected_resources = ('wood',5))
		self.tlist['local'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Tree dead',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'tree',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'There is a dead tree here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 4,
					tile_pos = (11,15),
					upper_tile = (10,15),
					transparency = False,
					conected_resources = ('wood',3))
		self.tlist['local'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Rock',
					tile_color = 'grey',
					use_group = 'None',
					move_group = 'rock',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You break a big rock.', 
					damage_message = None,
					destroy = 2,
					tile_pos = (3,8),
					conected_resources = ('stone',1))
		self.tlist['local'].append(t)
		techID+=1
		#15
		t=tile(techID = techID,
					name = 'Herbs',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'herblike',
					damage = 0, 
					move_message = 'Herbs grow at the ground.', 
					damage_message = None,
					tile_pos = (1,9),
					conected_tiles = ['local',16])
		self.tlist['local'].append(t)
		techID+=1
		#16
		t=tile(techID = techID,
					name = 'Flowering herbs',
					tile_color = 'white',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'herblike',
					flower = True,
					damage = 0, 
					move_message = 'Flowering herbs grow at the ground.', 
					damage_message = None,
					tile_pos = (0,9),
					conected_tiles = ['local',15],
					conected_resources = ('herbs',1))
		self.tlist['local'].append(t)
		techID+=1
		#17
		t=tile(techID = techID,
					name = 'Scrub with blue berries',
					tile_color = 'light_blue',
					use_group = 'gather_scrub',
					move_group = 'soil',
					grow_group = 'scrub_berries',
					damage = 0, 
					move_message = 'There is a scrub with blue berries here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (1,5),
					conected_items = (il.ilist['food'][27]),
					conected_tiles = [('local',5),('local',6),('local',1)])
		self.tlist['local'].append(t)
		techID+=1
		#18
		t=tile(techID = techID,
					name = 'Scrub with yellow berries',
					tile_color = 'yellow',
					use_group = 'gather_scrub',
					move_group = 'soil',
					grow_group = 'scrub_berries',
					damage = 0, 
					move_message = 'There is a scrub with yellow berries here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (4,4),
					conected_items = (il.ilist['food'][28]),
					conected_tiles = [('local',5),('local',6),('local',1)])
		self.tlist['local'].append(t)
		
		techID = 6000

		self.tlist['building'] = []
		#0
		t=tile(techID = techID,
					name = 'Floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					indoor = True,
					move_message = 'You walk over a wooden floor.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (7,5))
		self.tlist['building'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					roofed = True,
					tile_pos = (2,5))
		self.tlist['building'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Door open',
					tile_color = 'white_stair',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk through a open door.', 
					damage_message = None,
					civilisation = True,
					build_here = False,
					roofed = True,
					tile_pos = (7,10),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['building'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Door closed',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The door swings open.', 
					damage_message = None,
					destroy = 1,
					replace = self.tlist['building'][2],
					civilisation = True,
					transparency = False,
					special_group = 'flamable',
					special_num = 3,
					roofed = True,
					tile_pos = (6,6))
		self.tlist['building'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Agriculture',
					tile_color = 'yellow',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'agri0',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking on bare fields.', 
					damage_message = None,
					tile_pos = (9,7),
					conected_tiles = [('building',5),('building',6),('building',25)])
		self.tlist['building'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Camp floor',
					tile_color = 'grey',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You walk over tight soil.', 
					damage_message = None,
					tile_pos = (9,0))
		self.tlist['building'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Budding agriculture (caves)',
					tile_color = 'yellow',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0, 
					civilisation = True,
					move_message = 'Something starts to grow at this field.', 
					damage_message = None,
					tile_pos = (9,8),
					conected_tiles = ['building',8])
		self.tlist['building'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Agriculture crops',
					tile_color = 'yellow',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'The crops stand high here.', 
					damage_message = None,
					tile_pos = (8,10),
					conected_items = (il.ilist['food'][6]),
					replace = self.tlist['building'][4])
		self.tlist['building'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Agriculture mushroom',
					tile_color = 'yellow',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'A great mushroom grows at this field.', 
					damage_message = None,
					tile_pos = (8,9),
					conected_items = (il.ilist['food'][11]),
					replace = self.tlist['building'][4])
		self.tlist['building'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Blue floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a blue floor.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (7,4))
		self.tlist['building'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Blue wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					roofed = True,
					tile_pos = (2,4))
		self.tlist['building'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Green floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a green floor.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (7,3))
		self.tlist['building'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Green wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					roofed = True,
					tile_pos = (2,3))
		self.tlist['building'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Red floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a red floor.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (7,0))
		self.tlist['building'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Red wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					roofed = True,
					tile_pos = (2,0))
		self.tlist['building'].append(t)
		techID+=1
		#15
		t=tile(techID = techID,
					name = 'Orange floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a orange floor.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (7,2))
		self.tlist['building'].append(t)
		techID+=1
		#16
		t=tile(techID = techID,
					name = 'Orange wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					roofed = True,
					tile_pos = (2,2))
		self.tlist['building'].append(t)
		techID+=1
		#17
		t=tile(techID = techID,
					name = 'Purple floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a purple floor.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (7,1))
		self.tlist['building'].append(t)
		techID+=1
		#18
		t=tile(techID = techID,
					name = 'Purple wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					roofed = True,
					tile_pos = (2,1))
		self.tlist['building'].append(t)
		techID+=1
		#19
		t=tile(techID = techID,
					name = 'Orcish floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a hard soil.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (0,5))
		self.tlist['building'].append(t)
		techID+=1
		#20
		t=tile(techID = techID,
					name = 'Orcish wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					roofed = True,
					tile_pos = (0,4))
		self.tlist['building'].append(t)
		techID+=1
		#21
		t=tile(techID = techID,
					name = 'Noble floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a floor made of stone.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (0,3))
		self.tlist['building'].append(t)
		techID+=1
		#22
		t=tile(techID = techID,
					name = 'Noble wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					roofed = True,
					tile_pos = (2,6))
		self.tlist['building'].append(t)
		techID+1
		#23
		t=tile(techID = techID,
					name = 'Elfish floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over cobbled floor.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (7,8))
		self.tlist['building'].append(t)
		techID+=1
		#24
		t=tile(techID = techID,
					name = 'Elfish wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					roofed = True,
					tile_pos = (7,6))
		self.tlist['building'].append(t)
		techID+1
		#25
		t=tile(techID = techID,
					name = 'Budding agriculture (desert)',
					tile_color = 'yellow',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0, 
					move_message = 'Something starts to grow at this field.', 
					damage_message = None,
					tile_pos = (6,13),
					conected_tiles = ['building',26])
		self.tlist['building'].append(t)
		techID+=1
		#26
		t=tile(techID = techID,
					name = 'Agriculture corn',
					tile_color = 'yellow',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The corn stands high here.', 
					damage_message = None,
					tile_pos = (7,13),
					conected_items = (il.ilist['food'][38]),
					replace = self.tlist['building'][4])
		self.tlist['building'].append(t)
		techID+=1
		#27
		t=tile(techID = techID,
					name = 'Door open',
					tile_color = 'white_stair',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk through a open door.', 
					damage_message = None,
					civilisation = True,
					roofed = True,
					tile_pos = (7,10),
					ignore_liquid = True)
		self.tlist['building'].append(t)
		#28
		t=tile(techID = techID,
					name = 'Pasture',
					tile_color = 'green',
					use_group = 'pasture',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over a pasture.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (18,11))
		self.tlist['building'].append(t)
		
		techID = 7000

		self.tlist['help'] = []
		#0
		t=tile(techID = techID,
					name = 'scrub_here',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over a wooden floor.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (7,5))
		self.tlist['help'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'tree_here',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					civilisation = True,
					transparency = False,
					tile_pos = (0,0))
		self.tlist['help'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'water_here',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk through a open door.', 
					damage_message = None,
					civilisation = True,
					build_here = False,
					tile_pos = (0,0))
		self.tlist['help'].append(t)

		techID = 8000
	
		self.tlist['sanctuary'] = []
		#0
		t=tile(techID = techID,
					name = 'Sanctuary floor',
					tile_color = 'light_purple',
					use_group = 'None',
					move_group = 'holy',
					grow_group = 'None',
					damage = -1, 
					move_message = 'You walk over holy ground.', 
					damage_message = 'Your wounds are cured.',
					build_here = False,
					tile_pos = (4,10))
		self.tlist['sanctuary'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Sanctuary Pilar',
					tile_color = 'purple',
					use_group = 'None',
					move_group = 'holy_solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a pilar here.', 
					damage_message = None,
					build_here = False,
					transparency = False,
					tile_pos = (4,9))
		self.tlist['sanctuary'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Sanctuary spawnpoint',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over holy ground.', 
					damage_message = 'Your wounds are cured.',
					build_here = False,
					tile_pos = (2,10))
		self.tlist['sanctuary'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Sanctuary flame',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'holy_solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'FIRE.', 
					damage_message = 'Your wounds are cured.',
					build_here = False,
					light_emit = 5,
					tile_pos = (3,14,4,14))
		self.tlist['sanctuary'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Nest box',
					tile_color = 'light_red',
					use_group = 'nest',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a nest box here.', 
					damage_message = 'Your wounds are cured.',
					build_here = False,
					tile_pos = (7,14))
		self.tlist['sanctuary'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Nest box with egg',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'holy',
					grow_group = 'hatch',
					damage = 0, 
					move_message = 'You see a nest box with an egg here.', 
					damage_message = 'Your wounds are cured.',
					build_here = False,
					tile_pos = (9,14))
		self.tlist['sanctuary'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'players room enter',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'enter_players_room',
					grow_group = 'None',
					damage = 0,
					move_message = 'A simple door.', 
					damage_message = None,
					build_here = False,
					transparency = False,
					tile_pos = (6,6),
					ignore_liquid = True)
		self.tlist['sanctuary'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'players room exit',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'exit_players_room',
					grow_group = 'None',
					damage = 0,
					move_message = 'A simple door.', 
					damage_message = None,
					build_here = False,
					transparency = False,
					tile_pos = (6,6),
					ignore_liquid = True)
		self.tlist['sanctuary'].append(t)
		
		techID = 9000
		
		self.tlist['portal'] = []
		#0
		t=tile(techID = techID,
					name = 'Inactive elfish portal',
					tile_color = 'light_purple',
					use_group = 'activate_portal',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of a purple portal.', 
					damage_message = None,
					build_here = False,
					tile_pos = (9,13),
					conected_tiles = ['portal',1],
					conected_resources = ('gem',5),
					special_num = 1)
		self.tlist['portal'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Active elfish portal',
					tile_color = 'light_purple',
					use_group = 'go_fortress',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of an activated portal.', 
					damage_message = None,
					build_here = False,
					light_emit = 2,
					tile_pos = (10,13,11,13))
		self.tlist['portal'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Elfish exit portal',
					tile_color = 'light_purple',
					use_group = 'return_fortress',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of an activated portal.', 
					damage_message = None,
					build_here = False,
					light_emit = 2,
					tile_pos = (10,13,11,13))
		self.tlist['portal'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Inactive desert portal',
					tile_color = 'light_purple',
					use_group = 'activate_portal',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of a yellow portal.', 
					damage_message = None,
					build_here = False,
					tile_pos = (13,2),
					conected_tiles = ['portal',4],
					conected_resources = ('gem',12),
					special_num = 6)
		self.tlist['portal'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Active desert portal',
					tile_color = 'light_purple',
					use_group = 'go_desert',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of an activated portal.', 
					damage_message = None,
					build_here = False,
					light_emit = 2,
					tile_pos = (13,3,13,4))
		self.tlist['portal'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Desert exit portal',
					tile_color = 'light_purple',
					use_group = 'return_desert',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of an activated portal.', 
					damage_message = None,
					build_here = False,
					light_emit = 2,
					tile_pos = (13,3,13,4))
		self.tlist['portal'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Inactive grassland portal',
					tile_color = 'light_purple',
					use_group = 'activate_portal',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of a green portal.', 
					damage_message = None,
					build_here = False,
					tile_pos = (0,14),
					conected_tiles = ['portal',7],
					conected_resources = ('gem',5),
					special_num = 1)
		self.tlist['portal'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Active grassland portal',
					tile_color = 'light_purple',
					use_group = 'go_grassland',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of an activated portal.', 
					damage_message = None,
					build_here = False,
					light_emit = 2,
					tile_pos = (1,14,2,14))
		self.tlist['portal'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Grassland exit portal',
					tile_color = 'light_purple',
					use_group = 'return_grassland',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of an activated portal.', 
					damage_message = None,
					build_here = False,
					light_emit = 2,
					tile_pos = (1,14,2,14))
		self.tlist['portal'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Gilmenor portal',
					tile_color = 'light_purple',
					use_group = 'gilmenor_portal',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of a portal plate.', 
					damage_message = None,
					build_here = False,
					tile_pos = (8,14))
		self.tlist['portal'].append(t)
		#10
		t=tile(techID = techID,
					name = 'demonic exit portal',
					tile_color = 'light_purple',
					use_group = 'exit_keep',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of an unholy portal.', 
					damage_message = None,
					build_here = False,
					light_emit = 2,
					tile_pos = (0,17,1,17))
		self.tlist['portal'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Active demon portal',
					tile_color = 'light_purple',
					use_group = 'go_keep',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0,
					move_message = 'You stand in front of an unholy portal.', 
					damage_message = None,
					build_here = False,
					light_emit = 2,
					tile_pos = (0,17,1,17))
		self.tlist['portal'].append(t)
		techID+=1
		
		techID = 10000
		
		self.tlist['shop'] = []
		#0
		t=tile(techID = techID,
					name = 'Shop floor',
					tile_color = 'light_purple',
					use_group = 'None',
					move_group = 'shop',
					grow_group = 'None',
					damage = 0,
					move_message = 'You are inside a shop.', 
					damage_message = None,
					build_here = False,
					tile_pos = (0,3))
		self.tlist['shop'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Shop wall',
					tile_color = 'purple',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0,
					move_message = 'You are inside a shop.', 
					damage_message = None,
					build_here = False,
					transparency = False,
					tile_pos = (2,6))
		self.tlist['shop'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Shop door',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'shop',
					grow_group = 'None',
					damage = 0,
					move_message = 'You are entering a shop.', 
					damage_message = None,
					build_here = False,
					tile_pos = (7,10),
					ignore_liquid = True)
		self.tlist['shop'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Shop exit',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'shop_exit',
					grow_group = 'None',
					damage = 0,
					move_message = 'You are leaving a shop.', 
					damage_message = None,
					build_here = False,
					transparency = False,
					tile_pos = (6,6))
		self.tlist['shop'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Shop portal',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'shop_enter',
					grow_group = 'None',
					damage = 0,
					move_message = 'You are entering a shop.', 
					damage_message = None,
					build_here = False,
					transparency = False,
					tile_pos = (8,13))
		self.tlist['shop'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Sign',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = '[SHOP]', 
					damage_message = None,
					build_here = False,
					tile_pos = (9,9))
		self.tlist['shop'].append(t)
		
		techID =11000

		self.tlist['effect'] = []
		#0
		t=tile(techID = techID,
					name = 'Bomb', #Bomb3
					tile_color = 'red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The time is ticking.', 
					damage_message = 'None',
					build_here = False,
					light_emit = 1,
					tile_pos = (9,3))
		self.tlist['effect'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Bomb2',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The time is ticking.', 
					damage_message = 'None',
					build_here = False,
					light_emit = 1,
					tile_pos = (9,4))
		self.tlist['effect'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Bomb1',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The time is ticking.', 
					damage_message = 'None',
					build_here = False,
					light_emit = 1,
					tile_pos = (9,5))
		self.tlist['effect'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Boom',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'boom',
					grow_group = 'None',
					damage = 10, 
					move_message = 'BOOM', 
					damage_message = 'BOOM',
					build_here = False,
					light_emit = 2,
					tile_pos = (9,6))
		self.tlist['effect'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Flame',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'house',
					grow_group = 'None',
					damage = 1, 
					move_message = 'A fire burns here.', 
					damage_message = 'The fire burns your flesh.',
					build_here = False,
					special_group = 'hot',
					light_emit = 5,
					tile_pos = (6,5,13,0))
		self.tlist['effect'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Healing Aura',
					tile_color = 'light_blue',
					use_group = 'None',
					move_group = 'holy',
					grow_group = 'None',
					damage = -1, 
					move_message = 'A healing aura surrounds you.', 
					damage_message = 'Your wounds are cured.',
					build_here = False,
					light_emit = 2,
					tile_pos = (1,10))
		self.tlist['effect'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Elbereth',
					tile_color = 'light_blue',
					use_group = 'None',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The magic word protects this place.', 
					damage_message = None,
					build_here = False,
					tile_pos = (10,7))
		self.tlist['effect'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Explosive',
					tile_color = 'light_red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					special_group = 'highly explosive',
					move_message = 'A explosive lies on the ground here.', 
					damage_message = None,
					build_here = False,
					conected_items = (il.ilist['misc'][72]),
					tile_pos = (4,13))
		self.tlist['effect'].append(t)
		
		techID = 12000
	
		self.tlist['elfish'] = []
		#0
		t=tile(techID = techID,
					name = 'Elfish floor indoor',
					tile_color = 'light_purple',
					use_group = 'None',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over a cobbled floor.', 
					damage_message = None,
					roofed = True,
					tile_pos = (7,8))
		self.tlist['elfish'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Elfish floor outdoor',
					tile_color = 'light_purple',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over gritty floor.', 
					damage_message = None,
					tile_pos = (7,7))
		self.tlist['elfish'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Elfish agriculture',
					tile_color = 'light_green',
					use_group = 'resource',
					move_group = 'dry_entrance',
					grow_group = 'None',
					damage = 0, 
					move_message = 'Some strange plants grow at this field.', 
					damage_message = None,
					tile_pos = (7,9),
					conected_tiles = ['elfish',6],
					conected_resources = ('seeds',1))
		self.tlist['elfish'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Elfish wall',
					tile_color = 'purple',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					build_here = False,
					transparency = False,
					roofed = True,
					tile_pos = (7,6))
		self.tlist['elfish'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'help_active',
					tile_color = 'black',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'ACTIVE', 
					damage_message = None,
					build_here = False,
					transparency = False,
					tile_pos = (0,0))
		self.tlist['elfish'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'help_passive',
					tile_color = 'black',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'PASSIVE', 
					damage_message = None,
					build_here = False,
					transparency = False,
					tile_pos = (0,0))
		self.tlist['elfish'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Elfish agriculture',
					tile_color = 'light_green',
					use_group = 'None',
					move_group = 'dry_entrance',
					grow_group = 'herblike',
					damage = 0,
					move_message = 'You are walking on bare fields.', 
					damage_message = None,
					tile_pos = (9,7),
					conected_tiles = ['elfish',2])
		self.tlist['elfish'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Elfish floor deco',
					tile_color = 'light_purple',
					use_group = 'None',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over a solid floor.', 
					damage_message = None,
					roofed = True,
					tile_pos = (7,1))
		self.tlist['elfish'].append(t)
		
		techID= 13000
		
		self.tlist['extra'] = []
		#0
		t=tile(techID = techID,
					name = 'Sand',
					tile_color = 'light_yellow',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over hot sand.', 
					damage_message = None,
					build_here = True,
					can_grown = True,
					tile_pos = (4,5))
		self.tlist['extra'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Sandstone floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over solid ground.', 
					damage_message = None,
					build_here = False,
					tile_pos = (4,6))
		self.tlist['extra'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Sandstone wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					build_here = False,
					transparency = False,
					tile_pos = (4,7))
		self.tlist['extra'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Small cactus',
					tile_color = 'green',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'tree_sepling',
					damage = 0, 
					move_message = 'There is a small cactus here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 1,
					tile_pos = (2,7),
					conected_items = (il.ilist['misc'][49]),
					conected_tiles = ['extra',4])
		self.tlist['extra'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Young cactus',
					tile_color = 'green',
					use_group = 'None',
					move_group = 'tree',
					grow_group = 'tree_young',
					damage = 0, 
					move_message = 'There is a young cactus here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (2,8),
					conected_tiles = ['extra',5],
					conected_resources = ('wood',1))
		self.tlist['extra'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Cactus',
					tile_color = 'green',
					use_group = 'None',
					move_group = 'tree',
					grow_group = 'tree',
					damage = 0, 
					move_message = 'There is a tree here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (2,9),
					transparency = False,
					conected_tiles = [('extra',3),('extra',5)],
					conected_resources = ('wood',5))
		self.tlist['extra'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Guidepost Desert',
					tile_color = 'red',
					use_group = 'go_desert',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = '[DESERT]', 
					damage_message = None,
					build_here = False,
					tile_pos = (9,9))
		self.tlist['extra'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Guidepost Homeward',
					tile_color = 'red',
					use_group = 'return_desert',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = '[Womeward]', 
					damage_message = None,
					build_here = False,
					tile_pos = (9,9))
		self.tlist['extra'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Bridge (N-S)',
					tile_color = 'light_brown',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over a wooden bridge.', 
					damage_message = None,
					build_here = False,
					tile_pos = (4,8))
		self.tlist['extra'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Sandstone door',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk through a door.', 
					damage_message = None,
					build_here = False,
					tile_pos = (9,10),
					ignore_liquid = True)
		self.tlist['extra'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Palm sepling',
					tile_color = 'brown',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'tree_grow',
					damage = 0, 
					move_message = 'There is a palm sepling here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 1,
					tile_pos = (10,3),
					conected_items = (il.ilist['misc'][50]),
					conected_tiles = ['extra',11])
		self.tlist['extra'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Palm young',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'tree',
					grow_group = 'tree_grow',
					damage = 0, 
					move_message = 'There is a young palm here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (10,1),
					conected_tiles = ['extra',12],
					conected_resources = ('wood',1))
		self.tlist['extra'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Palm',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'tree',
					grow_group = 'tree',
					damage = 0, 
					move_message = 'There is a palm here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (16,0),
					upper_tile = (15,0),
					transparency = False,
					conected_tiles = [('extra',10),('extra',13)],
					conected_resources = ('wood',5))
		self.tlist['extra'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Palm dead',
					tile_color = 'brown',
					use_group = 'None',
					move_group = 'tree',
					grow_group = 'vanish',
					damage = 0, 
					move_message = 'There is a dead palm here.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (16,1),
					upper_tile = (15,1),
					transparency = False,
					conected_resources = ('wood',3))
		self.tlist['extra'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Desert rock',
					tile_color = 'grey',
					use_group = 'None',
					move_group = 'rock',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You break a big rock.', 
					damage_message = None,
					destroy = 2,
					tile_pos = (10,0),
					conected_resources = ('stone',1))
		self.tlist['extra'].append(t)
		
		techID = 14000

		self.tlist['dungeon'] = []
		#0
		t=tile(techID = techID,
					name = 'Dungeon Floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over wooden floor.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (7,5))
		self.tlist['dungeon'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Dungeon corridor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk through a dark corridor.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (10,6))
		self.tlist['dungeon'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Dungeon Door Open',
					tile_color = 'white_stair',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk through a open door.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					roofed = True,
					tile_pos = (7,10),
					ignore_liquid = True)
		self.tlist['dungeon'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Dungeon Door',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'door',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The door swings open.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					transparency = False,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (6,6),
					conected_tiles = ['dungeon',2],
					replace = self.tlist['dungeon'][2])
		self.tlist['dungeon'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Dungeon Door Resist 1',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'door',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The door resists.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					transparency = False,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (6,6),
					conected_tiles = ['dungeon',3],
					replace = self.tlist['dungeon'][2])
		self.tlist['dungeon'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Dungeon Door Resist 2',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'door',
					grow_group = 'None',
					damage = 0, 
					move_message = 'The door resists.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					transparency = False,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (6,6),
					conected_tiles = ['dungeon',4],
					replace = self.tlist['dungeon'][2])
		self.tlist['dungeon'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Dungeon Door Secret',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You notice a hidden door.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					transparency = False,
					special_group = 'flamable',
					special_num = 3,
					tile_pos = (2,6),
					conected_tiles = ['dungeon',2],
					replace = self.tlist['dungeon'][2])
		self.tlist['dungeon'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Grassland Dungeon Stair Down',
					tile_color = 'white_stair',
					use_group = 'grassland_down',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a downleading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,10),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Grassland Dungeon Stair up',
					tile_color = 'white_stair',
					use_group = 'grassland_up',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a up leading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,9),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Dungeon Wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					transparency = False,
					tile_pos = (2,6))
		self.tlist['dungeon'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Trap Active',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'CLICK', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (10,10))#########10,10 must always stay transparent
		self.tlist['dungeon'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Trap Inactive',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a inactive trap here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (10,9))
		self.tlist['dungeon'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Acid Fountain',
					tile_color = 'blue',
					use_group = 'drink_acid',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a fountain.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (6,3,10,12))
		self.tlist['dungeon'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Healing Fountain',
					tile_color = 'blue',
					use_group = 'drink_heal',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a fountain.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (6,3,10,12))
		self.tlist['dungeon'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Grot Stair Down',
					tile_color = 'white_stair',
					use_group = 'grot_down',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a downleading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,13),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		#15
		t=tile(techID = techID,
					name = 'Grot Stair up',
					tile_color = 'white_stair',
					use_group = 'grot_up',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a up leading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (12,13),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		#16
		t=tile(techID = techID,
					name = 'Mine Stair Down',
					tile_color = 'white_stair',
					use_group = 'mine_down',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a downleading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,12),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		#17
		t=tile(techID = techID,
					name = 'Mine Stair up',
					tile_color = 'white_stair',
					use_group = 'mine_up',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a up leading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,11),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		#18
		t=tile(techID = techID,
					name = 'Tomb Stair Down',
					tile_color = 'white_stair',
					use_group = 'tomb_down',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a downleading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,8),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		#19
		t=tile(techID = techID,
					name = 'Tomb Stair up',
					tile_color = 'white_stair',
					use_group = 'tomb_up',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a up leading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,7),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		#20
		t=tile(techID = techID,
					name = 'Grand Chest',
					tile_color = 'light_purple',
					use_group = 'None',
					move_group = 'holy',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a grand chest here.', 
					damage_message = None,
					civilisation = False,
					build_here = False,
					tile_pos = (6,8),
					drops_here = False)
		self.tlist['dungeon'].append(t)
		#21
		t=tile(techID = techID,
					name = 'Elfish Underground Stair Down',
					tile_color = 'white_stair',
					use_group = 'elfish_down',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a downleading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (3,4),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		#22
		t=tile(techID = techID,
					name = 'Elfish Underground Stair up',
					tile_color = 'white_stair',
					use_group = 'elfish_up',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a up leading stair here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (3,3),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['dungeon'].append(t)
		techID+=1
		
		techID = 15000
		
		self.tlist['statue'] = []
		#0
		t=tile(techID = techID,
					name = 'Naga statue',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a naga statue.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (11,6),
					conected_items = (il.ilist['misc'][55]))
		self.tlist['statue'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Orc statue',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a orc statue.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (11,7),
					conected_items = (il.ilist['misc'][56]))
		self.tlist['statue'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Blob statue',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a blob statue.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (11,8),
					conected_items = (il.ilist['misc'][57]))
		self.tlist['statue'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Elf statue',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a elf statue.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (11,9),
					conected_items = (il.ilist['misc'][58]))
		self.tlist['statue'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Neko statue',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a neko statue.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (11,10),
					conected_items = (il.ilist['misc'][59]))
		self.tlist['statue'].append(t)
		
		techID = 16000

		self.tlist['deco'] = []
		#0
		t=tile(techID = techID,
					name = 'Pendulum clock',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a pendulum clock.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (0,12,1,12),
					conected_items = (il.ilist['misc'][60]))
		self.tlist['deco'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Coffin',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of an old coffin.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 2,
					tile_pos = (2,12),
					conected_items = (il.ilist['misc'][61]))
		self.tlist['deco'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Candleholder',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a candleholder at this place.', 
					damage_message = None,
					civilisation = True,
					light_emit = 5,
					tile_pos = (3,12),
					conected_items = (il.ilist['misc'][62]))
		self.tlist['deco'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Hourglass',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a big hourglass.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (4,12,5,12),
					conected_items = (il.ilist['misc'][63]))
		self.tlist['deco'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Ancient idol',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of an ancient idol.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (6,12),
					conected_items = (il.ilist['misc'][64]))
		self.tlist['deco'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Sarcophagus',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of an old sarcophagus.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (7,12),
					conected_items = (il.ilist['misc'][65]))
		self.tlist['deco'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Bonsai',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a bonsai at this place.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					special_num = 1,
					tile_pos = (9,11),
					conected_items = (il.ilist['misc'][66]))
		self.tlist['deco'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Houseplant',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					flower = True,
					damage = 0, 
					move_message = 'There is a houseplant at this place.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (9,12),
					conected_items = (il.ilist['misc'][67]))
		self.tlist['deco'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Wooden throne',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a wooden throne.', 
					damage_message = None,
					special_group = 'flamable',
					special_num = 2,
					civilisation = True,
					tile_pos = (6,11),
					conected_items = (il.ilist['misc'][68]))
		self.tlist['deco'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Stony throne',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a stony throne.', 
					damage_message = None,
					tile_pos = (7,11),
					conected_items = (il.ilist['misc'][69]))
		self.tlist['deco'].append(t)
		#10
		t=tile(techID = techID,
					name = 'Menhir',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You stand in front of a menhir.', 
					damage_message = None,
					tile_pos = (16,7),
					conected_items = (il.ilist['misc'][113]))
		self.tlist['deco'].append(t)
		#11
		t=tile(techID = techID,
					name = 'Red Carpet',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a fluffy red carpet.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (18,0))
		self.tlist['deco'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Blue Carpet',
					tile_color = 'light_blue',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a fluffy blue carpet.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (18,1))
		self.tlist['deco'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Orange Carpet',
					tile_color = 'light_yellow',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a fluffy orange carpet.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (18,2))
		self.tlist['deco'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Green Carpet',
					tile_color = 'light_green',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a fluffy green carpet.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (18,3))
		self.tlist['deco'].append(t)
		techID+=1
		#15
		t=tile(techID = techID,
					name = 'Purple Carpet',
					tile_color = 'light_purple',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					indoor = True,
					move_message = 'You walk over a fluffy red carpet.', 
					damage_message = None,
					civilisation = True,
					tile_pos = (18,4))
		self.tlist['deco'].append(t)
		techID+=1
		#16
		t=tile(techID = techID,
					name = 'Cut-Glass Window',
					tile_color = 'white_stair',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a cut-glass window here.', 
					damage_message = None,
					civilisation = True,
					transparency = True,
					tile_pos = (18,10))
		self.tlist['deco'].append(t)
		techID+=1
		#17
		t=tile(techID = techID,
					name = 'Table with Candle',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a table with a candle here.', 
					damage_message = None,
					transparency = True,
					light_emit = 5,
					tile_pos = (11,18,12,18))
		self.tlist['deco'].append(t)
		techID+=1
		
		techID = 17000
		
		self.tlist['quest'] = []
		#0
		t=tile(techID = techID,
					name = 'Lost tool box',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a tool box here.', 
					damage_message = None,
					tile_pos = (7,15),
					conected_items = (il.ilist['misc'][92]))
		self.tlist['quest'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Neko amulet',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a neko amulet on the ground.', 
					damage_message = None,
					tile_pos = (8,15),
					conected_items = (il.ilist['misc'][102]))
		self.tlist['quest'].append(t)
		
		techID = 18000
		
		self.tlist['toys'] = []
		#0
		t=tile(techID = techID,
					name = 'Transmitter',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You see a transmitter on the ground.', 
					damage_message = None,
					tile_pos = (14,1),
					conected_items = (il.ilist['misc'][93]))
		self.tlist['toys'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Transmitter (Off)',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You see a transmitter on the ground.', 
					damage_message = None,
					tile_pos = (14,2))
		self.tlist['toys'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Transmitter (On)',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You see a transmitter on the ground.', 
					damage_message = None,
					light_emit = 2,
					tile_pos = (14,3))
		self.tlist['toys'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Signal',
					tile_color = 'light_red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You see a signal here.', 
					damage_message = None,
					light_emit = 4,
					tile_pos = (14,4),
					conected_items = (il.ilist['misc'][94]))
		self.tlist['toys'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Preasure Plate', #unused ATM
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You see a green signal here.', 
					damage_message = None,
					tile_pos = (14,5),
					conected_items = (il.ilist['misc'][94]))
		self.tlist['toys'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Switch',
					tile_color = 'white',
					use_group = 'switch',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You see a switch here.', 
					damage_message = None,
					tile_pos = (14,6),
					conected_items = (il.ilist['misc'][95]))
		self.tlist['toys'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Auto-Door',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You walk trough an open Auto-Door.', 
					damage_message = None,
					transparency = False,
					tile_pos = (14,8),
					conected_items = (il.ilist['misc'][96]))
		self.tlist['toys'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Pressure Plate',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You walk over a pressure plate.', 
					damage_message = None,
					tile_pos = (14,9),
					conected_items = (il.ilist['misc'][97]))
		self.tlist['toys'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Fire Pit',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					civilisation = True, 
					move_message = 'You walk over a fire pit.', 
					damage_message = None,
					tile_pos = (14,10),
					conected_items = (il.ilist['misc'][98]))
		self.tlist['toys'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Fire Pit (burning)',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 1,
					move_message = 'This fire pit is burning.', 
					damage_message = 'The fire burns your flesh.',
					build_here = False,
					special_group = 'hot',
					light_emit = 5,
					tile_pos = (14,11,14,12))
		self.tlist['toys'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Timed Emitter',
					tile_color = 'white',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					civilisation = True,
					move_message = 'You see a timed emitter here.', 
					damage_message = None,
					tile_pos = (14,14),
					conected_items = (il.ilist['misc'][99]))
		self.tlist['toys'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Thinker\'s Workshop',
					tile_color = 'white',
					use_group = 'thinker_workshop',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					civilisation = True, 
					move_message = 'You see a thinker\'s workshop here.', 
					damage_message = None,
					tile_pos = (14,13),
					conected_items = (il.ilist['misc'][100]))
		self.tlist['toys'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Heated Furnace',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					move_message = 'This furnace emits a lot of heat.', 
					damage_message = None,
					build_here = False,
					special_group = 'hot',
					light_emit = 5,
					tile_pos = (4,2,4,3))
		self.tlist['toys'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Fence',
					tile_color = 'white',
					use_group = 'None',
					move_group = 'jump',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a fence.', 
					damage_message = None,
					civilisation = True,
					special_group = 'flamable',
					tile_pos = (15,10))
		self.tlist['toys'].append(t)
		
		techID = 19000
		
		self.tlist['flower'] = []
		#0
		t=tile(techID = techID,
					name = 'White flowers',
					tile_color = 'light_green',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					flower = True,
					damage = 0, 
					civilisation = False,
					move_message = 'Some white flowers grow here.', 
					damage_message = None,
					tile_pos = (16,2),
					conected_items = (il.ilist['misc'][108]))
		self.tlist['flower'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Red flowers',
					tile_color = 'light_green',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					flower = True,
					damage = 0, 
					civilisation = False,
					move_message = 'Some red flowers grow here.', 
					damage_message = None,
					tile_pos = (16,3),
					conected_items = (il.ilist['misc'][109]))
		self.tlist['flower'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Blue flowers',
					tile_color = 'light_green',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					flower = True,
					damage = 0, 
					civilisation = False,
					move_message = 'Some blue flowers grow here.', 
					damage_message = None,
					tile_pos = (16,4),
					conected_items = (il.ilist['misc'][110]))
		self.tlist['flower'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Purple flowers',
					tile_color = 'light_green',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					flower = True,
					damage = 0, 
					civilisation = False,
					move_message = 'Some purple flowers grow here.', 
					damage_message = None,
					tile_pos = (16,5),
					conected_items = (il.ilist['misc'][111]))
		self.tlist['flower'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Yellow flowers',
					tile_color = 'light_green',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					flower = True,
					damage = 0, 
					civilisation = False,
					move_message = 'Some yellow flowers grow here.', 
					damage_message = None,
					tile_pos = (16,6),
					conected_items = (il.ilist['misc'][112]))
		self.tlist['flower'].append(t)
		techID+=1

		techID = 20000
		
		self.tlist['demon'] = []
		#0
		t=tile(techID = techID,
					name = 'Demonic Wall',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'There is a wall here.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					transparency = False,
					tile_pos = (10,16))
		self.tlist['demon'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Demonic Floor',
					tile_color = 'light_red',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You walk over solid ground.', 
					damage_message = None,
					tile_pos = (0,5))
		self.tlist['demon'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Prison Bars',
					tile_color = 'red',
					use_group = 'None',
					move_group = 'solid',
					grow_group = 'None',
					damage = 0, 
					move_message = 'some bars block your way.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					transparency = True,
					tile_pos = (16,16))
		self.tlist['demon'].append(t)
		techID+=1
		
		techID = 21000
		
		self.tlist['hole'] = []
		#0
		t=tile(techID = techID,
					name = 'Hole',
					tile_color = 'black',
					use_group = 'None',
					move_group = 'jump',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You see a hole.', 
					damage_message = None,
					civilisation = False,
					tile_pos = (3,17))
		self.tlist['hole'].append(t)
		#1
		t=tile(techID = techID,
					name = 'Filled Hole',
					tile_color = 'black',
					use_group = 'None',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You step upon a boulder.', 
					damage_message = None,
					civilisation = False,
					tile_pos = (4,17))
		self.tlist['hole'].append(t)
		
		techID = 22000
		
		self.tlist['key'] = []
		#0
		t=tile(techID = techID,
					name = 'Key',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A key lies on the ground here.', 
					damage_message = None,
					conected_items = il.ilist['misc'][116],
					tile_pos = (15,13))
		self.tlist['key'].append(t)
		techID+=1
		#0
		t=tile(techID = techID,
					name = 'Golden Key',
					tile_color = 'red',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0, 
					move_message = 'A golden key lies on the ground here.', 
					damage_message = None,
					conected_items = il.ilist['misc'][140],
					light_emit = 1,
					tile_pos = (15,16))
		self.tlist['key'].append(t)
		techID+=1
		
		techID = 23000
		
		self.tlist['farm'] = []
		#0
		t=tile(techID = techID,
					name = 'Farm Plot',
					tile_color = 'white_stair',
					use_group = 'farm',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over bare fields.', 
					damage_message = None,
					ignore_liquid = True,
					tile_pos = (9,7))
		self.tlist['farm'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Watered Farm Plot',
					tile_color = 'white_stair',
					use_group = 'farm',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over well watered fields.', 
					damage_message = None,
					ignore_liquid = True,
					tile_pos = (10,18))
		self.tlist['farm'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Poluted Farm Plot',
					tile_color = 'white_stair',
					use_group = 'farm',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over stinking fields.', 
					damage_message = None,
					ignore_liquid = True,
					tile_pos = (6,0))
		self.tlist['farm'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Lava Farm Plot',
					tile_color = 'white_stair',
					use_group = 'farm',
					move_group = 'soil',
					grow_group = 'None',
					damage = 0,
					special_group = 'hot',
					civilisation = True, 
					move_message = 'You are walking over bare fields. The ground feels hot.', 
					damage_message = None,
					ignore_liquid = True,
					light_emit = 3,
					tile_pos = (10,4))
		self.tlist['farm'].append(t)
		techID+=1
		
		techID = 24000
		
		self.tlist['seed'] = []
		#0
		t=tile(techID = techID,
					name = 'Grain Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',0],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Corn Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',1],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Watermelon Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',2],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Lotus Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',3],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Junk Leaf Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',4],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Glass Weed Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',5],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Burning Weed Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',6],
					ignore_liquid = True,
					light_emit = 3,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Fire Flower Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',7],
					ignore_liquid = True,
					light_emit = 5,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Cultivated Mushroom Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',8],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Sporeling Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',9],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Cave Herb Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',10],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Waterbag Mushroom Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',11],
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Goo Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',12],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Ironshroom Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',13],
					ignore_liquid = True,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Glowing Moss Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',14],
					ignore_liquid = True,
					light_emit = 3,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		#15
		t=tile(techID = techID,
					name = 'Red Mushroom Seeds',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					conected_tiles = ['plant1',15],
					ignore_liquid = True,
					light_emit = 3,
					tile_pos = (17,14))
		self.tlist['seed'].append(t)
		techID+=1
		
		techID = 25000
		
		self.tlist['plant1'] = []
		#0
		t=tile(techID = techID,
					name = 'Young Grain Plant',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',0],
					tile_pos = (8,8))
		self.tlist['plant1'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Young Corn Plant',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',1],
					tile_pos = (6,13))
		self.tlist['plant1'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Young Watermelon Plant',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',2],
					tile_pos = (5,17))
		self.tlist['plant1'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Young Lotus Plant',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',3],
					tile_pos = (7,17))
		self.tlist['plant1'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Young Junk Leaf Plant',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',4],
					tile_pos = (9,17))
		self.tlist['plant1'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Young Glass Weed',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',5],
					tile_pos = (11,17))
		self.tlist['plant1'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Young Burning Weed',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',6],
					light_emit = 4,
					tile_pos = (13,17))
		self.tlist['plant1'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Young Fire Flower',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',7],
					light_emit = 4,
					tile_pos = (15,17))
		self.tlist['plant1'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Young Cultivated Mushroom',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',8],
					tile_pos = (9,8))
		self.tlist['plant1'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Young Sporeling',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',9],
					tile_pos = (17,0))
		self.tlist['plant1'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Young Cave Herbs',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',10],
					tile_pos = (17,2))
		self.tlist['plant1'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Young Waterbag Mushroom',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',11],
					tile_pos = (17,4))
		self.tlist['plant1'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Young Goo',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',12],
					tile_pos = (17,6))
		self.tlist['plant1'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Young Ironshroom',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',13],
					tile_pos = (17,8))
		self.tlist['plant1'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Young Glowing Moss',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',14],
					light_emit = 4,
					tile_pos = (17,10))
		self.tlist['plant1'].append(t)
		techID+=1
		#15
		t=tile(techID = techID,
					name = 'Young Red Mushroom',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'agri1',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = ['plant2',15],
					light_emit = 4,
					tile_pos = (17,12))
		self.tlist['plant1'].append(t)
		techID+=1
		
		techID = 26000
		
		self.tlist['plant2'] = []
		#0
		t=tile(techID = techID,
					name = 'Grain Plant',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][6]),
					tile_pos = (8,10))
		self.tlist['plant2'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Corn Plant',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][38]),
					tile_pos = (7,13))
		self.tlist['plant2'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Watermelon Plant',
					tile_color = 'white_stair',
					use_group = 'none',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_tiles = (il.ilist['food'][63]),
					tile_pos = (6,17))
		self.tlist['plant2'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Lotus Plant',
					tile_color = 'white_stair',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_resources = ['herbs',3],
					tile_pos = (8,17))
		self.tlist['plant2'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Junk Leaf Plant',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][64]),
					tile_pos = (10,17))
		self.tlist['plant2'].append(t)
		techID+=1
		#5
		t=tile(techID = techID,
					name = 'Glass Weed',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][66]),
					tile_pos = (12,17))
		self.tlist['plant2'].append(t)
		techID+=1
		#6
		t=tile(techID = techID,
					name = 'Burning Weed',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][67]),
					light_emit = 5,
					tile_pos = (14,17))
		self.tlist['plant2'].append(t)
		techID+=1
		#7
		t=tile(techID = techID,
					name = 'Fire Flower',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					flower = True,
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][69]),
					light_emit = 5,
					tile_pos = (16,17))
		self.tlist['plant2'].append(t)
		techID+=1
		#8
		t=tile(techID = techID,
					name = 'Cultivated Mushroom',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][11]),
					tile_pos = (8,9))
		self.tlist['plant2'].append(t)
		techID+=1
		#9
		t=tile(techID = techID,
					name = 'Sporeling',
					tile_color = 'white_stair',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_resources = ['seeds',6],
					tile_pos = (17,1))
		self.tlist['plant2'].append(t)
		techID+=1
		#10
		t=tile(techID = techID,
					name = 'Cave Herbs',
					tile_color = 'white_stair',
					use_group = 'resource',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_resources = ['herbs',5],
					tile_pos = (17,3))
		self.tlist['plant2'].append(t)
		techID+=1
		#11
		t=tile(techID = techID,
					name = 'Waterbag Mushroom',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][71]),
					tile_pos = (17,5))
		self.tlist['plant2'].append(t)
		techID+=1
		#12
		t=tile(techID = techID,
					name = 'Goo',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][72]),
					tile_pos = (17,7))
		self.tlist['plant2'].append(t)
		techID+=1
		#13
		t=tile(techID = techID,
					name = 'Ironshroom',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][74]),
					tile_pos = (17,9))
		self.tlist['plant2'].append(t)
		techID+=1
		#14
		t=tile(techID = techID,
					name = 'Glowing Moss',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][76]),
					light_emit = 5,
					tile_pos = (17,11))
		self.tlist['plant2'].append(t)
		techID+=1
		#15
		t=tile(techID = techID,
					name = 'Red Mushroom',
					tile_color = 'white_stair',
					use_group = 'gather',
					move_group = 'soil',
					grow_group = 'none',
					damage = 0,
					civilisation = True, 
					move_message = 'You are walking over cultivated fields.', 
					damage_message = None,
					ignore_liquid = True,
					conected_items = (il.ilist['food'][78]),
					light_emit = 5,
					tile_pos = (17,13))
		self.tlist['plant2'].append(t)
		techID+=1
		
		techID = 27000
		
		self.tlist['arena_entrance'] = []
		#0
		t=tile(techID = techID,
					name = 'Dungeon Arena Stair Down',
					tile_color = 'white_stair',
					use_group = 'dungeon_arena',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You feel a evil aura from below.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,10),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['arena_entrance'].append(t)
		techID+=1
		#1
		t=tile(techID = techID,
					name = 'Tomb Arena Stair Down',
					tile_color = 'white_stair',
					use_group = 'tomb_arena',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You feel a evil aura from below.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,8),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['arena_entrance'].append(t)
		techID+=1
		#2
		t=tile(techID = techID,
					name = 'Sewers Arena Stair Down',
					tile_color = 'white_stair',
					use_group = 'sewers_arena',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You feel a evil aura from below.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (3,4),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['arena_entrance'].append(t)
		techID+=1
		#3
		t=tile(techID = techID,
					name = 'Orcish Mines Arena Stair Down',
					tile_color = 'white_stair',
					use_group = 'orcish_mines_arena',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You feel a evil aura from below.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,12),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['arena_entrance'].append(t)
		techID+=1
		#4
		t=tile(techID = techID,
					name = 'Grot Arena Stair Down',
					tile_color = 'white_stair',
					use_group = 'grot_arena',
					move_group = 'house',
					grow_group = 'None',
					damage = 0, 
					move_message = 'You feel a evil aura from below.', 
					damage_message = None,
					build_here = False,
					can_grown = False,
					tile_pos = (13,13),
					ignore_liquid = True,
					drops_here = False)
		self.tlist['arena_entrance'].append(t)
		techID+=1
