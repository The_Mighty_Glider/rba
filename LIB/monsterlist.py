#This file is part of RogueBox Adventures.
#
#    RogueBox Adventures is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    RogueBox Adventures is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with RogueBox Adventures.  If not, see <http://www.gnu.org/licenses/>.

from monster import monster
from attribute import attribute
from copy import deepcopy


class monsterlist():
	
	def __init__(self):
		self.mlist = {}
		techID = 0

		#grassland monster
		self.mlist['overworld'] = []
		#0
		m=monster(techID = techID,
				name = 'dryad',
				sprite_pos = (2,2),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'dryade',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				message = 'dryad',
				anger = 'tree',
				anger_monster = 2)
		techID+=1
		self.mlist['overworld'].append(m)
		#1
		m=monster(techID = techID,
				name = 'squirrel',
				sprite_pos = (6,7),
				move_border = 0,
				attribute_prev = (0,1,0,1,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'flee',
				corps_style = 'animal',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','tree'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				properties = ['cattle'],
				message = 'None')
		techID+=1
		self.mlist['overworld'].append(m)
		#2
		m=monster(techID = techID,
				name = 'rabbit',
				sprite_pos = (1,4),
				move_border = 0,
				attribute_prev = (0,1,0,1,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'flee',
				corps_style = 'animal',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'attack_melee', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['cattle'], 
				message = 'None')
		techID+=1
		self.mlist['overworld'].append(m)
		
		#cattle
		self.mlist['cattle'] = []
		#0
		m=monster(techID = techID,
				name = 'chicken',
				sprite_pos = (6,6),
				move_border = 2,
				attribute_prev = (0,0,1,1,0),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'ignore',
				corps_style = 'animal',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','house'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['cattle'],
				ability = {'produce_egg' : 1}, 
				memory = 'last_interaction = -1; wool_num = 0; interaction_type = "None"',
				message = 'cattle')
		techID+=1
		self.mlist['cattle'].append(m)
		#1
		m=monster(techID = techID,
				name = 'sheep',
				sprite_pos = (11,9),
				move_border = 3,
				attribute_prev = (0,0,1,1,0),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'ignore',
				corps_style = 'animal',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','house'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['cattle'], 
				memory = 'last_interaction = -1; wool_num = 3; interaction_type = "shearing"',
				message = 'cattle')
		techID+=1
		self.mlist['cattle'].append(m)
		#2
		m=monster(techID = techID,
				name = 'cow',
				sprite_pos = (12,0),
				move_border = 4,
				attribute_prev = (0,0,1,1,0),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'ignore',
				corps_style = 'animal',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','house'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['cattle'], 
				memory = 'last_interaction = -1; wool_num = 0; interaction_type = "milking"',
				message = 'cattle')
		techID+=1
		self.mlist['cattle'].append(m)

		#civilians
		self.mlist['civilian'] = []
		#0
		m=monster(techID = techID,
				name = 'dryad',
				sprite_pos = (5,8),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'dryade',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'The dryad smiles mysterious.')
		techID+=1
		self.mlist['civilian'].append(m)
		#1
		m=monster(techID = techID,
				name = 'wood elf',
				sprite_pos = (0,0),
				move_border = 2,
				attribute_prev = (0,2,0,2,1),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'The wood elf waves.')
		techID+=1
		self.mlist['civilian'].append(m)
		#2
		m=monster(techID = techID,
				name = 'tame orc (male)',
				sprite_pos = (0,4),
				move_border = 2,
				attribute_prev = (2,1,0,1,1),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'orc')
		techID+=1
		self.mlist['civilian'].append(m)
		#3
		m=monster(techID = techID,
				name = 'tame orc (female)',
				sprite_pos = (10,8),
				move_border = 2,
				attribute_prev = (2,1,0,1,1),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'orc')
		techID+=1
		self.mlist['civilian'].append(m)
		#4
		m=monster(techID = techID,
				name = ' female golden naga',
				sprite_pos = (1,9),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				memory = 'name = "Female Golden Naga";', 
				message = 'naga_fortress')
		techID+=1
		self.mlist['civilian'].append(m)
		#5
		m=monster(techID = techID,
				name = 'dwarf',
				sprite_pos = (5,7),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'The dwarf waves.')
		techID+=1
		self.mlist['civilian'].append(m)
		#6
		m=monster(techID = techID,
				name = ' male golden naga',
				sprite_pos = (4,11),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				memory = 'name = "Male Golden Naga";',
				message = 'naga_fortress')
		techID+=1
		self.mlist['civilian'].append(m)
		#7
		m=monster(techID = techID,
				name = ' naga librarian',
				sprite_pos = (1,9),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				quest_state = True,
				memory = 'first_meet = False;',
				message = 'naga_library')
		techID+=1
		self.mlist['civilian'].append(m)
		
		#rescued mobs
		
		self.mlist['rescued'] = []
		#0
		m=monster(techID = techID,
				name = 'Gilmenor',
				sprite_pos = (8,5),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'None',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'attack_monster' : 70,
							'warchant' : 1},
				properties = ['npc','villager','stay_at_lvl','no_dismiss'],
				memory = 'first_meet = True',
				message = 'gilmenor')
		techID+=1
		self.mlist['rescued'].append(m)
		#1
		m=monster(techID = techID,#gilmenor elysium
				name = 'Gilmenor',
				sprite_pos = (8,5),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'None',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'attack_monster' : 70,
							'warchant' : 1},
				properties = ['npc','villager'],
				quest_state = True,
				memory = 'first_meet = True',
				message = 'gilmenor_elysium')
		techID+=1
		self.mlist['rescued'].append(m)
		#2
		m=monster(techID = techID,#lillya fortress
				name = 'Lillya',
				sprite_pos = (8,6),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'None',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'attack_monster' : 70,
							'protectionspell' : 1},
				properties = ['npc','villager'],
				memory = 'first_meet = True',
				message = 'lillya_fortress')
		techID+=1
		self.mlist['rescued'].append(m)
		#3
		m=monster(techID = techID,#lillya elysium
				name = 'Lillya',
				sprite_pos = (8,6),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'None',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'attack_monster' : 70,
							'protectionspell' : 1},
				properties = ['npc','villager','no_dismiss'],
				memory = 'first_meet = True',
				message = 'lillya_elysium')
		techID+=1
		self.mlist['rescued'].append(m)
		#4
		m=monster(techID = techID,#Karula
				name = 'Karula',
				sprite_pos = (9,12),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'None',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'attack_monster' : 70,
							'protectionspell' : 1},
				properties = ['npc','villager','no_dismiss'],
				memory = 'first_meet = True',
				message = 'karula')
		techID+=1
		self.mlist['rescued'].append(m)
		#5
		m=monster(techID = techID,#Narasa
				name = 'Narasa',
				sprite_pos = (10,12),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'None',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'attack_monster' : 70,
							'protectionspell' : 1},
				properties = ['npc','villager','no_dismiss'],
				memory = 'first_meet = True',
				message = 'narasa')
		techID+=1
		self.mlist['rescued'].append(m)
		
		#cages
		
		self.mlist['cage'] = []
		#0:cage
		m=monster(techID = techID,
				name = 'cage',
				sprite_pos = (5,6),
				move_border = 10,
				attribute_prev = (0,0,0,0,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'flee',
				corps_style = 'cage',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['cattle'],
				message = 'None')
		techID+=1
		self.mlist['cage'].append(m)
		
		
		#statue
		
		self.mlist['statue'] = []
		#0
		m=monster(techID = techID,
				name = 'wood elf statue',
				sprite_pos = (8,8),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'statue',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'bump_mes="Looks like a statue of a wood elf."; vil_num=0',
				message = 'statue')
		techID+=1
		self.mlist['statue'].append(m)
		#1
		m=monster(techID = techID,
				name = 'dwarf statue',
				sprite_pos = (8,9),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'statue',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'bump_mes="Looks like a statue of a dwarf."; vil_num=1',
				message = 'statue')
		techID+=1
		self.mlist['statue'].append(m)
		#2
		m=monster(techID = techID,
				name = 'blob statue',
				sprite_pos = (9,8),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'statue',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'bump_mes="Looks like a statue of a blob."; vil_num=2',
				message = 'statue')
		techID+=1
		self.mlist['statue'].append(m)
		#3
		m=monster(techID = techID,
				name = 'dryad statue',
				sprite_pos = (9,9),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'statue',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'bump_mes="Looks like a statue of a dryad."; vil_num=3',
				message = 'statue')
		techID+=1
		self.mlist['statue'].append(m)
		#4
		m=monster(techID = techID,
				name = 'cylira statue',
				sprite_pos = (9,7),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'statue',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'bump_mes="Looks like a statue of Cylira the elfish leader."; vil_num=4',
				message = 'statue')
		techID+=1
		self.mlist['statue'].append(m)
		
		#villager
		
		self.mlist['villager'] = []
		#0
		m=monster(techID = techID,
				name = 'wood elf',
				sprite_pos = (0,0),
				move_border = 0,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('house','house'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				properties = ['npc','villager'],
				message = 'wood_elf')
		techID+=1
		self.mlist['villager'].append(m)
		#1
		m=monster(techID = techID,
				name = 'dwarf',
				sprite_pos = (5,7),
				move_border = 0,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				properties = ['npc','villager'],
				message = 'dwarf')
		techID+=1
		self.mlist['villager'].append(m)
		#2
		m=monster(techID = techID,
				name = 'animated blob',
				sprite_pos = (6,8),
				move_border = 4,
				attribute_prev = (2,2,0,1,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'life_essence',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				properties = ['corrosive'],
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'None')
		techID+=1
		self.mlist['villager'].append(m)
		#3
		m=monster(techID = techID,
				name = 'dryad',
				sprite_pos = (5,8),
				move_border = 0,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('house','house'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				properties = ['npc','villager'],
				message = 'dryad')
		techID+=1
		self.mlist['villager'].append(m)
		#4
		m=monster(techID = techID,
				name = 'Cylira',
				sprite_pos = (8,7),
				move_border = 0,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				properties = ['npc','villager'],
				quest_state = True,
				memory = 'first_meet = True; name = "Cylira"',
				message = 'cylira')
		techID+=1
		self.mlist['villager'].append(m)
		
		#cave

		self.mlist['cave'] = []
		#0
		m=monster(techID = techID,
				name = 'cave orc',
				sprite_pos = (2,3),
				move_border = 2,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'hostile',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'def_potion' : 35,
							'range_throw' : (20,0)},
				num_special = 3,
				message = 'None.')
		techID+=1
		self.mlist['cave'].append(m)
		#1
		m=monster(techID = techID,
				name = 'blue blob',
				sprite_pos = (2,5),
				move_border = 4,
				attribute_prev = (2,1,0,0,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'jelly',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				properties = ['corrosive'],
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'None.')
		techID+=1
		self.mlist['cave'].append(m)
		#2
		m=monster(techID = techID,
				name = 'bat',
				sprite_pos = (2,7),
				move_border = 0,
				attribute_prev = (0,2,0,2,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'flee',
				corps_style = 'animal',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','swim','dry_entrance','wet_entance','jump'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['cattle','hover'],
				message = 'None.')
		techID+=1
		self.mlist['cave'].append(m)
		#3
		m=monster(techID = techID,
				name = 'soil spirit',
				sprite_pos = (1,0),
				move_border = 2,
				attribute_prev = (0,1,2,1,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'hostile',
				corps_style = 'troll',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'range_shoot' : 65}, 
				message = 'None.')
		techID+=1
		self.mlist['cave'].append(m)
		#4
		m=monster(techID = techID,
				name = 'goblin',
				sprite_pos = (3,7),
				move_border = 1,
				attribute_prev = (2,2,0,3,2),
				worn_equipment = (0,0,1,0,0),
				AI_style = 'hostile',
				corps_style = 'thief',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'close_steal' : 70,
							'def_flee' : 80,
							'range_throw' : (30,0)},
				num_special = 5,
				message = 'None.')
		techID+=1
		self.mlist['cave'].append(m)

		#mine

		self.mlist['orcish_mines'] = []
		#0
		m=monster(techID = techID,
				name = 'orc warlord',
				sprite_pos = (1,5),
				move_border = 2,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,1),
				AI_style = 'hostile',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'bleeding', 
				effect_duration = 5, 
				effect_probability = 20,
				ability = {'def_potion' : 50,
							'range_throw' : (15,2)},
				num_special = 4, 
				message = 'The orc warlord tears you a bleeding wound.')
		techID+=1
		self.mlist['orcish_mines'].append(m)
		#1
		m=monster(techID = techID,
				name = 'orcish hag',
				sprite_pos = (0,6),
				move_border = 2,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'scrollkeeper',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = 'hexed', 
				effect_duration = 40, 
				effect_probability = 20,
				ability = {'range_shoot' : 99,
							'def_teleport' : 20},
				message = 'The orcish hag puts a hex on you.',
				light_emit = 2)
		techID+=1
		self.mlist['orcish_mines'].append(m)
		#2
		m=monster(techID = techID,
				name = 'orcish digger',
				sprite_pos = (1,6),
				move_border = 2,
				attribute_prev = (1,2,0,1,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'miner',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				ability = {'def_flee' : 30},
				message = 'None')
		techID+=1
		self.mlist['orcish_mines'].append(m)
		#3
		m=monster(techID = techID,
				name = 'blood snake',
				sprite_pos = (2,6),
				move_border = 2,
				attribute_prev = (2,2,0,0,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = 'poisoned', 
				effect_duration = 60,
				effect_probability = 45,
				ability = {'def_flee' : 30},
				message = 'Poison runs trough your veins!')
		techID+=1
		self.mlist['orcish_mines'].append(m)
		
		#elfish fortress

		self.mlist['elfish_fortress'] = []
		
		m=monster(techID = techID,
				name = 'male elf',
				sprite_pos = (2,0),
				move_border = 0,
				attribute_prev = (2,2,0,0,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','house'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'elf',
				anger = 'kill',
				anger_monster = 0)
		techID+=1
		self.mlist['elfish_fortress'].append(m)

		m=monster(techID = techID,
				name = 'female elf',
				sprite_pos = (2,1),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','house'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'elf',
				anger = 'kill',
				anger_monster = 1)
		techID+=1
		self.mlist['elfish_fortress'].append(m)
		
		#elfish special
		
		self.mlist['elfish_special'] = []
		#0
		m=monster(techID = techID,
				name = 'elfish landlord',
				sprite_pos = (6,9),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('house','house'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'landlord',
				anger = 'kill',
				anger_monster = 0)
		techID+=1
		self.mlist['elfish_special'].append(m)
		#1
		m=monster(techID = techID,
				name = 'elfish drunkard',
				sprite_pos = (7,0),
				move_border = 4,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('house','house'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'drunkard',
				anger = 'kill',
				anger_monster = 0)
		techID+=1
		self.mlist['elfish_special'].append(m)
		#2
		m=monster(techID = techID,
				name = 'elfish farmer',
				sprite_pos = (6,10),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('dry_entrance','dry_entrance'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'farmer',
				anger = 'kill',
				anger_monster = 0)
		techID+=1
		self.mlist['elfish_special'].append(m)
		#3
		m=monster(techID = techID,
				name = 'elfish priestess',
				sprite_pos = (7,1),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('house','house'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'priestess',
				anger = 'kill',
				anger_monster = 0)
		techID+=1
		self.mlist['elfish_special'].append(m)
		#4
		m=monster(techID = techID,
				name = 'elfish rowdy (male)',
				sprite_pos = (10,9),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'rowdy',
				anger = 'kill',
				anger_monster = 0)
		techID+=1
		self.mlist['elfish_special'].append(m)
		#5
		m=monster(techID = techID,
				name = 'elfish rowdy (female)',
				sprite_pos = (10,10),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'rowdy',
				anger = 'kill',
				anger_monster = 0)
		techID+=1
		self.mlist['elfish_special'].append(m)
		#6
		m=monster(techID = techID,
				name = 'orcish guard (male)',
				sprite_pos = (10,6),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'guard',
				light_emit = 4,
				anger = 'kill',
				anger_monster = 0)
		techID+=1
		self.mlist['elfish_special'].append(m)
		#7
		m=monster(techID = techID,
				name = 'orcish guard (female)',
				sprite_pos = (10,6),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'guard',
				light_emit = 4,
				anger = 'kill',
				anger_monster = 0)
		techID+=1
		self.mlist['elfish_special'].append(m)
		
		#angry_monsters
		
		self.mlist['angry_monster'] = []
		#0		
		m=monster(techID = techID,
				name = 'male elf',
				sprite_pos = (2,0),
				move_border = 0,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'hostile',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'None')
		techID+=1
		self.mlist['angry_monster'].append(m)
		#1
		m=monster(techID = techID,
				name = 'female elf',
				sprite_pos = (2,1),
				move_border = 0,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'range_shoot' : 70},
				message = 'None')
		techID+=1
		self.mlist['angry_monster'].append(m)
		#2
		m=monster(techID = techID,
				name = 'angry dryad',
				sprite_pos = (2,8),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'hostile',
				corps_style = 'dryade',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				ability = {'def_teleport' : 10},
				message = 'None')
		techID+=1
		self.mlist['angry_monster'].append(m)
		#3
		m=monster(techID = techID,
				name = 'male neko',
				sprite_pos = (2,10),
				move_border = 0,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'hostile',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','house'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'None')
		techID+=1
		self.mlist['angry_monster'].append(m)
		#4
		m=monster(techID = techID,
				name = 'female neko',
				sprite_pos = (2,9),
				move_border = 0,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'human',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','house'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'None')
		techID+=1
		self.mlist['angry_monster'].append(m)
		
		#grot

		self.mlist['grot'] = []
		
		m=monster(techID = techID,
				name = 'blue naga',
				sprite_pos = (2,4),
				move_border = 3,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'scrollkeeper',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_magic', 
				attack_were = ('Head'),
				possible_effect = 'hexed', 
				effect_duration = 30,
				effect_probability = 50,
				ability = {'range_shoot' : 50,
							'def_potion' : 20},
				message = 'The blue naga puts a hex on you.',
				light_emit = 2)
		techID+=1
		self.mlist['grot'].append(m)

		m=monster(techID = techID,
				name = 'red naga',
				sprite_pos = (1,2),
				move_border = 3,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,0,1,1),
				AI_style = 'hostile',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = 'poisoned', 
				effect_duration = 50,
				effect_probability = 40,
				ability = {'def_potion' : 20,
							'range_throw' : (25,2)},
				num_special = 4,
				message = 'Poison runs trough your veins!')
		techID+=1
		self.mlist['grot'].append(m)

		m=monster(techID = techID,
				name = 'purple blob',
				sprite_pos = (0,5),
				move_border = 4,
				attribute_prev = (2,2,0,0,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'jelly',
				corps_lvl = 4,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				properties = ['corrosive'],
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None')
		techID+=1
		self.mlist['grot'].append(m)

		m=monster(techID = techID,
				name = 'water spirit',
				sprite_pos = (0,1),
				move_border = 2,
				attribute_prev = (2,2,0,1,1),
				worn_equipment = (1,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 3,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = 'confused', 
				effect_duration = 60,
				effect_probability = 20,
				message = 'The water sprit confuses you.')
		techID+=1
		self.mlist['grot'].append(m)
		
		#lava cave

		self.mlist['lava_cave'] = []
		#0		
		m=monster(techID = techID,
				name = 'lava monster',
				sprite_pos = (0,8),
				move_border = 2,
				attribute_prev = (2,2,0,2,2),
				worn_equipment = (1,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'attack_melee', 
				attack_were = ('Body','Legs','Feet'),
				properties = ['flaming'],
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None',
				light_emit = 3)
		techID+=1
		self.mlist['lava_cave'].append(m)
		#1
		m=monster(techID = techID,
				name = 'flame spirit',
				sprite_pos = (0,10),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'attack_magic', 
				attack_were = ('Body','Legs','Feet'),
				properties = ['flaming'],
				possible_effect = 'immobilized', 
				effect_duration = 4,
				effect_probability = 30,
				ability = {'range_shoot' : 30,
							'close_flames' : 60},
				message = 'The flame spirit holds you!',
				light_emit = 4)
		techID+=1
		self.mlist['lava_cave'].append(m)
		#2
		m=monster(techID = techID,
				name = 'red blob',
				sprite_pos = (1,3),
				move_border = 4,
				attribute_prev = (2,2,0,0,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'jelly',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				properties = ['corrosive'],
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None',
				light_emit = 2)
		techID+=1
		self.mlist['lava_cave'].append(m)
		#3
		m=monster(techID = techID,
				name = 'fire bat',
				sprite_pos = (1,10),
				move_border = 0,
				attribute_prev = (2,2,0,0,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body'),
				properties = ['flaming'],
				possible_effect = 'confused', 
				effect_duration = 40,
				effect_probability = 30,
				message = 'The bat\'s screen make you confused!',
				light_emit = 3)
		techID+=1
		self.mlist['lava_cave'].append(m)
		
		#special

		self.mlist['special'] = []
		#0
		m=monster(techID = techID,
				name = 'vase',
				sprite_pos = (0,3),
				move_border = 10,
				attribute_prev = (2,0,2,0,0),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'flee',
				corps_style = 'miner',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#1
		m=monster(techID = techID,
				name = 'monster vase',
				sprite_pos = (0,3),
				move_border = 10,
				attribute_prev = (2,0,2,0,0),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'flee',
				corps_style = 'vase',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#2
		m=monster(techID = techID,
				name = 'vase monster',
				sprite_pos = (0,2),
				move_border = 1,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'miner',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance'), 
				behavior = 'attack_random', 
				attack_were = ('Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#3
		m=monster(techID = techID,
				name = 'sleeping mimic',
				sprite_pos = (1,1),
				move_border = 10,
				attribute_prev = (2,0,2,0,0),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'flee',
				corps_style = 'mimic',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#4
		m=monster(techID = techID,
				name = 'mimic',
				sprite_pos = (0,7),
				move_border = 4,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 99,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance'), 
				behavior = 'attack_random', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#5
		m=monster(techID = techID,
				name = 'female neko',
				sprite_pos = (2,9),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('house','house'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'neko',
				memory = 'name = "Female Neko"',
				anger = 'destroy',
				anger_monster = 4)
		techID+=1
		self.mlist['special'].append(m)
		#6
		m=monster(techID = techID,
				name = 'male neko',
				sprite_pos = (2,10),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('house','house'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'neko',
				memory = 'name = "Male Neko"',
				anger = 'destroy',
				anger_monster = 3)
		techID+=1
		self.mlist['special'].append(m)
		#7
		m=monster(techID = techID,
				name = 'skeleton',
				sprite_pos = (4,0),
				move_border = 1,
				attribute_prev = (2,2,1,1,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'reset_parent',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#8
		m=monster(techID = techID,
				name = 'shadow',
				sprite_pos = (4,1),
				move_border = 1,
				attribute_prev = (1,1,2,2,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'reset_parent',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#9
		m=monster(techID = techID,
				name = 'ghost',
				sprite_pos = (5,0),
				move_border = 1,
				attribute_prev = (0,3,2,1,2),
				worn_equipment = (0,0,1,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','swim','solid','dry_entrance','wet_entrance','jump'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs','Feet'),
				properties = ['hover'],
				possible_effect = 'hexed', 
				effect_duration = 25,
				effect_probability = 30,
				ability = {'range_shoot' : 90,
							'blink' : 20},
				num_special = 10,
				message = 'None',
				light_emit = 2)
		techID+=1
		self.mlist['special'].append(m)
		#10
		m=monster(techID = techID,
				name = 'giant blob',
				sprite_pos = (5,1),
				move_border = 4,
				attribute_prev = (2,2,0,1,3),
				worn_equipment = (1,1,0,1,0),
				AI_style = 'hostile',
				corps_style = 'make_blobs',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				properties = ['corrosive'],
				possible_effect = 'poisoned', 
				effect_duration = 180, 
				effect_probability = 50, 
				message = 'You got poisoned by a giant blob.')
		techID+=1
		self.mlist['special'].append(m)
		#11
		m=monster(techID = techID,
				name = 'vampire bat',
				sprite_pos = (4,4),
				move_border = 4,
				attribute_prev = (2,2,2,2,3),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'hostile',
				corps_style = 'animal',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance','swim'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body'),
				possible_effect = 'bleeding', 
				effect_duration = 10, 
				effect_probability = 40,
				ability = {'close_vampirism' : 50},
				message = 'A vampire bat tears a bleeding wound.')
		techID+=1
		self.mlist['special'].append(m)
		#12
		m=monster(techID = techID,
				name = 'crow',
				sprite_pos = (5,10),
				move_border = 1,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'reset_parent',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance','swim'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				ability = {'close_stealItem' : 75},
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#13
		m=monster(techID = techID,
				name = 'drowner',
				sprite_pos = (10,2),
				move_border = 3,
				attribute_prev = (2,2,0,1,3),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('low_liquid','wet_entrance','swim'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet','Body','Head'),
				properties = ['invisible'],
				possible_effect = None,
				effect_duration = 0, 
				effect_probability = 0,
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#14
		m=monster(techID = techID,
				name = 'demonic chest',
				sprite_pos = (5,9),
				move_border = 1,
				attribute_prev = (0,2,3,2,4),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'hostile',
				corps_style = 'kill_childs',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body'),
				possible_effect = 'hexed', 
				effect_duration = 30, 
				effect_probability = 40,
				ability = {'range_shoot' : 50,
							'spawn' : (40,12)},
				num_special = 2,
				message = 'The demonic chest puts a hex on you.',
				light_emit = 2)
		techID+=1
		self.mlist['special'].append(m)
		#15
		m=monster(techID = techID,
				name = 'green blob',
				sprite_pos = (1,8),
				move_border = 4,
				attribute_prev = (2,2,0,1,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'jelly',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				properties = ['corrosive'],
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'None')
		techID+=1
		self.mlist['special'].append(m)
		#16
		m=monster(techID = techID,
				name = 'albino rat',
				sprite_pos = (10,5),
				move_border = 1,
				attribute_prev = (3,1,0,0,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'hostile',
				corps_style = 'toolbox',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = 'sick', 
				effect_duration = 4319, 
				effect_probability = 20, 
				message = 'You feel very sick!')
		techID+=1
		self.mlist['special'].append(m)
		#17
		m=monster(techID = techID,
				name = 'animated statue',
				sprite_pos = (8,4),
				move_border = 2,
				attribute_prev = (2,2,0,1,3),
				worn_equipment = (1,0,1,0,1),
				AI_style = 'hostile',
				corps_style = 'life_essence',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'bleeding', 
				effect_duration = 5, 
				effect_probability = 30,
				ability = {'range_throw' : (30,0)},
				properties = ['uneatable','unflamable'],
				num_special = 4, 
				message = 'The animated statue tears you a bleeding wound.')
		techID+=1
		self.mlist['special'].append(m)
		#18
		m=monster(techID = techID,
				name = 'escaped wood elf',
				sprite_pos = (0,0),
				move_border = 2,
				attribute_prev = (0,2,0,2,1),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				quest_state = True,
				properties = ['npc','villager'],
				memory = 'first_meet = True; name = "Wood Elf"',
				message = 'escaped_elf')
		techID+=1
		self.mlist['special'].append(m)
		#19
		m=monster(techID = techID,
				name = 'elfish worker', #version before the quest
				sprite_pos = (10,4),
				move_border = 2,
				attribute_prev = (0,2,0,2,1),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				quest_state = True,
				properties = ['npc','villager'],
				memory = 'first_meet = True; name = "Elfish Worker"',
				message = 'worker1')
		techID+=1
		self.mlist['special'].append(m)
		#20
		m=monster(techID = techID,
				name = 'elfish worker', #version after the quest
				sprite_pos = (10,4),
				move_border = 2,
				attribute_prev = (0,2,0,2,1),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				quest_state = 'None',
				properties = ['npc','villager'],
				memory = 'first_meet = True; name = "Elfish Worker"',
				message = 'worker2')
		techID+=1
		self.mlist['special'].append(m)
		#21
		m=monster(techID = techID,
				name = 'banished orc',
				sprite_pos = (0,4),
				move_border = 2,
				attribute_prev = (2,1,0,1,1),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				quest_state = True,
				properties = ['npc','villager'],
				memory = 'quest_status = 0;',
				message = 'banished_orc')
		techID+=1
		self.mlist['special'].append(m)
		#22
		m=monster(techID = techID,
				name = 'rescued orc',
				sprite_pos = (0,4),
				move_border = 2,
				attribute_prev = (2,1,0,1,1),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','house'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				quest_state = True,
				properties = ['npc','villager'],
				memory = 'None',
				message = 'fortress_orc')
		techID+=1
		self.mlist['special'].append(m)
		#23
		m=monster(techID = techID,
				name = 'runed door',
				sprite_pos = (1,11),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'name = "Runed Door";',
				message = 'runed_door',
				light_emit = 2)
		techID+=1
		self.mlist['special'].append(m)
		#24
		m=monster(techID = techID,
				name = 'dwarfen king',
				sprite_pos = (2,11),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				properties = ['npc','villager'],
				memory = 'name = "Dwarfen King";',
				message = 'dwarfen_king')
		techID+=1
		self.mlist['special'].append(m)
		#25
		m=monster(techID = techID,
				name = 'wizard',
				sprite_pos = (0,11),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'wizard0',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_magic',
				attack_were = ('Head','Body'),
				possible_effect = 'hexed', 
				effect_duration = 30, 
				effect_probability = 30,
				ability = {'spawn' : (50,17),
							'range_shoot' : 50},
				properties = ['unflamable','uneatable'],
				message = 'The wizard murmurs strange words.',
				num_special = 4)
		techID+=1
		self.mlist['special'].append(m)
		#26
		m=monster(techID = techID,
				name = 'hammer dryad',
				sprite_pos = (11,0),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'dryade',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				quest_state = True,
				properties = ['npc','villager'],
				memory = "name='Dryad'; first_meet = True;", 
				message = 'dryad_hammer')
		techID+=1
		self.mlist['special'].append(m)
		#27
		m=monster(techID = techID,
				name = 'Old Neko',
				sprite_pos = (9,5),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				quest_state = True,
				properties = ['npc','villager'],
				memory = "name='Old Neko'; first_meet = True; shop_style = 'old_neko'", 
				message = 'old_neko')
		techID+=1
		self.mlist['special'].append(m)
		#28
		m=monster(techID = techID,
				name = 'Neko Mummy',
				sprite_pos = (9,6),
				move_border = 3,
				attribute_prev = (0,1,3,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'hostile',
				corps_style = 'neko_amulet',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body'),
				possible_effect = 'hexed', 
				effect_duration = 45, 
				effect_probability = 50, 
				ability = {'range_throw' : (20,2),
							'range_shoot' : 50},
				message = 'The neko mummy puts a hex on you!')
		techID+=1
		self.mlist['special'].append(m)
		#29
		m=monster(techID = techID,
				name = 'chicklet',
				sprite_pos = (11,2),
				move_border = 0,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,0,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','house'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				memory = "None", 
				message = 'swap_place')
		techID+=1
		self.mlist['special'].append(m)
		#30
		m=monster(techID = techID,
				name = 'angry wizard',
				sprite_pos = (3,11),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'hostile',
				corps_style = 'wizard1',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_magic',
				attack_were = ('Head','Body'),
				possible_effect = 'hexed', 
				effect_duration = 30, 
				effect_probability = 30,
				ability = {'blink' : (50),
							'range_shoot' : 80,
							'close_flames' : 70},
				properties = ['unflamable','uneatable','flaming'],
				message = 'The wizard murmurs strange words.',
				num_special = 4)
		techID+=1
		self.mlist['special'].append(m)
		#31
		m=monster(techID = techID,
				name = ' female golden naga',
				sprite_pos = (1,9),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['invisible','perma-invisible','npc','villager'],
				memory = 'name = "Female Golden Naga";',
				message = 'naga')
		techID+=1
		self.mlist['special'].append(m)
		#32
		m=monster(techID = techID,
				name = ' male golden naga',
				sprite_pos = (4,11),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['invisible','perma-invisible','npc','villager'],
				memory = 'name = "Male Golden Naga";',
				message = 'naga')
		techID+=1
		self.mlist['special'].append(m)
		#33
		m=monster(techID = techID,
				name = ' escaped naga',
				sprite_pos = (1,9),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['npc','villager'],
				quest_state = True,
				memory = 'name = "Female Golden Naga";',
				message = 'escaped_naga')
		techID+=1
		self.mlist['special'].append(m)
		#34
		m=monster(techID = techID,
				name = 'dwarfen scout',
				sprite_pos = (5,7),
				move_border = 0,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				properties = ['npc','villager'],
				memory = 'first_meet = True;',
				quest_state = True,
				message = 'dwarfen_scout')
		techID+=1
		self.mlist['special'].append(m)
		#35 
		m=monster(techID = techID, #after solving 'Going down!'
				name = 'dwarfen scout',
				sprite_pos = (5,7),
				move_border = 0,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'ignore',
				corps_style = 'human',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'talk', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				properties = ['npc','villager'],
				message = 'happy_dwarfen_scout')
		techID+=1
		self.mlist['special'].append(m)
		#36
		m=monster(techID = techID,
				name = 'boulder',
				sprite_pos = (7,11),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'name = "boulder";',
				message = 'boulder')
		techID+=1
		self.mlist['special'].append(m)
		#37
		m=monster(techID = techID,
				name = 'iron door',
				sprite_pos = (8,11),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'name = "door"; sound = "iron_door"',
				message = 'simple_door')
		techID+=1
		self.mlist['special'].append(m)
		#38
		m=monster(techID = techID,
				name = 'lesser imp',
				sprite_pos = (12,4),
				move_border = 1,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'munanoid',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_random', 
				attack_were = ('Body','Legs','Feet'),
				possible_effect = 'hexed', 
				effect_duration = 20,
				effect_probability = 10,
				message = 'A lesser imp puts a hex on you.')
		techID+=1
		self.mlist['special'].append(m)
		#39
		m=monster(techID = techID,
				name = 'locked iron door',
				sprite_pos = (5,11),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'name = "locked door"; sound = "iron_door"',
				message = 'locked_door')
		techID+=1
		self.mlist['special'].append(m)
		#40
		m=monster(techID = techID,
				name = 'abyssal knight',
				sprite_pos = (8,12),
				move_border = 2,
				attribute_prev = (2,1,2,1,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'hostile',
				corps_style = 'explode',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_magic',
				attack_were = ('Head','Body'),
				possible_effect = 'bleeding', 
				effect_duration = 15, 
				effect_probability = 30,
				ability = {'range_shoot' : 80,
							'close_flames' : 70},
				properties = ['unflamable','uneatable','flaming'],
				message = 'The abyssal knight tears you a bleeding wound.',
				num_special = 4,
				light_emit = 3)
		techID+=1
		self.mlist['special'].append(m)
		#41
		m=monster(techID = techID,
				name = 'locked iron door (final)',
				sprite_pos = (6,11),
				move_border = 10,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				num_special = 3,
				memory = 'name = "locked door"; sound = "iron_door"',
				message = 'final_locked_door')
		techID+=1
		self.mlist['special'].append(m)
		
		#desert_cave
		self.mlist['desert_cave'] = []
		#0
		m=monster(techID = techID,
				name = 'ghoul',
				sprite_pos = (4,10),
				move_border = 3,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_magic', 
				attack_were = ('Body','Legs','Feet'),
				possible_effect = 'confused', 
				effect_duration = 30,
				effect_probability = 120,
				ability = {'range_shoot' : 50},
				message = 'The ghoul confuses you!')
		techID+=1
		self.mlist['desert_cave'].append(m)
		#1
		m=monster(techID = techID,
				name = 'goblin maniac',
				sprite_pos = (4,9),
				move_border = 3,
				attribute_prev = (2,2,0,0,2),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'hostile',
				corps_style = 'human',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				ability = {'close_throwExplosive' : 70},
				num_special = 10,
				message = 'None')
		techID+=1
		self.mlist['desert_cave'].append(m)
		
		self.mlist['desert_cave'].append(self.mlist['special'][11])
		self.mlist['desert_cave'].append(self.mlist['lava_cave'][2])
		
		#shop
		
		self.mlist['shop'] = []
		#0
		m=monster(techID = techID,
				name = 'elfish merchant',
				sprite_pos = (3,2),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Elvish Merchant"; shop_style = "fortress_elf"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#1
		m=monster(techID = techID,
				name = 'orcish merchant',
				sprite_pos = (3,0),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Orcish Merchant"; shop_style = "orc"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#2
		m=monster(techID = techID,
				name = 'naga merchant',
				sprite_pos = (3,1),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Naga Merchant"; shop_style = "naga"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#3
		m=monster(techID = techID,
				name = 'Naeria',
				sprite_pos = (6,1),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Naeria"; shop_style = "pharmacy"',
				message ='shop')
		techID+=1
		self.mlist['shop'].append(m)
		#4
		m=monster(techID = techID,
				name = 'Eklor',
				sprite_pos = (6,2),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Eklor"; shop_style = "pickaxe"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#5
		m=monster(techID = techID,
				name = 'Grimork',
				sprite_pos = (6,3),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Grimork"; shop_style = "hardware"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#6
		m=monster(techID = techID,
				name = 'Larosas',
				sprite_pos = (6,4),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Larosas"; shop_style = "deco"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#7
		m=monster(techID = techID,
				name = 'Xirazzzia',
				sprite_pos = (3,1),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Xirazzzia"; shop_style = "book"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#8
		m=monster(techID = techID,
				name = 'Torguly',
				sprite_pos = (6,5),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Torguly"; shop_style = "bomb"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#9
		m=monster(techID = techID,
				name = 'Arialus',
				sprite_pos = (3,2),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Arialus"; shop_style = "general"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#10
		m=monster(techID = techID,
				name = 'Dwarfen Merchant',
				sprite_pos = (6,2),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('shop','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Dwarfen Merchant"; shop_style = "dwarf"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#11
		m=monster(techID = techID,
				name = 'Pharmacist',
				sprite_pos = (6,1),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Pharmacist"; shop_style = "pharmacy"',
				message ='shop')
		techID+=1
		self.mlist['shop'].append(m)
		#12
		m=monster(techID = techID,
				name = 'Thinkerer',
				sprite_pos = (6,2),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Thinkerer"; shop_style = "thinkerer"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#13
		m=monster(techID = techID,
				name = 'Artefact Seller',
				sprite_pos = (6,3),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Artefact Seller"; shop_style = "artefact"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#14
		m=monster(techID = techID,
				name = 'Farmer',
				sprite_pos = (9,5),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Farmer"; shop_style = "farmer"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		#15
		m=monster(techID = techID,
				name = 'Explosive Seller',
				sprite_pos = (6,5),
				move_border = 0,
				attribute_prev = (2,2,2,2,2),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','shop'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0,
				effect_probability = 0,
				properties = ['npc','villager'],
				memory = 'name = "Explosive Seller"; shop_style = "bomb"',
				message = 'shop')
		techID+=1
		self.mlist['shop'].append(m)
		
		#mechants
		self.mlist['merchant'] = []
		#0
		self.mlist['merchant'].append(deepcopy(self.mlist['shop'][0]))
		self.mlist['merchant'][0].techID = techID
		techID += 1
		#1
		self.mlist['merchant'].append(deepcopy(self.mlist['shop'][1]))
		self.mlist['merchant'][1].techID = techID
		techID += 1
		#2
		self.mlist['merchant'].append(deepcopy(self.mlist['shop'][2]))
		self.mlist['merchant'][2].techID = techID
		techID += 1
		#3
		self.mlist['merchant'].append(deepcopy(self.mlist['shop'][10]))
		self.mlist['merchant'][3].techID = techID
		techID += 1
		
		#desert monster
		self.mlist['desert'] = []
		#0
		m=monster(techID = techID,
				name = 'desert snake',
				sprite_pos = (3,3),
				move_border = 4,
				attribute_prev = (3,1,0,0,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'animal',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = 'poisoned', 
				effect_duration = 80, 
				effect_probability = 80,
				ability ={'def_flee' : 40},
				message = 'Poison runs trough your veins!')
		techID+=1
		self.mlist['desert'].append(m)
		#1
		m=monster(techID = techID,
				name = 'yellow blob',
				sprite_pos = (3,4),
				move_border = 4,
				attribute_prev = (2,2,0,1,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'jelly',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','soil'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				properties = ['corrosive'],
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'None')
		techID+=1
		self.mlist['desert'].append(m)
		#2
		m=monster(techID = techID,
				name = 'scarab',
				sprite_pos = (3,5),
				move_border = 2,
				attribute_prev = (2,2,0,2,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0, 
				message = 'None')
		techID+=1
		self.mlist['desert'].append(m)
		#3
		m=monster(techID = techID,
				name = 'lizard',
				sprite_pos = (3,6),
				move_border = 0,
				attribute_prev = (0,1,0,1,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'flee',
				corps_style = 'animal',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['cattle'],
				message = 'None')
		techID+=1
		self.mlist['desert'].append(m)
		
		#kobold
		self.mlist['kobold'] = []
		#0
		m=monster(techID = techID,
				name = 'kobold shaman',
				sprite_pos = (5,4),
				move_border = 2,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'kobold_fear',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_magic', 
				attack_were = ('Body','Legs','Feet'),
				possible_effect = 'hexed', 
				effect_duration = 30, 
				effect_probability = 30,
				ability = {'range_shoot' : 80},
				message = 'The kobold shaman puts a hex on you.',
				light_emit = 2)
		techID+=1
		self.mlist['kobold'].append(m)
		#1
		m=monster(techID = techID,
				name = 'kobold',
				sprite_pos = (5,5),
				move_border = 2,
				attribute_prev = (2,1,0,1,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'hostile',
				corps_style = 'thief',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Body','Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				ability = {'close_steal' : 35,
							'range_throw' : (10,1)},
				num_special = 5,
				message = 'None.')
		techID+=1
		self.mlist['kobold'].append(m)
		
		#dungeon monster
		self.mlist['dungeon'] = []
		for count in range(0,2):
		#0
			m=monster(techID = techID,
				name = 'floating eye',
				sprite_pos = (3,8),
				move_border = 4,
				attribute_prev = (1,0,1,0,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 10,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','swim','dry_entrance','wet_entance'),
				behavior = 'attack_melee',
				attack_were = ('Head','Head'),
				possible_effect = 'immobilized', 
				effect_duration = 10, 
				effect_probability = 30, 
				message = 'The floating eye stares at you.')
			techID+=1
			self.mlist['dungeon'].append(m)
			#1
			m=monster(techID = techID,
				name = 'nymph',
				sprite_pos = (3,10),
				move_border = 0,
				attribute_prev = (0,1,2,1,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'thief',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_magic',
				attack_were = ('Head','Body'),
				possible_effect = 'instable', 
				effect_duration = 60, 
				effect_probability = 20, 
				ability = {'def_teleport' : 80,
							'close_steal' : 60},
				message = 'Suddenly you feel very instable!',)
			techID+=1
			self.mlist['dungeon'].append(m)
		#4
		m=monster(techID = techID,
				name = 'wisp',
				sprite_pos = (4,2),
				move_border = 5,
				attribute_prev = (0,2,2,2,0),
				worn_equipment = (0,0,0,1,1),
				AI_style = 'hostile',
				corps_style = 'kill_childs',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','swim','dry_entrance','wet_entance'), 
				behavior = 'attack_melee',
				attack_were = ('Head','Body'),
				possible_effect = 'blind', 
				effect_duration = 30, 
				effect_probability = 30,
				ability = {'spawn' : (40,8)},
				message = 'The wisp glows.',
				num_special = 3,
				light_emit = 3,)
		techID+=1
		self.mlist['dungeon'].append(m)
		
		for count in range(0,3):
			self.mlist['dungeon'].append(self.mlist['cave'][0])#cave orc
			self.mlist['dungeon'].append(self.mlist['cave'][2])#bat
			self.mlist['dungeon'].append(self.mlist['cave'][4])#goblin
			self.mlist['dungeon'].append(self.mlist['orcish_mines'][0])#orc warlord
			self.mlist['dungeon'].append(self.mlist['orcish_mines'][1])#orcish hag
			
		#tomb monster
		self.mlist['tomb'] = []
		for count in range(0,2):
		#0
			m=monster(techID = techID,
				name = 'mummy',
				sprite_pos = (3,9),
				move_border = 3,
				attribute_prev = (2,2,0,1,1),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'hostile',
				corps_style = 'mummy',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'),
				behavior = 'attack_melee',
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'hexed', 
				effect_duration = 30, 
				effect_probability = 15, 
				message = 'The mummy puts a curse on you.')
			techID+=1
			self.mlist['tomb'].append(m)
		#2
		m=monster(techID = techID,
				name = 'necromancer',
				sprite_pos = (4,3),
				move_border = 2,
				attribute_prev = (0,1,2,2,1),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'kill_childs',
				corps_lvl = 0,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_magic',
				attack_were = ('Head','Body'),
				possible_effect = 'hexed', 
				effect_duration = 30, 
				effect_probability = 30,
				ability = {'spawn' : (40,7)},
				message = 'Necromancer murmurs strange words.',
				num_special = 3,
				light_emit = 2)
		techID+=1
		self.mlist['tomb'].append(m)
		
		for count in range(0,3):
			self.mlist['tomb'].append(self.mlist['desert'][0])#desert snake
			self.mlist['tomb'].append(self.mlist['desert'][2])#scarab
			self.mlist['tomb'].append(self.mlist['desert'][3])#lizard
			self.mlist['tomb'].append(self.mlist['cave'][2])#orc warlord
			self.mlist['tomb'].append(self.mlist['lava_cave'][3])#fire bat
		
		
		#overworld orc
		self.mlist['orc'] = []
		
		for a in range(0,4):
			m=monster(techID = techID,
					name = 'hill orc',
					sprite_pos = (1,7),
					move_border = 2,
					attribute_prev = (3,0,1,0,1),
					worn_equipment = (1,0,1,0,0),
					AI_style = 'hostile',
					corps_style = 'human',
					corps_lvl = 2, 
					personal_id = 'None',
					move_groups = ('soil','low_liquid'), 
					behavior = 'attack_melee', 
					attack_were = ('Head','Body','Legs','Feet'),
					possible_effect = None, 
					effect_duration = 0, 
					effect_probability = 0,
					ability = {'def_potion' : 20,
								'range_throw' : (20,0)},
					message = 'None',)
			techID+=1
			self.mlist['orc'].append(m)
			
		self.mlist['orc'].append(self.mlist['cave'][0])#cave orc
		self.mlist['orc'].append(self.mlist['orcish_mines'][2])#orcish digger
		
		#demonic keep

		self.mlist['demonic_keep'] = []
		#0
		m=monster(techID = techID,
				name = 'male blue demon',
				sprite_pos = (4,7),
				move_border = 2,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'scrollkeeper',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_magic', 
				attack_were = ('Head'),
				possible_effect = 'hexed', 
				effect_duration = 30,
				effect_probability = 60,
				ability = {'range_shoot' : 70},
				message = 'A blue demon puts a hex on you.',
				light_emit = 2)
		techID+=1
		self.mlist['demonic_keep'].append(m)
		#1
		m=monster(techID = techID,
				name = 'female blue demon',
				sprite_pos = (4,8),
				move_border = 2,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'scrollkeeper',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_magic', 
				attack_were = ('Head'),
				possible_effect = 'hexed', 
				effect_duration = 30,
				effect_probability = 60,
				ability = {'range_shoot' : 70},
				message = 'A blue demon puts a hex on you.',
				light_emit = 2)
		techID+=1
		self.mlist['demonic_keep'].append(m)
		#2
		m=monster(techID = techID,
				name = 'male red demon',
				sprite_pos = (4,5),
				move_border = 2,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,1,0,0,1),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'None', 
				effect_duration = 0,
				effect_probability = 0,
				ability = {'close_flames' : 70},
				message = 'None',
				light_emit = 2,)
		techID+=1
		self.mlist['demonic_keep'].append(m)
		#3
		m=monster(techID = techID,
				name = 'female red demon',
				sprite_pos = (4,6),
				move_border = 2,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,1,0,0,1),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'None', 
				effect_duration = 0,
				effect_probability = 0,
				ability = {'close_flames' : 70},
				message = 'None',
				light_emit = 2)
		techID+=1
		self.mlist['demonic_keep'].append(m)
		#4
		m=monster(techID = techID,
				name = 'magic mirror',
				sprite_pos = (5,2),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'None', 
				effect_duration = 0,
				effect_probability = 0,
				ability = {'range_shoot' : 70,
							'spawn' : (20,8)},
				message = 'None',
				light_emit = 3)
		techID+=1
		self.mlist['demonic_keep'].append(m)
		#5
		m=monster(techID = techID,
				name = 'imp',
				sprite_pos = (12,3),
				move_border =3,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,1,1,1),
				AI_style = 'hostile',
				corps_style = 'scrollkeeper',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_magic', 
				attack_were = ('Head'),
				possible_effect = 'hexed', 
				effect_duration = 20,
				effect_probability = 30,
				ability = {'range_shoot' : 70,
								'blink': 20},
				message = 'A imp puts a hex on you.',
				light_emit = 2)
		techID+=1
		self.mlist['demonic_keep'].append(m)
		#6
		m=monster(techID = techID,
				name = 'hellhound',
				sprite_pos = (12,2),
				move_border = 1,
				attribute_prev = (2,0,2,1,2),
				worn_equipment = (0,1,1,1,1),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Body','Legs','Feet'),
				possible_effect = 'bleeding', 
				effect_duration = 15,
				effect_probability = 30,
				message = 'A hellhound tears you a bleeding wound.',
				light_emit = 3)
		techID+=1
		self.mlist['demonic_keep'].append(m)
		#7
		m=monster(techID = techID,
				name = 'eyeball',
				sprite_pos = (12,5),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'miner',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'None', 
				effect_duration = 0,
				effect_probability = 0,
				ability = {'range_shoot' : 90},
				message = 'None')
		techID+=1
		self.mlist['demonic_keep'].append(m)
		#8
		m=monster(techID = techID,
				name = 'hollow',
				sprite_pos = (12,6),
				move_border = 4,
				attribute_prev = (2,0,0,0,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'humanoid',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body'),
				possible_effect = 'hexed', 
				effect_duration = 50,
				effect_probability = 50,
				message = 'A hollow puts a hex on you.')
		techID+=1
		self.mlist['demonic_keep'].append(m)
		
		#desert lava cave
		self.mlist['desert_lava_cave'] = []
		#0
		m=monster(techID = techID,
				name = 'magma snail',
				sprite_pos = (12,7),
				move_border = 6,
				attribute_prev = (2,2,0,1,2),
				worn_equipment = (1,1,0,0,1),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_melee', 
				attack_were = ('Feet','Legs'),
				possible_effect = 'None', 
				effect_duration = 0,
				effect_probability = 0,
				ability = {'close_flames' : 70},
				message = 'None',
				light_emit = 3)
		techID+=1
		self.mlist['desert_lava_cave'].append(m)
		#1
		m=monster(techID = techID,
				name = 'djinn',
				sprite_pos = (7,12),
				move_border = 2,
				attribute_prev = (0,1,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'scrollkeeper',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entance'), 
				behavior = 'attack_magic', 
				attack_were = ('Head'),
				possible_effect = 'hexed', 
				effect_duration = 30,
				effect_probability = 60,
				properties = ['hover',], 
				ability = {'range_shoot' : 70},
				upper_sprite =(6,12),
				message = 'A djinn puts a hex on you.',
				light_emit = 2)
		techID+=1
		self.mlist['desert_lava_cave'].append(m)
		#2
		m=monster(techID = techID,
				name = 'lizard man',
				sprite_pos = (12,8),
				move_border = 2,
				attribute_prev = (3,2,0,0,2),
				worn_equipment = (1,0,1,0,0),
				AI_style = 'hostile',
				corps_style = 'human',
				corps_lvl = 2, 
				personal_id = 'None',
				move_groups = ('soil','dry_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'bleeding', 
				effect_duration = 10, 
				effect_probability = 30,
				ability = {'def_potion' : 40,
								'range_throw' : (20,2)},
				message = 'None',)
		techID+=1
		self.mlist['desert_lava_cave'].append(m)
		#4
		m=monster(techID = techID,
				name = 'ancient death totem',
				sprite_pos = (12,9),
				move_border = 0,
				attribute_prev = (0,0,2,2,2),
				worn_equipment = (0,1,0,1,1),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'None', 
				effect_duration = 0,
				effect_probability = 0,
				ability = {'range_shoot' : 70,
							'spawn' : (20,7)},
				message = 'None',
				light_emit = 2)
		techID+=1
		self.mlist['desert_lava_cave'].append(m)
		
		
		#pet
		self.mlist['pet'] = []
		#0
		m=monster(techID = techID,
				name = 'baby blob',
				sprite_pos = (7,2),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 1,
				memory = 'first_meet = True;',
				properties = ['pet0'],
				ability = {'attack_monster' : 40},
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#1
		m=monster(techID = techID,
				name = 'blob',
				sprite_pos = (7,3),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 2,
				memory = 'first_meet = True;',
				properties = ['pet1'], 
				ability = {'attack_monster' : 60,
							'-skill' : 1},
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#2
		m=monster(techID = techID,
				name = 'great blob',
				sprite_pos = (7,4),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 2,
				memory = 'first_meet = True;',
				properties = ['pet2'],
				ability = {'attack_monster' : 80,
							'-skill' : 1,
							'eat_monster' : 10},
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#3
		m=monster(techID = techID,
				name = 'lux',
				sprite_pos = (7,5),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 4,
				memory = 'first_meet = True;',
				properties = ['pet0'],
				light_emit = 5,
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#4
		m=monster(techID = techID,
				name = 'duo-lux',
				sprite_pos = (7,6),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 5,
				memory = 'first_meet = True;',
				properties = ['pet1'], 
				ability = {'blind' : 20},
				light_emit = 6,
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#5
		m=monster(techID = techID,
				name = 'tri-lux',
				sprite_pos = (7,7),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 1,
				memory = 'first_meet = True;',
				properties = ['pet2'], 
				ability = {'blind' : 40},
				light_emit = 7,
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#6
		m=monster(techID = techID,
				name = 'small eye',
				sprite_pos = (9,1),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 7,
				memory = 'first_meet = True;',
				properties = ['pet0','hover'], 
				ability = {'stasis' : (10,1,3)},
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#7
		m=monster(techID = techID,
				name = 'floating eye',
				sprite_pos = (9,2),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 8,
				memory = 'first_meet = True;',
				properties = ['pet1','hover'], 
				ability = {'stasis' : (30,3,5)},
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#8
		m=monster(techID = techID,
				name = 'beholder',
				sprite_pos = (9,3),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 1,
				memory = 'first_meet = True;',
				properties = ['pet2','hover'], 
				ability = {'stasis' : (50,5,8)},
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#9
		m=monster(techID = techID,
				name = 'spark',
				sprite_pos = (11,3),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 10,
				memory = 'first_meet = True;',
				properties = ['pet0','hot'], 
				ability = {'burn' : 15},
				light_emit = 3,
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#10
		m=monster(techID = techID,
				name = 'flame',
				sprite_pos = (11,4),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 11,
				memory = 'first_meet = True;',
				properties = ['pet1','hot'], 
				ability = {'burn' : 25},
				light_emit = 4,
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#11
		m=monster(techID = techID,
				name = 'blaze',
				sprite_pos = (11,5),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 1,
				memory = 'first_meet = True;',
				properties = ['pet2','hot'], 
				ability = {'burn' : 50},
				light_emit = 5,
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#12
		m=monster(techID = techID,
				name = 'flint',
				sprite_pos = (11,6),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 13,
				memory = 'first_meet = True;',
				properties = ['pet0','double_lp'], 
				ability = {'eat_rock' : 10,
							'attack_monster' : 10},
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#13
		m=monster(techID = techID,
				name = 'boulder',
				sprite_pos = (11,7),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 14,
				memory = 'first_meet = True;',
				properties = ['pet1','double_lp'], 
				ability = {'eat_rock' : 10,
							'attack_monster' : 20,},
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		#14
		m=monster(techID = techID,
				name = 'old rock',
				sprite_pos = (11,8),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'pet',
				anger_monster = 1,
				memory = 'first_meet = True;',
				properties = ['pet2','double_lp'], 
				ability = {'eat_rock' : 10,
							'attack_monster' : 30,},
				message = 'pet')
		techID+=1
		self.mlist['pet'].append(m)
		
		#seraph
		self.mlist['seraph'] = []
		#0
		m=monster(techID = techID,
				name = 'seraph treasurer',
				sprite_pos = (7,10),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'first_meet = True; name = "Unknown"; stored_coins = 0',
				quest_state = True,
				properties = ['npc','villager'],
				message = 'treasurer')
		techID+=1
		self.mlist['seraph'].append(m)
		#1
		m=monster(techID = techID,
				name = 'seraphine petkeeper',
				sprite_pos = (8,0),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'first_meet = True; name = "Unknown"',
				quest_state = True,
				properties = ['npc','villager'],
				message = 'petkeeper')
		techID+=1
		self.mlist['seraph'].append(m)
		#2
		m=monster(techID = techID,
				name = 'seraph warrior',
				sprite_pos = (8,1),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'first_meet = True; name = "Unknown"',
				quest_state = True,
				properties = ['npc','villager'],
				message = 'warrior')
		techID+=1
		self.mlist['seraph'].append(m)
		#3
		m=monster(techID = techID,
				name = 'seraphine mage',
				sprite_pos = (8,2),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'first_meet = True; name = "Unknown"',
				quest_state = True,
				properties = ['npc','villager'],
				message = 'mage')
		techID+=1
		self.mlist['seraph'].append(m)
		#4
		m=monster(techID = techID,
				name = 'seraph constructor',
				sprite_pos = (8,3),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'first_meet = True; name = "Unknown"',
				quest_state = True,
				properties = ['npc','villager'],
				message = 'constructor')
		techID+=1
		self.mlist['seraph'].append(m)
		#5
		m=monster(techID = techID,
				name = 'seraphine crafter',
				sprite_pos = (7,9),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'first_meet = True; name = "Unknown"',
				quest_state = True,
				properties = ['npc','villager'],
				message = 'crafter')
		techID+=1
		self.mlist['seraph'].append(m)
		#6
		m=monster(techID = techID,
				name = 'seraph portalkeeper',
				sprite_pos = (7,8),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'first_meet = True; name = "Unknown"',
				quest_state = True,
				properties = ['npc','villager'],
				message = 'portalkeeper')
		techID+=1
		self.mlist['seraph'].append(m)
		#7
		m=monster(techID = techID,
				name = 'seraph cook',
				sprite_pos = (9,0),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'first_meet = True; name = "Unknown"; last_meet = -1;',
				quest_state = True,
				properties = ['npc','villager'],
				message = 'cook')
		techID+=1
		self.mlist['seraph'].append(m)
		#8
		m=monster(techID = techID,
				name = 'seraph portalkeeper(intro)',
				sprite_pos = (7,8),
				move_border = 0,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'first_meet = True; name = "Unknown"',
				quest_state = True,
				properties = ['npc','villager'],
				message = 'intro')
		techID+=1
		self.mlist['seraph'].append(m)

		#sewer
		self.mlist['sewer'] = []
		#0
		m=monster(techID = techID,
				name = 'spider',
				sprite_pos = (6,0),
				move_border = 2,
				attribute_prev = (2,2,0,1,3),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'spider',
				corps_lvl = 2,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance','low_liquid','wet_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = 'poisoned', 
				effect_duration = 20, 
				effect_probability = 40,
				message = 'A spider poisons you.')
		techID+=1
		self.mlist['sewer'].append(m)
		#1
		m=monster(techID = techID,
				name = 'green snake',
				sprite_pos = (0,9),
				move_border = 1,
				attribute_prev = (2,1,0,1,1),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'animal',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance','low_liquid','wet_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = 'poisoned', 
				effect_duration = 20, 
				effect_probability = 40, 
				ability = {'def_flee' : 20},
				message = 'Poison runs trough your veins!')
		techID+=1
		self.mlist['sewer'].append(m)
		#2
		m=monster(techID = techID,
				name = 'wild duo-lux',
				sprite_pos = (10,1),
				move_border = 2,
				attribute_prev = (1,1,1,1,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'vanish',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance'), 
				behavior = 'attack_magic', 
				attack_were = ('Legs','Body'),
				possible_effect = 'blind', 
				effect_duration = 50, 
				effect_probability = 40, 
				message = 'A wild duo-lux blinds you!',
				light_emit = 4)
		techID+=1
		self.mlist['sewer'].append(m)
		#3
		m=monster(techID = techID,
				name = 'rat',
				sprite_pos = (10,0),
				move_border = 2,
				attribute_prev = (3,1,0,0,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'animal',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Feet'),
				possible_effect = 'sick', 
				effect_duration = 4319, 
				effect_probability = 15, 
				message = 'You feel very sick!')
		techID+=1
		self.mlist['sewer'].append(m)
		#4
		m=monster(techID = techID,
				name = 'gas-filled bubble',
				sprite_pos = (10,3),
				move_border = 4,
				attribute_prev = (1,1,1,1,2),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'ignore',
				corps_style = 'explode',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','low_liquid','dry_entrance','wet_entrance','jump'), 
				behavior = 'attack_melee', 
				attack_were = ('Legs','Body'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['cattle','hover'],
				message = 'None')
		techID+=1
		self.mlist['sewer'].append(m)
		
		self.mlist['sewer'].append(self.mlist['special'][15])
		
		#boss
		self.mlist['boss'] = []
		for i in range(0,2):
			#0
			m=monster(techID = techID,
				name = 'demon lord',
				sprite_pos = (10,11),
				move_border = 3,
				attribute_prev = (2,2,0,1,3),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'boss_dungeon',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance','low_liquid','wet_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				properties = ['give knockback','knockback res'],
				message = 'None',
				upper_sprite =(9,11))
			techID+=1
			self.mlist['boss'].append(m)
			#1
			m=monster(techID = techID,
				name = 'ancient demi-god',
				sprite_pos = (5,12),
				move_border = 2,
				attribute_prev = (1,1,2,2,3),
				worn_equipment = (0,0,0,0,0),
				AI_style = 'hostile',
				corps_style = 'boss_tomb',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance','low_liquid','wet_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body'),
				possible_effect = 'hexed', 
				effect_duration = 100, 
				effect_probability = 60,
				properties = ['give knockback','knockback res'],
				ability = {'range_shoot' : 70,	'spawn' : (40,8)},
				message = 'The ancient god puts a hex on you.',
				upper_sprite =(4,12))
			techID+=1
			self.mlist['boss'].append(m)
			#2
			m=monster(techID = techID,
				name = 'tentacle abnormality',
				sprite_pos = (3,12),
				move_border = 3,
				attribute_prev = (2,2,2,2,3),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'hostile',
				corps_style = 'boss_sewers',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance','low_liquid','wet_entrance','swim'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs','Feet'),
				possible_effect = 'sick', 
				effect_duration = 4319, 
				effect_probability = 30,
				properties = ['give knockback','knockback res'],
				ability = {'spawn' : (40,45)},
				message = 'The abnormalities touch makes you feel sick.',
				upper_sprite =(2,12))
			techID+=1
			self.mlist['boss'].append(m)
			#3
			m=monster(techID = techID,
				name = 'cave troll',
				sprite_pos = (12,11),
				move_border = 3,
				attribute_prev = (2,2,2,2,3),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'hostile',
				corps_style = 'boss_orcish_mines',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance','low_liquid','wet_entrance'), 
				behavior = 'attack_melee', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'confused', 
				effect_duration = 10, 
				effect_probability = 30,
				properties = ['give knockback','knockback res'],
				ability = {'range_throw' : (40,0)},
				message = 'The cave troll\'s punch makes you feel dizzy for a moment.',
				upper_sprite =(11,11))
			techID+=1
			self.mlist['boss'].append(m)
			#4
			m=monster(techID = techID,
				name = 'gorgon',
				sprite_pos = (1,12),
				move_border = 3,
				attribute_prev = (2,2,2,2,3),
				worn_equipment = (1,1,1,1,1),
				AI_style = 'hostile',
				corps_style = 'boss_grot',
				corps_lvl = 1,
				personal_id = 'None',
				move_groups = ('soil','dry_entrance','low_liquid','wet_entrance'), 
				behavior = 'attack_magic', 
				attack_were = ('Head','Body','Legs'),
				possible_effect = 'immobilized', 
				effect_duration = 10, 
				effect_probability = 35,
				properties = ['give knockback','knockback res'],
				ability = {'range_shoot' : (80,0)},
				message = 'The gorgon makes eye contact. You cant move any longer.',
				light_emit = 2,
				upper_sprite =(0,12))
			techID+=1
			self.mlist['boss'].append(m)
			
		#books
		self.mlist['book'] = []
		#0
		m=monster(techID = techID,
				name = 'Book: The History of Elvenkind',
				sprite_pos = (12,10),
				move_border = 10,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'title="The History Of Elvenkind"',
				message = 'book')
		techID+=1
		self.mlist['book'].append(m)
		#1
		m=monster(techID = techID,
				name = "Book: Karula's Notes",
				sprite_pos = (12,10),
				move_border = 10,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'title="Karula\'s Notes"',
				message = 'book')
		techID+=1
		self.mlist['book'].append(m)
		#2
		m=monster(techID = techID,
				name = "Book: Karula's Notes",
				sprite_pos = (12,10),
				move_border = 10,
				attribute_prev = (0,0,0,0,1),
				worn_equipment = (0,1,0,1,0),
				AI_style = 'ignore',
				corps_style = 'vanish',
				corps_lvl = 5,
				personal_id = 'None',
				move_groups = ('NO','MOVE'), 
				behavior = 'talk', 
				attack_were = ('Legs','Feet'),
				possible_effect = None, 
				effect_duration = 0, 
				effect_probability = 0,
				anger = 'None',
				anger_monster = 1,
				memory = 'title="Karula\'s Confession"',
				message = 'book')
		techID+=1
		self.mlist['book'].append(m)
